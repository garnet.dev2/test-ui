import moment from "moment";

export const NumberToWord = (numberInput) => {
  let oneToTwenty = [
    "",
    "one ",
    "two ",
    "three ",
    "four ",
    "five ",
    "six ",
    "seven ",
    "eight ",
    "nine ",
    "ten ",
    "eleven ",
    "twelve ",
    "thirteen ",
    "fourteen ",
    "fifteen ",
    "sixteen ",
    "seventeen ",
    "eighteen ",
    "nineteen ",
  ];
  let tenth = [
    "",
    "",
    "twenty",
    "thirty",
    "forty",
    "fifty",
    "sixty",
    "seventy",
    "eighty",
    "ninety",
  ];
  if (numberInput?.toString().length > 7) return false;
  //let num = ('0000000000'+ numberInput).slice(-10).match(/^(\d{1})(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  let num = ("0000000" + numberInput)
    .slice(-7)
    .match(/^(\d{1})(\d{1})(\d{2})(\d{1})(\d{2})$/);
  if (!num) return;
  let outputText =
    num[1] != 0
      ? (oneToTwenty[Number(num[1])] ||
          `${tenth[num[1][0]]} ${oneToTwenty[num[1][1]]}`) + " million "
      : "";
  outputText +=
    num[2] != 0
      ? (oneToTwenty[Number(num[2])] ||
          `${tenth[num[2][0]]} ${oneToTwenty[num[2][1]]}`) + "hundred "
      : "";
  outputText +=
    num[3] != 0
      ? (oneToTwenty[Number(num[3])] ||
          `${tenth[num[3][0]]} ${oneToTwenty[num[3][1]]}`) + " thousand "
      : "";
  outputText +=
    num[4] != 0
      ? (oneToTwenty[Number(num[4])] ||
          `${tenth[num[4][0]]} ${oneToTwenty[num[4][1]]}`) + "hundred "
      : "";
  outputText +=
    num[5] != 0
      ? oneToTwenty[Number(num[5])] ||
        `${tenth[num[5][0]]} ${oneToTwenty[num[5][1]]} `
      : "";

  return outputText;
};

export const toIndianCurrency = (num) => {
  // console.log(num, "====num");
  const nums = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2,
  }).format(num);
  return nums;
};

export const fomatGspDate = (date) => {
  const dateArr = date?.split(" ");
  // console.log(`${dateArr[1]} ${dateArr[2]}, ${dateArr[3]}`);
  return dateArr ? `${dateArr[1]} ${dateArr[2]}, ${dateArr[3]}` : "";
};

export function runMapOnce(installments) {
  let hasRun = false;
  let dueDate = "";
  if (!hasRun) {
    installments.map((kitty, index) => {
      if (kitty?.payment_status == "Paid") {
        dueDate = fomatGspDate(installments[index + 1]?.due_date);
      } else {
        dueDate = fomatGspDate(installments[index]?.due_date);
      }
      hasRun = true;
      console.log(dueDate, "--due_date");
      return dueDate;
    });
  } else {
    return "hello";
  }
}

export const getDueDate = (installments) => {
  let date = "";
  for (var i in installments) {
    if (installments[i].payment_status == "Not Paid") {
      date = fomatGspDate(installments[i]?.due_date);
      break;
    } else {
      date = fomatGspDate(installments[i]?.due_date);
    }
  }

  return date;
};

export const checkDate = (dateString) => {
  var dateMoment = moment(dateString + " " + moment().year(), "MMM DD YYYY");
  var todayMoment = moment();

  const dueDate = new Date(dateMoment?._i).toISOString();
  const today = new Date(todayMoment?._d).toISOString();

  // console.log(
  //   new Date(dateMoment?._i).toISOString(),
  //   "date",
  //   new Date(todayMoment?._d).toISOString()
  // );

  if (today < dueDate) {
    return false;
  } else {
    return true;
  }
};
