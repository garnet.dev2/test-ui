export function findMatchingStringsInSlug(slug, arrayOfStrings) {
  // Split the slug string by '-'
  const slugParts = slug?.split("-");

  // Initialize an array to store matching strings
  const matchingStrings = [];
console.log(slugParts,'slugParts')
  // Iterate over each part of the slug
  if(slugParts?.length>0){
    for (const part of slugParts) {
      // Check if the part matches any string in the array
      if (arrayOfStrings.includes(part)) {
        matchingStrings.push(part); // If match found, add it to the matchingStrings array
      }
    }
  }


  // Return the array of matching strings
  return matchingStrings;
}

export function keyExistsInArrayOfObjects(key, arrayOfObjects) {
  for (let i = 0; i < arrayOfObjects.length; i++) {
    if (arrayOfObjects[i].hasOwnProperty(key)) {
      return { exists: true, index: i }; // Key exists in object at index i
    }
  }
  return { exists: false, index: -1 };
}
