  export const toIndianCurrency = (num) => {
    const nums=new Intl.NumberFormat("en-IN",{
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }).format(num)
  return nums;
  };
  export const getCountByTitle=(title,data)=>{
    const categoryArray = data?.category;
    const filteredCategory = categoryArray?.filter(item => item.value.title === title);
    // If the array is not empty, return the count; otherwise, return 0
    return filteredCategory?.length > 0 ? filteredCategory[0].count : 0;
  }