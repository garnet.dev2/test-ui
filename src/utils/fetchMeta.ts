import { API_BASE_URL } from "@/constants";
import { metaUrls, metaData, meta, metaData2 } from "../utils/staticMeta";
import axios from "axios";

const categories = [];

export const containsCategory = (string: string, categories: any) => {
  const foundCategories = [];
  for (const category of categories) {
    if (string.toLowerCase().includes(category.toLowerCase())) {
      foundCategories.push(category); // Return true if any category is found in the string
    }
  }
  console.log(foundCategories, "foundcategories");
  if (foundCategories.length > 1) {
    return foundCategories[foundCategories.length - 1];
  } else {
    return foundCategories[0];
  }
};

export const fetchStaticMeta = async (url: string) => {
  const categoryArray = [
    "rings",
    "earrings",
    "bracelets",
    "mangalsutra",
    "pendants",
    "nosepins",
    "bangles",
    "goldcoins",
    "necklaces",
    "nosepins",
    "watch+accessory",
    "stones",
    "chains",
  ];
  let finalString = "";
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  console.log(url, "urlconfig");

  const check = url.split("-");

  if (check?.length == 1) {
    finalString = check[0];
  }

  if (check?.length > 1) {
    finalString = containsCategory(url, categoryArray);
  }

  console.log(finalString, "finalString");
  let metaDatas = {};
  if (metaUrls.includes(url)) {
    metaData.map((meta) => {
      if (meta?._id == url) {
        metaDatas = meta;
      }
    });
    return metaDatas;
  } else if (url == "jewellery") {
    let data: any = metaData2;
    const tempData = data[url];

    return tempData;
  } else if (meta.includes(url)) {
    let data: any = metaData2;
    const tempData = data[url];

    return tempData;
  } else {
    const resp = await axios.get(
      `${API_BASE_URL}/getMeta?q=${finalString}`,
      config
    );
    const currentMeta = resp?.data?.meta;
    console.log(currentMeta, "currentMeta");

    if (!currentMeta) {
      return {
        notFound: true,
      };
    }
    return currentMeta;
  }
};

export const fetchDynamicMeta = async (url: string) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const resp = await axios.get(
    `${API_BASE_URL}/getMeta/metaUrl?q=${url}?`,
    config
  );
  const currentMeta = resp?.data?.meta;

  if (!currentMeta) {
    return {
      notFound: true,
    };
  }
  return currentMeta;
};
