import { fomatGspDate } from "./numberToWord";
import { toIndianCurrency } from "./priceHelper";

export function isValidNumber(number) {
  return typeof number === "number" && number >= 2000 && number <= 200000;
}

export function isMultipleOf1000(number) {
  return number % 100 === 0;
}

export const checkNumber = (number) => {
  if (!isValidNumber(number)) {
    return false;
  }
  return isMultipleOf1000(number);
};

export const hasValidEmailAndAmount = (obj) => {
  if (!obj || !obj.hasOwnProperty("email") || !obj.hasOwnProperty("amount")) {
    return false;
  }

  return (
    typeof obj.email === "string" &&
    obj.email.trim() !== "" &&
    typeof obj.amount === "string" &&
    obj.amount.trim() !== ""
  );
};

export const formatGspInstallments = (array) => {
  let formattedArray = [];
  array.map((installment, index) => {
    formattedArray.push({
      month: installment?.installmentNo,
      dueDate: fomatGspDate(installment?.due_date),
      amount: toIndianCurrency(Number(installment?.installment_amount)).split(
        "."
      )[0],
      paymentDate:
        installment?.payment_date !== null
          ? fomatGspDate(installment?.payment_date)
          : "--",
      status: installment.payment_status,
      amountSource: installment.amount_source,
      action: installment.payment_status,
      key: index,
    });
  });
  return formattedArray;
};

export const hasPassedToday = (date) => {
  // Get the current date
  const currentDate = new Date();
  console.log(date, "date");

  // Get the year, month, and day components of the current date
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth();
  const currentDay = currentDate.getDate();

  // Get the year, month, and day components of the given date
  const givenYear = date.getFullYear();
  const givenMonth = date.getMonth();
  const givenDay = date.getDate();

  // Compare if the given date is the same day or before the current date
  return (
    givenYear < currentYear ||
    (givenYear === currentYear &&
      (givenMonth < currentMonth ||
        (givenMonth === currentMonth && givenDay <= currentDay)))
  );
};
export const getDateToString = (date) => {
  let dd = String(date?.getDate()).padStart(2, "0");
  let mm = String(date?.getMonth() + 1).padStart(2, "0"); //January is 0!
  let yyyy = date.getFullYear();

  return mm + "/" + dd + "/" + yyyy;
};

export function findUnpaidPayment(plan) {
  if (Array.isArray(plan) && plan.length > 0) {
    const [ins] = plan;
    const { installments } = ins;
    // Loop through each object in the array
    for (let i = 0; i < installments?.length; i++) {
      // Check if the payment status of the current object is 'unpaid'
      if (installments[i]?.payment_status === "Not Paid") {
        // If found, return the object
        return installments[i];
      }
    }
  }

  // If no unpaid payment is found, return null or any suitable value
  return null;
}

export function checkObjectKeys(obj) {
  const requiredKeys = [
    "receipentName",
    "nomineeName",
    "relationship",
    "nationality",
    "pan",
    "receipentNumber",
    "gspPincode",
    "email",
  ];
  console.log(typeof obj, "keys");
  if (obj == undefined) {
    return false;
  }
  for (const key of requiredKeys) {
    if (!(key in obj) || obj[key].trim() === "") {
      return false;
    }
  }

  return true;
}
