export function isValidUrl(urlString) {
  try {
    new URL(urlString);
    return true;
  } catch (error) {
    if (error instanceof TypeError) {
      return false; // URL is invalid
    }
    throw error; // Re-throw other errors
  }
}
