
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { collection, onSnapshot, getDocs, doc, getCountFromServer} from "firebase/firestore";

// const firebaseConfig = {
//     apiKey: process.env.NEXT_PUBLIC_API_KEY,
//     authDomain: process.env.NEXT_PUBLIC_AUTH_DOMAIN,
//     projectId: process.env.NEXT_PUBLIC_PROJECT_ID,
//     storageBucket: process.env.NEXT_PUBLIC_STORAGE_BUCKET,
//     messagingSenderId: process.env.NEXT_PUBLIC_MESSAGIN_SENDER_ID,
//     appId: process.env.NEXT_PUBLIC_APP_ID,
//     measurementId: process.env.NEXT_PUBLIC_MEASUREMENT_ID,
// }
// //____________Initialize Firebase______________\\

// const app = initializeApp(firebaseConfig)
// const firebaseDb = getFirestore(app)
// const Cart = getDocs(collection(firebaseDb, "cart"))
// const Wishlist = getDocs(collection(firebaseDb, "Wishlist"))
// export {
//     getCountFromServer,
//     firebaseConfig,
//     collection,
//     onSnapshot,
//     getDocs,
//     doc,
//     Cart,
//     Wishlist,
//     firebaseDb,
//     app
// };

