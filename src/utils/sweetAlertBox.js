import swal from "sweetalert";

export const Confirmbox = async (title, message, callback) => {
  // const t = langval ? langval : "";
  return await swal({
    title: title,
    text: message,
    icon: "warning",
    closeOnClickOutside: false,
    closeOnEsc: false,
    timer: 20000,
    buttons: {
      yes: {
        text: `OK`,
        value: true,
        closeModal: true,
      },
      no: {
        text: `Cancel`,
        value: false,
        closeModal: true,
      },
    },
  }).then((confirmed) => {
    if (confirmed) {
      callback(true);
    } else {
      callback(false);
    }
    return false;
  });
};
