export function findMissingKeys(obj) {
    if (!obj) {
        return [];
    }
    const keys = ['yellowGold', 'whiteGold', 'roseGold'];
    const missingKeys = keys.filter(key => !(key in obj));
    return missingKeys;
}