export const metaUrls = ["rings-solitaire"];

export const metaData = [
  {
    _id: "rings-solitaire",
    title:
      "Diamond Solitaire Rings | Exquisite Solitaire Diamond rings for Men & Women | Shop Now",
    description:
      "Discover the elegance of Diamond Solitaire Rings at Gemlay. Explore our stunning collection of solitaire diamond rings for Weddings, Engagements & Everyday . Shop now and find your perfect solitaire ring!",
    meta_link_type: "https://www.gemlay.com/jewellery/rings-solitaire ",
    keywords:
      "Solitaire Ring, Diamond Solitaire Ring, Solitaire, Solitaire Diamond Jewellery, Solitaire Diamond Rings, Diamond Rings, Solitaire Jewellery, Gemlay",
  },
];

export const meta = ["women", "men", "unisex"];

export const metaData2 = {
  women: {
    title: "Buy Women's Diamond Jewellery .For all Occasion | Gemlay",
    description:
      "Explore stunning women's diamond jewellery .Discover the latest designs in rings, earrings, necklaces, and more. Find your perfect piece at Gemlay. Shop today!",
    keyWords:
      "women's diamond jewellery sets, diamond hoop earrings, women's diamond earrings, diamond jewellery for women, Gemlay women's jewellery, elegant diamond sets, women's diamond accessories, diamond studs ,diamond necklaces",
  },
  men: {
    title: "Discover Men's Diamond Jewellery Collection | Gemlay",
    description:
      "Discover men's diamond jewellery collection. Featuring solitaire rings and diamond cufflinks, find the perfect accessory at Gemlay. Buy now!",
    keyWords:
      "men's diamond jewellery, diamond solitaire rings, men's diamond rings, diamond cufflinks, Gemlay men's jewellery, men's diamond accessories, stylish men's diamond jewellery",
  },
  unisex: {
    title: "Unisex Diamond Jewellery | Gemlay",
    description:
      "Discover our stylish unisex diamond jewellery . Explore genuine diamond designs at Gemlay and elevate your style. Shop now!",
    keyWords:
      "unisex diamond watch accessories, diamond watch accessories, unisex jewellery, unisex diamond jewellery, Gemlay unisex accessories, stylish diamond watch, diamond watch straps",
  },
  jewellery: {
    title:
      "Diamond Jewellery for Every Style & Occasion | Men, Women, and Kids",
    description:
      "Discover diamond jewellery for every style and occasion at Gemlay. Explore our collections of rings, earrings, bracelets, and pendants for men, women, and kids. Crafted with elegance and precision.",
    keyWords:
      "diamond jewellery, men's jewellery, women's jewellery, kids' jewellery, diamond rings, diamond earrings, diamond bracelets, diamond pendants, jewellery collections.",
  },
};
