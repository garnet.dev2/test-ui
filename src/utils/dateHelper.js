  export const matchGspDateToCurrent = (date) => {
    let dateStr = date;

    // Convert the string to a Date object
    let givenDate = new Date(dateStr);
    
    // Get the current date
    let currentDate = new Date();
    
    // Compare dates
    if (givenDate <= currentDate) {
        return true;
    } else {
      return false;
    }
  };
