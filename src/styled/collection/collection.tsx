import styled from "styled-components";

export const H1 = styled.h1`
  color: red !important;
  font-size: 4rem;
`;
export const Span = styled.span`
  font-size: 20px;
  color: rgb(253, 145, 73);
  font-weight: 500;
  font-style: normal;
  line-height: 18px;
  @media only screen and (max-width: 767px) {
    font-size: 12px;
    line-height: normal !important;
    display: inline-block;
  }
`;

export const Span2 = styled.span`
  font-size: 13px;
  color: rgb(0, 0, 0);
  font-weight: 400;
  font-style: normal;
  line-height: 18px;
  @media only screen and (max-width: 767px) {
    font-size: 9px;
    line-height: normal !important;
    display: inline-block;
  }
`;

export const Line = styled.div`
    position: absolute;
    width: 100%;
    left: 0px;
    top: 21px;
    border: 1px solid #dee2e6;
`;

export const LineDiv = styled.div`
  box-shadow: 0 1px 6px rgba(0,0,0,.25);
  height: 1px;
`;
