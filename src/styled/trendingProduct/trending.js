import styled from 'styled-components'

export const MainDiv = styled.div`
display: grid;
    place-items: center;
`

export const MainCard = styled.div`
    background-color: #fff;
    padding-bottom: 0;
    border-radius: 20px;
    margin-bottom: 0px;
    overflow: hidden;
    width: 100%!important;
    box-shadow: 0px 4px 8px 0px #ccc;
    @media only screen and (max-width: 767px) {
        height: auto;
      }
`

export const UpperSection = styled.div`
height: 250px;
@media only screen and (max-width: 767px) {
    height: auto;
  }
`

export const ImgTopDiv = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
min-height: 30px;
    padding-top: 14px;
    padding-right: 25px!important;
`

export const ImgContainer = styled.div`
margin-top: -24px;
display: flex;
justify-content: center;
`

export const BottomCardName = styled.div`
height: 91px;
margin-top: 20px;
.color-btn {
    display: flex;
    flex-flow: column;
    gap: 10px;
    .card-btn {
        height: 16px;
        width: 16px;
        display: block;
        background: red;
        border-radius: 100px;
        position: relative;
        &.yellow {
            background: #FFDE6A;
        }
        &.grey {
            background: #C2C0C0;
        }
        &.orange {
            background: linear-gradient(89deg, #FF6600 -7.64%, #FF9900 88.49%);
        }
        &:before {
            content: "";
            border: 1px solid #FFDE6A;
            width: 23px;
            height: 23px;
            position: absolute;
            border-radius: 100px;
            top: -4px;
            left: -4px;
            opacity: 0;
            visibility: hidden;
        }
        &:hover {
            &:before {
                opacity: 1;
                visibility: visible;
            }
        }
    }
}
@media only screen and (max-width: 767px) {
    height: 86px;
    margin-top: 0px;
  }
`

export const TxtDiv = styled.div`
display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: 9px;
`

export const MinHeightDiv = styled.div`
min-height: 23px;
`

export const PriceHeading = styled.h1`
    font-size: 16px;
    color: #302f2f;
    font-weight: 500;
    display: flex;
    align-items: center;
    gap: 5px;
    .old-price {
        text-decoration: line-through;
        font-size: 14px;
    }
`

export const TxtDiv2 = styled.div`
    padding-left: 9px;
    padding-right: 9px;
    padding-top: 4px;
    display: flex;
    justify-content: space-between;
`
export const PriceHeading2 = styled.h4`
white-space: nowrap;
    display: block;
    text-overflow: ellipsis;
    overflow: hidden;
    font-size: 13px;
    color: #302f2f;
    line-height: 18px;
    font-weight: 400;
}
`
export const BtnDivCard = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
`

export const SmallTxtBottom = styled.div`
padding-left: 6px;
    padding-top: 0;
    align-items: center!important;
    display: flex!important;
`

export const SmallTxtBottomTxt = styled.span`
    color: rgb(48, 47, 47);
    font-weight: 400;
    font-style: normal;
    line-height: 18px;
    font-size: 13px;
    white-space: nowrap;
    margin-left: 8px;
    @media only screen and (max-width: 767px) {
        font-size: 9px!important;
      }
`
export const CardBottomBtn = styled.div`
cursor: pointer;
    position: relative;
    height: 45px;
    width: 127px;
    @media only screen and (max-width: 767px) {
        height: 32px;
    width: 100px;
      }
`

export const ImgTg = styled.img`
height: 100%!important;
    width: 100%!important;
    position: absolute;
    transition: opacity .1s ease-in-out;
    &:hover {
        opacity: 0;
      }
      @media only screen and (max-width: 767px) {
        height: 110%!important;
      }
`

export const BottomBtnSpan = styled.span`
font-size: 13px;
    color: #fff;
    font-weight: 700;
    font-style: normal;
    line-height: 18px;
    position: absolute;
    top: 45%;
    left: 52%;
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
    @media only screen and (max-width: 767px) {
        font-size: 11px!important;
      }
`

export const GreenTxt = styled.span`
color: #16A06F;
font-size: 11px;
font-style: normal;
font-weight: 700;
line-height: 18px; /* 163.636% */
letter-spacing: 1px;
`

