import { Button } from "@nextui-org/react";
import styled from "styled-components";

export const DiscoverBtn = styled(Button).attrs({
  // size:'xs'
})`
  border-radius: 5px;
  background: linear-gradient(
    180deg,
    ${(props) => props.theme.colors.orange} 0%,
    ${(props) => props.theme.colors.secondary} 100%
  );
  width: 200px;
  height: 40px;
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: #fff;
  letter-spacing: 1px;
  border: 0;
  border-radius: 20px;
  &:hover {
    opacity: 0.8;
  }
  @media only screen and (max-width: 767px) {
    font-size: 10px;
  }
`;
export const DiscoverBtn2 = styled(Button).attrs((props) => ({
  // size:'xs'
  backgroundColor:
    props.bgColor || "linear-gradient(90deg,#012433 30%,#007A64 100%)",
  color: props.textColor || "#ffffff",
  backgroundColorHover:
    props.bgColorHover || "linear-gradient(180deg, #012433 0%, #007A64 100%)",
}))`
  border-radius: 5px;
  background: ${(props) => props.backgroundColor};
  width: 131px;
  height: 40px;
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: ${(props) => props.textColor};
  letter-spacing: -0.28px;
  border: 1px solid #000000;
  &:hover {
    background: ${(props) => props.backgroundColorHover};
    color: #FFFFFF;
    border: none;
  }
  @media only screen and (max-width: 767px) {
    font-size: 10px;
  }
`;
export const DiscoverBtn3 = styled(Button).attrs((props) => ({
  // size:'xs'
  backgroundColor:
    props.bgColor || "linear-gradient(90deg,#012433 30%,#007A64 100%)",
  color: props.textColor || "#ffffff",
  backgroundColorHover:
    props.bgColorHover || "linear-gradient(180deg, #012433 0%, #007A64 100%)",
  height: props.btnHeight || "40px"
}))`
  border-radius: 20px;
  background: ${(props) => props.backgroundColor};
  width: fit-content;
  height: ${(props) => props.btnHeight};
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: ${(props) => props.textColor};
  letter-spacing: -0.28px;
  /* border: 1px solid #000000; */
  &:hover {
    background: ${(props) => props.backgroundColorHover};
    color: #FFFFFF;
    border: none;
  }
  @media only screen and (max-width: 767px) {
    font-size: 10px;
  }
`;
export const DiscoverBtn4 = styled(Button).attrs((props) => ({
  // size:'xs'
  backgroundColor:
    props.bgColor || "linear-gradient(90deg,#012433 30%,#007A64 100%)",
    textColor: props.textColor || "#ffffff",
    btnHeight:props.btnHeight || "40px",
    btnWidth:props.btnWidth || "100%",
    btnBorderRadius:props.btnBorderRadius||"20px",
}))`
  border-radius: ${(props) => props.btnBorderRadius};
  background: ${(props) => props.backgroundColor};
  width: ${(props) => props.btnWidth};
  height: ${(props) => props.btnHeight};
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: ${(props) => props.textColor};
  letter-spacing: -0.28px;
  border: 0;
  &:hover {
    background: ${(props) => props.backgroundColor};
  }
  @media only screen and (max-width: 767px) {
    font-size: 10px;
  }
`;

export const CustomiseBtn = styled.button`
  border-radius: 20px;
  background: linear-gradient(
    89deg,
    ${(props) => props.theme.colors.orange} -7.64%,
    ${(props) => props.theme.colors.secondary} 88.49%
  );
  width: 120px;
  height: 40px;
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: #fff;
  letter-spacing: 1px;
  border: 0;
  &:hover {
    background: linear-gradient(
      89deg,
      ${(props) => props.theme.colors.secondary} -7.64%,
      ${(props) => props.theme.colors.orange} 88.49%
    );
  }
  @media only screen and (max-width: 767px) {
    font-size: 10px;
  }
`;

export const MobileCustomiseBtn = styled.button`
  border-radius: 10px;
  border: 1px solid ${(props) => props.theme.colors.orange};
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 35px;
  letter-spacing: 1px;
  width: 100%;
  &:hover {
    background: ${(props) => props.theme.colors.orange};
    color: #fff;
  }
`;

export const CardBadgeOne = styled.span`
  background: linear-gradient(
    267.38deg,
    #e5e5e5 24.98%,
    hsla(0, 0%, 90%, 0) 52.19%
  );
  background-color: #dceff6;
  min-width: 94px;
  height: 33px;
  border-radius: 20px;
  margin-left: 0.5rem !important;
  align-items: center !important;
  justify-content: center !important;
  display: flex !important;
  font-size: 11px;
  color: rgb(48, 47, 47);
  font-weight: 500;
  font-style: normal;
  line-height: 18px;
  z-index: 5;
  @media only screen and (max-width: 767px) {
    font-size: 9px;
    min-width: 74px;
    height: 25px;
  }
`;

export const BtnWhite = styled.span`
  border-radius: 30px;
  border: 0.5px solid #eee;
  background: #fff;
  color: ${(props) => props.theme.colors.orange};
  text-align: center;
  font-size: 13px;
  font-weight: 400;
  letter-spacing: 1px;
  height: 53px;
  padding: 15px;
  width: 100%;
  cursor: pointer;
  &:hover {
    background: linear-gradient(
      89deg,
      ${(props) => props.theme.colors.secondary} -7.64%,
      ${(props) => props.theme.colors.orange} 88.49%
    );
    color: #fff;
  }
`;

export const PlainBtn = styled.button`
  border-radius: 5px;
  border: 1px solid #dadada;
  background: #fff;
  color: rgba(0, 0, 0, 0.8);
  font-size: 13px;
  font-weight: 700;
  line-height: 18px;
  letter-spacing: 1px;
  height: 45px;
  padding: 15px;
  cursor: pointer;
  &:hover {
    background: linear-gradient(
      89deg,
      ${(props) => props.theme.colors.secondary} -7.64%,
      ${(props) => props.theme.colors.orange} 88.49%
    );
    color: #fff;
  }
`;
export const AddToCart = styled.button`
  border-radius: 20px;
  background: linear-gradient(
    90deg,
    ${(props) => props.theme.colors.orange} 0%,
    ${(props) => props.theme.colors.secondary} 100%
  );
  width: 100%;
  height: 45px;
  font-size: 13px;
  font-style: normal;
  font-weight: 600;
  line-height: 18px;
  color: #fff;
  letter-spacing: 1px;
  border: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  &:hover {
    background: linear-gradient(
      90deg,
      ${(props) => props.theme.colors.secondary} 0%,
      ${(props) => props.theme.colors.orange} 100%
    );
  }
`;
