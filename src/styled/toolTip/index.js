import styled from 'styled-components';
export const TooltipContainer = styled.div`
  position: relative;
  display: inline-block;
  cursor: pointer;
`;

export const TooltipText = styled.div`
  visibility: hidden;
  width: 230px;
  /* background-color: #fff7e8 !important; */
  background-color: #F4FFFD !important;
  color: #807F7F;
  text-align: center;
  border-radius: 5px;
  padding: 5px;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -60px;
  opacity: 0;
  transition: opacity 0.3s;

  ${TooltipContainer}:hover & {
    visibility: visible;
    opacity: 1;
  }
  @media (max-width: 768px) {
    white-space: normal;  
    overflow: visible;    
    text-overflow: clip;  
  }
`;


