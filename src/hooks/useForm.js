import _ from '../../src/lodash/@lodash';
import { useCallback, useState } from 'react';

function useForm(initialState, onSubmit) {
  const [form, setForm] = useState(initialState);
  const [text, setText] = useState(initialState);
  const handleChange = useCallback(event => {
    // event.persist();
    const persistedEvent = { ...event };
    setForm(_form =>
      _.setIn(
        { ..._form },
        event.target.name,
        event.target.type === 'checkbox' ? event.target.checked : event.target.value
      )
    );
  }, []);
  const MIN_LENGTH = 0; // Minimum length constraint



  const resetForm = useCallback(() => {
    if (!_.isEqual(initialState, form)) {
      setForm(initialState);
    }
  }, [form, initialState]);

  const setInForm = useCallback((name, value) => {
    setForm(_form => _.setIn(_form, name, value));
  }, []);

  const handleSubmit = useCallback(
    event => {
      if (event) {
        event.preventDefault();
      }
      if (onSubmit) {
        onSubmit();
      }
    },
    [onSubmit]
  );

  return {
    form,
    text,
    setText,
    handleChange,
    handleSubmit,
    resetForm,
    setForm,
    setInForm
  };
}

export default useForm;