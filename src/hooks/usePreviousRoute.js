import { useRouter } from 'next/router';
import { useRef } from 'react';

export const usePreviousRoute = () => {
  const Router = useRouter();

  const Ref = useRef(null);

  Router.events?.on('routeChangeStart', () => {
    // Ref?.current = Router?.asPath;
  });

  return Ref.current;
};