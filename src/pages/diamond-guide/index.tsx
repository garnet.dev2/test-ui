import { DIAMOND_GUIDE } from "@/constants/contents/jewelleryKnowledge";
import DiamondGuideNewCms from "@/container/footerContent/diamond-guide-new";

export default function DiamondGuide({ items }: any) {
  return (
    <>
      <DiamondGuideNewCms data={items} />
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {
      items: DIAMOND_GUIDE,
    },
  };
}
