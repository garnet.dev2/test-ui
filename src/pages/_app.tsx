import { useEffect, useState, useRef } from "react";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import "../../src/styles/globals.css";
import type { AppProps } from "next/app";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { NextUIProvider } from "@nextui-org/react";
import { useMobile } from "@/utils/useMobile";
import Footer from "@/container/home/footer/footer";
// import Header from "@/container/header/navbar";
// import MegaMenu from "@/container/home/megaMenu";
import { Provider } from "react-redux";
import { store, persistor } from "@/redux/store";
import { PersistGate } from "redux-persist/integration/react";
import Head from "next/head";
import Theme from "../container/common/theme/Theme";
import Createglobalstyle from "../container/common/theme/Globalstyled";
import { QueryClient, QueryClientProvider } from "react-query";
// import { AuthLayout } from "@/container/layout/AuthLayout";
import { WishlistProvider } from "react-use-wishlist";
import { CartProvider } from "react-use-cart";
import { BASE_URL } from "@/constants";
import "react-toastify/dist/ReactToastify.css";
import "@/styles/globals.css";
import AppContext from "@/constants/context";
import "../../public/fonts.css";
import Router, { useRouter } from "next/router";
import { Toaster } from "react-hot-toast";
// import HeaderSilver from "@/container/silver/header/navbar";
// import MegaMenuSilver from "@/container/silver/megaMenu";
NProgress.configure({
  minimum: 0.6,
  easing: "ease",
  speed: 800,
  showSpinner: false,
});

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});

Router.events.on("routeChangeComplete", () => {
  NProgress.done();
});

Router.events.on("routeChangeError", () => {
  NProgress.done();
});

export default function App({ Component, pageProps }: AppProps) {
  const queryClient = new QueryClient();
  const [userInfos, setUserInfos] = useState(false);
  const [pincode, setPincode] = useState("");
  const [gsp, setGsp] = useState<any>();
  const [slide, setSlide] = useState(false);
  const isMobileScreen = useMobile();
  const [appRender, setAppRender] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [wishlistPids, setWishlistPids] = useState([]);
  const [wishlistCount, setWishListCount] = useState<any>(null);
  const [wishlistItems, setWishListItems] = useState<any>();
  const [cartCount, setCartCount] = useState<any>(null);
  const Route = useRouter();
  useEffect(() => {
    setAppRender(true);

    const timeout = setTimeout(() => {
      setShowToast(true);
    }, 2500);

    return () => {
      clearTimeout(timeout);
    };
  }, []);
  // console.log(BASE_URL, 'BASE_URL')
  return (
    appRender && (
      <>
        <AppContext.Provider
          value={{
            userInfos,
            setUserInfos,
            pincode,
            setPincode,
            gsp,
            setGsp,
            slide,
            setSlide,
            wishlistPids,
            setWishlistPids,
            wishlistCount,
            setWishListCount,
            wishlistItems,
            setWishListItems,
            cartCount,
            setCartCount,
          }}
        >
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <CartProvider>
                <WishlistProvider>
                  {showToast && <Toaster position="bottom-center" />}
                  <QueryClientProvider client={queryClient}>
                    <NextUIProvider>
                      <Theme>
                        <Head>
                          {(BASE_URL === "https://www.gemlay.com/" ||
                            BASE_URL === "https://gemlay.com") && (
                            <>
                              {/* {console.log(BASE_URL, 'BASE_URL11')} */}
                              <script
                                async
                                src="https://www.googletagmanager.com/gtag/js?id=GTM-5QT4VMP"
                              ></script>
                              <script
                                async
                                dangerouslySetInnerHTML={{
                                  __html: `
                                window.dataLayer = window.dataLayer || [];
                                function gtag() {
                                dataLayer.push(arguments);
                                  }
                                gtag("js", new Date());
        
                                gtag("config", "GTM-5QT4VMP");`,
                                }}
                              ></script>
                              {/* microsoft clarity */}
                              <script
                                async
                                dangerouslySetInnerHTML={{
                                  __html: `
                                      (function(c,l,a,r,i,t,y){
                                        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
                                        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
                                        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
                                      })(window, document, "clarity", "script", "m664ffjp86");
                                    `,
                                }}
                              ></script>
                              {/* CRISP code  */}
                              <script
                                async
                                type="text/javascript"
                                dangerouslySetInnerHTML={{
                                  __html: `window.$crisp=[];window.CRISP_WEBSITE_ID="d8a1a423-c25c-443b-be17-6b67fc8e9bda";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();`,
                                }}
                              ></script>
                              {/* FACEBOOK domain Verify*/}
                              <meta
                                name="facebook-domain-verification"
                                content="cu5dfky14ffg07399g3kf5028h47yg"
                              />
                              <noscript>
                                <iframe
                                  src="https://www.googletagmanager.com/ns.html?id=GTM-5QT4VMP"
                                  height="0"
                                  width="0"
                                  style={{
                                    display: "none",
                                    visibility: "hidden",
                                  }}
                                />
                              </noscript>
                              {/* End CRISP code  */}

                              {/* google tag manager script-2 */}
                              <script
                                async
                                dangerouslySetInnerHTML={{
                                  __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                                })(window,document,'script','dataLayer','GTM-5QT4VMP');`,
                                }}
                              ></script>
                              <script
                                async
                                dangerouslySetInnerHTML={{
                                  __html: `!function(f,b,e,v,n,t,s)
                          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                          n.queue=[];t=b.createElement(e);t.async=!0;
                          t.src=v;s=b.getElementsByTagName(e)[0];
                          s.parentNode.insertBefore(t,s)}(window, document,'script',
                          'https://connect.facebook.net/en_US/fbevents.js');
                          fbq('init', '1364026761009054');
                          fbq('track', 'PageView');
                          fbq('track', 'ViewContent', {
                            content_type: 'product',
                            currency: 'INR',
                          });

                          { /* Hotjar script */ }
                          <!-- Hotjar Tracking Code for Site 4961876 (name missing) -->
                          <script>
                           (function(h,o,t,j,a,r){
                            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                            h._hjSettings={hjid:4961876,hjsv:6};
                            a=o.getElementsByTagName('head')[0];
                            r=o.createElement('script');r.async=1;
                            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                            a.appendChild(r);
                            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
                            </script>
                          `,
                                }}
                              ></script>

                              <noscript>
                                <img
                                  height="1"
                                  width="1"
                                  style={{ display: "none" }}
                                  src="https://www.facebook.com/tr?id=1364026761009054&ev=PageView&noscript=1"
                                />
                              </noscript>
                              {/* <!-- End Meta Pixel Code --> */}
                            </>
                          )}
                        </Head>
                        <Createglobalstyle />
                        {/* <AuthLayout> */}
                          {Route.pathname !== "/checkout" &&
                          Route.pathname !== "/gsp-checkout" &&
                          Route.pathname !== "/cart" &&
                          Router.pathname !== "/silver" &&
                          Router.pathname !== "/silver/jewellery" &&
                          Route.pathname !== "/silver/jewellery/[slug]" &&
                          Route.pathname !== "/silver/[category]/[slug]"
                           ? (
                            // <Header />
                            <></>
                          ) : (
                            <></>
                          )}
                          {Route.pathname == "/silver" 
                          || Route.pathname == "/silver/jewellery" 
                          || Route.pathname == "/silver/jewellery/[slug]" 
                          || Route.pathname == "/silver/[category]/[slug]"
                          ? (
                            // <HeaderSilver />
                            <></>
                          ) : (
                            <></>
                          )}
                          {/* <Header /> */}

                          {!isMobileScreen &&
                            Route.pathname !== "/checkout" &&
                            Route.pathname !== "/gsp-checkout" &&
                            Route.pathname !== "/cart" &&
                            Route.pathname !== "/silver" &&
                            Route.pathname !== "/silver/jewellery" &&
                            Route.pathname !== "/silver/jewellery/[slug]" && 
                            Route.pathname !== "/silver/[category]/[slug]" &&
                            // <MegaMenu />
                            <></>
                            }
                          {(!isMobileScreen && (Route.pathname == "/silver" ||Route.pathname == "/silver/jewellery" ||
                            Route.pathname == "/silver/jewellery/[slug]" ||
                            Route.pathname == "/silver/[category]/[slug]"
                          
                          )?  (
                              // <MegaMenuSilver />
                              <></>
                            ):(
                              <></>
                            ))}

                          {/* <ToastContainer
                          className="toastStyle"
                          bodyClassName="toastBody"
                          progressClassName="progressBar"
                          ---toaster---
                          position="bottom-center"
                          reverseOrder={true}
                          toastOptions={{
                            className: "",
                            style: {
                              fontSize: "14px",
                              padding: "16px",
                              color: "#000",
                              boxShadow: "0px 3px 12px 0px #0000001f",
                            },
                          }}
                        /> */}

                          <div className="middle-section">
                            {/* <Cursor/> */}
                            <Component {...pageProps} />
                          </div>
                          <Footer />
                        {/* </AuthLayout> */}
                        {/* { showToast && <Toaster />} */}
                      </Theme>
                    </NextUIProvider>
                  </QueryClientProvider>
                </WishlistProvider>
              </CartProvider>
            </PersistGate>
          </Provider>
        </AppContext.Provider>
      </>
    )
  );
}
