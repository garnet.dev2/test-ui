import { CERTICATION_GUIDE } from "@/constants/contents/jewelleryKnowledge";
import CertificationGuideCms from "@/container/footerContent/certification-guide";

export default function CertificationGuide({ items }: any) {
  return (
    <>
      <CertificationGuideCms data={items} />
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {
      items: CERTICATION_GUIDE,
    },
  };
}
