import { R_AND_R_POLICY } from "@/constants/contents/ourPolicy";
import ReturnAndRefundPolicyCms from "@/container/footerContent/return-and-refund-policy";

export default function ReturnAndRefundPolicy({ items }: any) {
  return (
    <>
      <ReturnAndRefundPolicyCms data={items} />
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {
      items: R_AND_R_POLICY,
    },
  };
}
