import Banner from "@/container/home/banner";
import SimpleSlider from "@/container/home/categoryBanner";
import { API_BASE_URL, NEXT_PUBLIC_API_NAME } from "@/constants";
import dynamic from "next/dynamic";

const Meta = dynamic(() => import("@/components/meta"), { suspense: true });

export default function Home(props: any) {
  const { banner, categories, reviews, currentMeta } = props;

  console.log(currentMeta, "currentMeta");

  // const homeMeta = currentMeta?.MetaData[0];

  return (
    <>
      <Meta
        title={currentMeta?.metaTitle}
        meta_link_type={currentMeta?.pageUrl}
        description={currentMeta?.metaDescription}
        keywords={currentMeta?.metaKeywords}
        image={currentMeta?.image}
      />
      <main className="">
        <Banner banner={banner} />
        <SimpleSlider categories={categories} />
        {/* <CollectionCard /> */}
        {/* <FastDelivery /> */}
        {/* <FastDelivery />
        
        <TrendingProducts />
        <BestSellerProducts />
        <ShopByCategoryCard categories={categories} />
        <ShopByGender />
        <Testimonials reviews={reviews} />
        <Brands />
        <Faqs /> */}
        <div className="bottom_social">
          {/* <SocialMedia /> */}
        </div>
      </main>
    </>
  );
}

export async function getServerSideProps(context: any) {
  try {
    // Fetch data from external APIs
    const responses = await Promise.all([
      fetch(`${API_BASE_URL}/banner?frontend=true`),
      fetch(`${API_BASE_URL}/category`),
      fetch(`${API_BASE_URL}/customer_review`),
      fetch(`${API_BASE_URL}/metaData?metaTitle=HOME`),
    ]);

    const [bannerResponse, categoryResponse, reviewResponse, metaDataResponse] =
      await Promise.all(responses.map((response) => response.json()));

    return {
      props: {
        banner: bannerResponse,
        categories: categoryResponse,
        reviews: reviewResponse,
        currentMeta: metaDataResponse?.MetaData[0],
      },
    };
  } catch (error) {
    console.error("Error fetching data:", error);
    return { props: { error: "Failed to fetch data" } };
  }
}
