import OurVisionCms from "@/container/footerContent/our-vision";
import { OUR_VISION } from "@/constants/contents/aboutUs";

export default function OurVision({ items }: any) {
  return (
    <>
      <OurVisionCms data={items} />
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {
      items: OUR_VISION,
    },
  };
}
