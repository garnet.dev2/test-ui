export const GSP_TC = {
  mainHeading: "GSP TERMS AND CONDITIONS",
  subHeading1: "Enrollment",
  lists: [
    "The minimum monthly payment for Gold Secure Pool (GSP) stands at ₹2,000 with a maximum limit of ₹2,00,000. The enrolment amount must be in multiples of ₹100.",
    "For monthly contributions surpassing ₹50,000 a PAN card/Aadhaar Card is mandatory.",
    "GSP instalments can exclusively be settled online. While we do accept cash, it is limited to ₹50,000 at the store. The GSP Value can be redeemed online at www.gemlay.com or at any Gemlay store.",
    "The monthly instalment amount remains unalterable during the Gold Secure Pool tenure.",
    "NRIs, international users, and minors (below the age of 18 years) are ineligible to enrol in GSP!",
  ],
  subHeading2: "Redemption",
  lists2: [
    "The Gold Secure Pool (GSP) from Gemlay spans a duration of 11 months. The customer's redemption window unfolds 30 days after the final instalment payment and stays open for an additional 3 months.",
    "GSP Value can be utilized for the acquisition of any Diamond, Gemstone, Pre-set Solitaires, or Customized Jewellery. However, it is not applicable for the redemption of 22KT jewellery, gold coins, loose diamonds, and gift coupons.",
    "A grace period of 15 days is extended after the due date for the monthly payment. In case of a delay, Gemlay contribution shall be diminished proportionally based on the number of days by which the payment was delayed.",
    "The GSP Value must be redeemed entirely in a single purchase. Partial redemption is not permitted. While you can purchase multiple products, they must be ordered in a single transaction.",
    "Redemption cannot involve the merging of two GSP accounts",
    "You have the flexibility to redeem your GSP Plan subscription at any time. You can then use the paid instalments and avail special discounts, as outlined in the table below, to purchase jewellery.",
  ],
  tableData: [
    {
      title: "10 Months",
      content: "100% of one installment amount",
    },
    {
      title: "7 Months",
      content: "50% of one installment amount",
    },
    {
      title: "5 Months",
      content: "25% of one installment amount",
    },
  ],
  subHeading3: "Refunds",
  lists3: [
    "In case of a 15-day money-back and cancellation, ONLY the principal amount will be refunded to the customer. Customers won't receive the Gemlay contribution.",
    "Refunds will be processed within 7-10 working days to the same account from which the contribution was made",
  ],
  subHeading4: "Others",
  lists4: [
    "In case of the demise of the enrolling individual, the nominee will be eligible to receive the amount. The nominee has to submit the required documents to transfer the GSP balance to themselves.",
  ],
};

export const FREE_SHIPPING = {
  mainHeading: "FREE SHIPPING",
  content: [
    "Gemlay is thrilled to announce that we now offer free shipping on all jewellery orders across India. We understand the importance of convenience and affordability when it comes to purchasing exquisite jewellery, which is why we've eliminated shipping fees for our customers nationwide.",
    "Whether you're in bustling cities like Delhi or Mumbai, or in remote areas across the country, you can now enjoy the luxury of shopping for our stunning collection of diamond and gold jewellery without worrying about additional shipping costs. From sparkling diamond rings to elegant gold necklaces, our entire range is now more accessible than ever before. At Gemlay, we believe that every customer deserves a seamless shopping experience from start to finish. With our commitment to providing exceptional service and unparalleled quality, we're delighted to extend free shipping to jewellery lovers across Pan India. Shop with confidence and indulge in the timeless beauty of Gemlay jewellery, delivered straight to your doorstep, without any extra charges.",
  ],
};

// export caches

export const R_AND_R_POLICY = {
  mainHeading: "RETURN AND REFUND POLICY",
  subHeading: "30-Day Return Policy",
  content1:
    "You can initiate a 100% cashback return of the Diamond Jewellery order within ",
  subHeading2: "T&C on the return",
  lists: [
    "Please note that in case you have received the order, then return or exchange is possible only if you start the return/exchange procedure on Gemlay website or mobile application within 30 working days.",
    "After an exhaustion of 30 days, the option to return will be disabled from the website and mobile application. Moreover, a return or exchange facility is only available within India. For international orders, this facility is not available.",
    "For post-30-Days orders, you must follow the Lifetime exchange or Life Time Buyback procedure.",
    "The invoice of the order must be with you!",
    "The box in which you received your Diamond/Gold Jewellery must be intact, not damaged.",
    "The return is totally free, with no extra shipping cost.",
    "Hand over the jewellery to the pick-up guy after verifying his details we will send you.",
    "The original Certificate of the Diamond Jewellery must be returned for verification.",
    "If the jewellery is found damaged by the pick-up guy, it will not be taken for return or exchange.",
    "The jewellery once received by us will undergo a checkup procedure to see if it had been worn or damaged. In case of a failure of the check pass, no return will be provided.",
    "If any discount or coupon had been applied to the original price, then the corresponding amount will be deducted. Hence, you will get only what you paid. No more or less!",
    "Any product purchased by leveraging Life time exchange will not be eligible for the 30-Days return.",
    "30-Days return or exchange is not applicable for the Diamond Jewellery you received after exchanging the prior received product through a 30-Days exchange.",
    "Orders paid through COD, will get the refund in the Bank Account.",
    "Orders paid through Credit card/Debit card/ Net Banking/UPI will be refunded to the source.",
    "Orders paid through International Credit/Debit cards are eligible for a 30-Days exchange only.",
  ],
  tableData1: [
    {
      title: "Diamond and Gold Jewellery",
      content1: ["Only as per invoice.", "Upto Rs. 1,50,000"],
      content2: "No Charges",
    },
    {
      title: "Damaged Jewellery",
      content1: ["NO Refund/Exchange"],
      content2: "NA",
    },
    {
      title: "Gold Coin",
      content1: ["Only as per invoice.", "Upto Rs. 1,50,000"],
      content2: "No Charges",
    },
    {
      title: "Jewellery Without Certificate/Invoices",
      content1: ["10-20% will be deducted"],
      content2: "No Charges",
    },
    {
      title: "Engraved Jewellery",
      content1: ["NO Refund/Exchange"],
      content2: "NA",
    },
  ],
  tableData2: [
    {
      title: "Diamond and Gold Jewellery",
      content1: [
        "100% of the current Gold Cost.",
        "100% of the current Diamond Cost.",
        "80-95% of the Current Gemstone cost.",
        "100% of the Current Solitaire Cost.",
        "Upto Rs. 1,50,000 worth Product.",
      ],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Loose Solitaire",
      content1: [
        "90% of the current diamond Cost on presence of original certificate.",
        "Upto Rs. 2,50,000 worth Product.",
        "Cost of certificate making will be deducted if not present.",
      ],
      content3: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Gold Coin",
      content1: ["100% of the current Gold Cost."],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Jewellery Without Certificate/Invoices",
      content1: ["10-20% will be deducted"],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Engraved Jewellery",
      content1: [
        "100% of the current Gold Cost.",
        "100% of the current Diamond Cost.",
        "80-95% of the Current Gemstone cost.",
        "100% of the Current Solitaire Cost.",
        "Upto Rs. 1,50,000 worth Product.",
      ],
      content2: "Rs. 500-1000 (as per city)",
    },
  ],
  subHeading3: "Lifetime Exchange",
  content2:
    "We arrange the pickup you pick the new Diamond or Gold Jewellery and get the brand new product delivered to your home. A hassle-free procedure.",
  lists2: [
    "Hand over the jewellery to the pick-up guy after verifying his details we will send you.",
    "The original Certificate of the Diamond Jewellery must be returned for verification.",
    "The jewellery once received by us will undergo a checkup procedure to check the authenticity. In case of a failure of the check pass, no exchange/return will be provided.",
    "If any discount or coupon had been applied to the original price, then the corresponding amount will be deducted.",
    "If any discount or coupon had been applied to the gold price, then the corresponding amount will be deducted.",
    "If any discount or coupon had been applied to the making price, then the corresponding amount will not be deducted.",
    "Any product purchased by leveraging Life time exchange will not be eligible for the 30-Days return.",
    "Lifetime exchange/ Buy back is calculated after deducting the making charges and other taxes.",
  ],
  subHeading4: "Lifetime Buy Back",
  content4:
    "At Gemlay, we offer lifetime buy back on the jewellery you bought from us. We arrange the pickup, you pick the new Diamond or Gold Jewellery and get the brand new product delivered to your home. A hassle-free procedure.",
  tableData3: [
    {
      title: "Diamond and Gold Jewellery",
      content1: [
        "100% of the current Gold Cost.",
        "100% of the current Diamond Cost.",
        "80-95% of the Current Gemstone cost.",
        "100% of the Current Solitaire Cost.",
        "Upto Rs. 1,50,000 worth Product.",
        "Additional 5-10% will be deducted from the overall cost.",
      ],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Loose Solitaire",
      content1: [
        "80% of the current diamond Cost on presence of original certificate.",
        "Upto Rs. 2,50,000 worth Product.",
        "Cost of certificate making will be deducted if not present.",
      ],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Gold Coin",
      content1: ["100% of the current Gold Cost."],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Jewellery Without Certificate/Invoices",
      content1: ["10-20% will be deducted"],
      content2: "Rs. 500-1000 (as per city)",
    },
    {
      title: "Engraved Jewellery",
      content1: [
        "100% of the current Gold Cost.",
        "100% of the current Diamond Cost.",
        "80-95% of the Current Gemstone cost.",
        "100% of the Current Solitaire Cost.",
        "Upto Rs. 1,50,000 worth Product.",
        "Additional 5-10% will be deducted from the overall cost.",
      ],
      content2: "Rs. 500-1000 (as per city)",
    },
  ],
  lists4: [
    "Hand over the jewellery to the pick-up guy after verifying his details we will send you.",
    "The original Certificate of the Diamond Jewellery must be returned for verification.",
    "The jewellery once received by us will undergo a checkup procedure to check the authenticity. In case of a failure of the check pass, no exchange/return will be provided.",
    "If any discount or coupon had been applied to the original price, then the corresponding amount will be deducted.",
    "If any discount or coupon had been applied to the gold price, then the corresponding amount will be deducted.",
    "If any discount or coupon had been applied to the making price, then the corresponding amount will not be deducted.",
    "Any product purchased by leveraging Life time exchange will not be eligible for the 30-Days return.",
    "Lifetime exchange/ Buy back is calculated after deducting the making charges and other taxes.",
  ],
};
