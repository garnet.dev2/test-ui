export const GOLD_RATE = {
  mainHeading: "GOLD RATE",
  subHeading1:
    "The Ever-Glittering Tale of Gold Rates: A Cornerstone of India's Market",
  content1:
    "Gold has held a special place in the hearts and minds of people across cultures and civilisations for centuries. In India, the significance of gold transcends mere adornment; it is deeply intertwined with tradition, culture, and economic prosperity. In this blog, we delve into the dynamic world of gold rates and explore why gold holds such immense importance in the Indian market.",
  subHeading2: "Understanding Gold Rates:",
  content2:
    "Gold rates, also known as gold prices or gold values, refer to the current market price of gold per unit of weight, typically measured in grams or ounces. These rates fluctuate daily based on various factors such as global demand and supply, geopolitical tensions, inflation, currency fluctuations, and economic indicators.",
  subHeading3: "Factors Influencing Gold Rates",
  content3: [
    {
      boldText: "Global Economic Conditions:",
      content:
        "Gold is often viewed as a safe-haven asset during times of economic uncertainty or instability. As a result, gold rates tend to rise during periods of geopolitical tensions, economic downturns, or currency devaluations.",
    },
    {
      boldText: "Inflation and Currency Movements:",
      content:
        "Gold is considered a dodge against inflation, as its value tends to surge when the purchasing power of currencies declines. Changes in currency exchange rates also impact gold rates, as gold is traded globally in US dollars.",
    },
    {
      boldText: "Central Bank Policies: ",
      content:
        "Monetary policies implemented by central banks, such as interest rate adjustments and quantitative easing measures, can influence gold rates by affecting investor sentiment and market liquidity.",
    },
    {
      boldText: "Demand and Supply Dynamics:",
      content:
        "Demand for gold is driven by various factors, including jewelry consumption, investment demand, central bank purchases, and industrial usage. Fluctuations in demand and supply levels can impact gold rates significantly.",
    },
    {
      boldText: "The Importance of Gold in the Indian Market:",
      content:
        "In India, gold holds immense cultural, social, and economic significance, making it an integral part of the country's market dynamics. Here's why gold plays a pivotal role in the Indian market.",
    },
    {
      boldText: "Cultural and Religious Significance:",
      content:
        "Gold has deep-rooted cultural and religious significance in India, where it is associated with auspicious occasions, festivals, weddings, and rituals. Gold jewelry is often passed down through generations and is considered a symbol of prosperity and good fortune.",
    },
    {
      boldText: "Store of Value and Wealth Preservation:",
      content:
        "Gold has historically served as a reliable store of value and a means of wealth preservation in India. Many Indian households invest in gold as a form of savings and financial security, particularly in rural areas where access to formal banking services may be limited.",
    },
    {
      boldText: "Jewellery Consumption:",
      content:
        "India is one of the largest consumers of gold jewellery globally, with a rich tradition of intricate designs and craftsmanship. Demand for gold jewelry remains robust throughout the year, driven by cultural preferences, gifting traditions, and festive celebrations.",
    },
    {
      boldText: "Investment Demand:",
      content:
        "Gold is also widely regarded as an investment asset in India, offering diversification benefits and a hedge against inflation and currency depreciation. Investors can purchase gold in various forms, including physical gold (jewelry, coins, bars) and financial instruments (gold ETFs, sovereign gold bonds).",
    },
    {
      boldText: "Monetary Reserves and Central Bank Holdings:",
      content:
        "India's central bank, the Reserve Bank of India (RBI), holds significant gold reserves as part of its foreign exchange reserves. Gold reserves serve as a crucial asset for maintaining financial stability, supporting the value of the Indian rupee, and diversifying reserve assets.",
    },
  ],
  subHeading4: "Conclusion",
  content4:
    "In conclusion, gold rates play a vital role in shaping market dynamics and investor sentiment in India. Beyond its economic significance, gold holds a special place in the hearts of millions of Indians, symbolising tradition, culture, and prosperity. As the glittering metal continues to captivate minds and markets alike, its timeless allure and enduring value remain undiminished in the fabric of India's market landscape.",
};

export const PAYMENT_OPTIONS = {
  mainHeading: "VARIOUS PAYMENT OPTIONS",
  subHeading1: "Simplify Your Shopping: Exploring Payment Options at Gemlay",
  content1:
    "At Gemlay, we understand the importance of convenience and flexibility when it comes to shopping for your favourite jewellery pieces. That's why we offer a variety of secure and convenient payment options to suit your preferences. Whether you're purchasing a stunning diamond necklace or a timeless gold ring, you can rest assured that your payment experience will be seamless and hassle-free.",
  content2: [
    {
      boldText: "Net Banking:",
      content:
        "With our net banking option, you can make payments directly from your bank account, eliminating the need for additional cards or payment gateways. Simply log in to your bank's internet banking portal and complete the transaction with ease.",
    },
    {
      boldText: "Indian Credit/Debit Cards:",
      content:
        "We accept all major Indian credit and debit cards, including Visa, Mastercard, and American Express. Simply enter your card details at checkout, and your payment will be processed securely through our encrypted payment gateway.",
    },
    {
      boldText: "International Credit/Debit Cards (with Minimal Fee):",
      content:
        "For our international customers, we offer the convenience of paying with international credit and debit cards. While a minimal fee may apply for international transactions, rest assured that your payment will be processed securely and efficiently.",
    },
    {
      boldText: "UPI Payment:",
      content:
        "For those who prefer the convenience of UPI payments, we offer seamless integration with popular UPI apps such as Google Pay, PhonePe, and Paytm. Simply scan the QR code or enter the UPI ID provided at checkout, and complete your payment in seconds.",
    },
    {
      boldText: "Cash on Delivery (for Orders Less than Rs 50,000):",
      content:
        "For added convenience and flexibility, we also offer cash on delivery for orders less than Rs 50,000. Simply select the cash on delivery option at checkout, and pay for your purchase when it is delivered to your doorstep.",
    },
  ],
  content3:
    "At Gemlay, we prioritise your security and peace of mind. That's why all our payment options are encrypted and secure, ensuring that your personal and financial information remains safe at all times. So go ahead, indulge in the luxury of shopping for exquisite diamond and gold jewellery, knowing that your payment experience will be as seamless and delightful as the jewellery itself.",
};
