export const contentBottom: any = [
  {
    rings: {
      heading:
        "Unveiling the Brilliance: A Guide to Different Types of Diamond Rings",
      para1: [
        "Diamond rings hold a special place in the realm of jewellery, symbolizing love, commitment, and eternal beauty. From timeless solitaires to intricate floral designs, each type of diamond ring offers a unique expression of style and sophistication. In this comprehensive guide, we explore the diverse world of diamond rings, including solitaire rings, gents rings, floral rings, ring bands, lightweight rings, and cocktail rings, while highlighting the affordability and sustainability of lab-grown diamonds, particularly for solitaire designs.",
      ],
      subHeading1: [
        {
          header: "Different types of Rings",
          points: [
            "Solitaire Rings :- Solitaire diamond rings epitomize classic elegance with their single, dazzling diamond focal point. These rings are revered for their simplicity and timeless appeal, making them a popular choice for engagement rings. Whether set in platinum, white gold, or yellow gold, solitaire rings effortlessly exude sophistication and grace.",
            "Gents Rings :- Gents diamond rings offer a bold and masculine statement, featuring sleek designs and sturdy settings. These rings often incorporate larger, more angular diamonds or intricate patterns, catering to the unique tastes of men who seek both style and substance in their jewellery.",
            "Floral Rings :- Inspired by the beauty of nature, floral diamond rings showcase intricate designs resembling delicate blooms and foliage. These rings feature clusters of smaller diamonds arranged in floral patterns, evoking a sense of romance and whimsy. Floral rings are perfect for those who appreciate the timeless charm of nature-inspired jewellery.",
            "Ring Bands :- Diamond ring bands offer a modern and versatile twist on traditional designs, with diamonds encrusted along the entire circumference of the band. These rings exude glamour and sophistication, whether worn alone or paired with other rings for a stacked look. Ring bands come in various widths and styles, catering to individual preferences and tastes.",
            "Lightweight Rings :- Ideal for everyday wear, lightweight diamond rings feature delicate designs and minimalistic settings, ensuring comfort and ease of wear. These rings are perfect for those who prefer understated elegance and subtle sparkle in their jewellery, making them a versatile addition to any jewellery collection.",
            "Cocktail Rings :- Make a bold statement with cocktail diamond rings, featuring oversized center stones and intricate settings. These rings are designed to dazzle and captivate, commanding attention with their dramatic flair and eye-catching designs. Perfect for special occasions or evening events, cocktail rings add a touch of glamour and extravagance to any ensemble.",
          ],
        },
      ],
      para2: [
        "While traditional mined diamonds have long been the go-to choice for diamond rings, there is a growing trend towards lab-grown diamonds, particularly for solitaire designs. Lab-grown diamonds offer the same beauty, brilliance, and durability as mined diamonds but at a fraction of the cost. This affordability makes them an attractive option for those seeking high-quality diamond rings without the hefty price tag.",
        "In addition to being more affordable, lab-grown diamonds are also more sustainable and ethically sourced, making them a conscientious choice for environmentally conscious consumers. By opting for lab-grown diamond solitaire rings, individuals can enjoy the timeless elegance and symbolic significance of diamond jewellery while also supporting sustainable practices in the jewellery industry.",
        "In conclusion, diamond rings encompass a diverse array of styles and designs, each offering its unique allure and charm. Whether you prefer the classic simplicity of solitaire rings or the bold extravagance of cocktail rings, there is a diamond ring to suit every style and occasion. And with the affordability and sustainability of lab-grown diamonds, now more than ever, individuals can indulge in the luxury of diamond jewellery with a clear conscience and a lighter wallet.",
      ],
    },
    earrings: {
      heading: "Did you find your Diamond Earrings Online?",
      para1: [
        "Ring for the ears makes you look complete. When the shiny designs in gold and diamond studded or hanging on your earlobe, it gives you an edge over others around you. Just earrings with a matching dress amplifies your appearance. Consequently, you feel beautiful and confident.",
        "Buying Earrings Online For Women has been made simple and interesting by categorising them to taste and shapes on Gemlay. ",
        "Moreover, we have provided various other filters to select. Without explicating more let’s take you through a handy guide to earrings selection on our website. ",
      ],

      subHeading1: [
        {
          header: "Different types earrings",
          points: [
            "Hoops Earrings :- Traditionally these are known to be Bali. But, in the current scenario, Bali is a term only used for small hoops. Other than the small, in general, hoops are bigger rings that surround your ear lobes without touching them. Similarly, Semi-Hoops, as the name describes, are incomplete rings. They surround half of the ear lobes and hang around them.",
            "Stud Earrings :- In Northern India, these are also known as TOPS. It is because these top your ear lobes, stick to them. These are the best to compliment your everyday look. Solitaire studs are pure-love Diamond Earrings For Women.",
            "Dangler Earrings :- The small hanging elements of gold and diamond are called Dangler drops. While the ones that reach your shoulders are termed by us Shoulder drops. There are different types of other. danglers - tassel danglers, floral danglers, sway danglers, etc. The crux of all is that hanging ones come in the category of danglers.",
          ],
        },
        {
          header: "What ease you to Buy Gold Earrings Online on Garnet Lanee?",
          points: [
            "Price Filter :- Everybody has their own Budget. Hence, you can select a range that lies within your spending limit. ",
            "Gold Weight Filter :- For daily wear jewellery, you may prefer light earrings (may be below 1 gram). But for a party, you may want a heavier one.",
            "Design-based Filter :- Hoops, Danglers, Bali, studs and are the design filters you can choose from. Moreover, floral patterns, wavy patterns and more are other filters for picking up an earring pair. ",
            "There is no reason to wait now. You’ve got the guide and it is time to select. ",
          ],
        },
      ],
    },
    bracelets: {
      heading: "Brace your Beauty with Diamond Bracelet",
      para1: [
        "Brace your wrist with a beautiful Diamond and Gold Bracelet to enchant the crowd around you. We, Gemlay, welcome you to the paradise of ethereal design Bracelets.",
        "A dapper look with shiny Diamond Bracelets for Men adds X-factor to a burly man’s look. Similarly, a sheen dress with a dazzling Gold Bracelet for Women bought online from our website would enhance a lady’s grace.",
        "We don’t shun to just adults when it comes to our collection of Diamond and Gold Jewellery online shop. For kids, Gemlay has the cutest Bracelet collection, including Daily wear bracelets, floral bracelets, chained bracelets, and more.",
      ],
      subHeading1: [
        {
          header: "Why You Should Buy Gold Bracelets Online from us?",
          points: [
            "We render the facility of customization of any jewellery you choose. All Bracelets for Men, Women, and Kids are available in 14k and 18k gold. You can even choose the color of gold for your Diamond Bracelet – Yellow, Rose, or White. Upgrade the shine by opting for high-grade quality Diamonds or lab-grown diamonds, which can reduce the cost by 50-80%.",
            "No shipping charges are levied to deliver the Diamond Jewellery ordered online from us. The discounts on Gold Bracelets with Diamonds range from 30% to a humongous 50%, which you will find nowhere on any other Online Diamond Jewellery shop.",
            "Party or Daily wear, we have bracelets weight range between 3 grams to 30 grams of Gold. Bracelets for Ladies and Kids have the most variety of designs. Our Diamond Bracelet Designers are the best in the country, having years of gold jewellery design experience, giving us an edge over the rest of the jewellers in the market selling Gold Ornaments online.",
          ],
        },
      ],
      para2: [
        "It is practically impossible you would not buy it once you visit our website as we have a collection of the most expensive as well as most affordable Diamond Jewellery. The hurdle of choosing from a plethora of Bracelet Designs from our Online Jewellery has been sorted by providing your filters to choose easily. Lest you are still confounded, give us a call. And our experts are ever ready to guide you through.",
        "Let the wait end now! Click on the Buy Now button for the Diamond Bracelet you love the most.",
      ],
    },
    bangles: {
      heading:
        "Embrace Elegance: Exploring Gold and Diamond Bangles with Gemlay",
      para1: [
        "In the realm of timeless adornments, gold and diamond bangles stand as epitomes of elegance and grace. At Garnet Lanee, we offer a curated collection of exquisite bangles crafted to elevate your style quotient and adorn your wrists with sophistication.",
      ],
      subHeading1: [
        {
          header: "Craftsmanship and Quality:          ",
          points: [
            "Our gold and diamond bangles are crafted with precision and passion, reflecting the highest standards of craftsmanship. Available in 14k and 18k gold variants, each bangle is meticulously designed to exude opulence and charm. Whether you prefer the warm allure of yellow gold, the subtle sophistication of white gold, or the romantic allure of rose gold, we offer customization options to suit your preferences.",
          ],
        },
        {
          header: "Lab Grown Diamonds:",
          points: [
            "In our commitment to sustainability and innovation, we proudly offer the option of lab-grown diamonds for our bangles. These ethically sourced diamonds possess the same brilliance, sparkle, and allure as natural diamonds, while also offering a more affordable and environmentally conscious alternative.",
          ],
        },
        {
          header: "Customization and Personalization:",
          points: [
            "At Gemlay, we understand that every individual has unique tastes and preferences. That's why we offer customization options for our gold and diamond bangles, allowing you to create a piece that truly reflects your personality and style. From selecting the perfect gold karatage to choosing the ideal diamond size and quality, our team of experts is dedicated to bringing your vision to life.            ",
          ],
        },
        {
          header: "Timeless Elegance:",
          points: [
            "Whether worn as standalone statement pieces or stacked for a layered look, our gold and diamond bangles exude timeless elegance and sophistication. Perfect for both formal occasions and everyday wear, these versatile accessories add a touch of luxury to any ensemble, effortlessly elevating your look from day to night.",
          ],
        },
      ],
    },
    pendants: {
      heading: "Diamond Pendants – Choose what you love!",
      para1: [
        "A pendant is a personification of a feeling for someone, belief, or defining any other proclivity of you. It makes you recognisable amid the crowd. Thus, each one has different ways to express themselves. ",
        "The crux has been appropriately understood by us. That has enabled us to produce an elaborate range of Gold and Diamond Pendants for women, men and kids. Assortments we’re offering in pendants, you will find nowhere. ",
        "To bring transparency to the buying procedure of Diamond Pendant Online from Gemlay, proper authentication certificates of gold and diamonds will be sent to you. Thereby, at any time you wish to exchange any diamond jewellery, it would be done without any hassles.",
        "We also have a range of pendants that comes with its sister jewellery partner. This provision is for women and girls, who want to buy Diamond Pendant Set Online from Gemlay. The inference is – you will get a pendant, matching ring and earrings in a Gold Pendant Set for Women. ",
        "As we talked a lot about your liking, which inspires us to design beautiful gold pendants, let’s give you a brief idea of the range of designs we have",
      ],
      subHeading1: [
        {
          header: "Affection Gold Pendant",
          points: [
            "The name itself implies the inspiration of these pendants – LOVE. The major focus on these pendants is given to heart designs studded with lustrous diamonds. You are going to love our collections of heartthrob. Buy online Diamond Affection pendant for yourself or gift it to your special one from Gemlay.",
          ],
        },
        {
          header: "Bloom Diamond Pendant",
          points: [
            "Rose, marigold, dandelion and many other Bloom Gold pendants scintillated with precious diamonds to show your love for Mother Nature. ",
            ,
          ],
        },
        {
          header: "Holy Diamond Pendants",
          points: [
            "Om, Swastik, Khalsa, etc. You may believe in any representation of the god, which is one. Respecting your belief and beseech to keep the mighty close, Garnet Lanee presents you with an exclusive range of Godly Diamond Pendants.",
          ],
        },
        {
          header: "Other Diamond Pendants for women, men and kids",
          points: [
            "Sun sign Diamond Pendants, Anime Pendants, Initials Diamond Pendants and much more. You name it, and we are ready to deliver it to your home to see a big smile on your face. ",
          ],
        },
      ],
      para2: ["Wait No Long! Hunt your favourite from Gemlay today."],
    },
    mangalsutra: {
      heading:
        "Radiant Romance: Exploring Mangalsutras and Tanmaniyas with Gemlay",
      para1: [
        "In the tapestry of Indian weddings, the Mangalsutra and Tanmaniya are threads that weave together the sacred bond of marriage, adorned with the essence of tradition and eternal love. At Garnet Lanee, we embrace this cultural legacy by offering a diverse collection of Mangalsutras and Tanmaniyas, each meticulously crafted to capture the essence of timeless elegance and marital bliss",
      ],
      subHeading1: [
        {
          header: "The Significance of Mangalsutra and Tanmaniya:",
          points: [
            "The Mangalsutra holds a sacred place in Indian culture, symbolising the marital union and the lifelong commitment between husband and wife. Traditionally worn by married women, it is a timeless emblem of love, respect, and devotion. Similarly, the Tanmaniya, with its delicate design and shimmering diamonds, embodies the grace and beauty of a married woman, enhancing her aura with understated elegance.",
          ],
        },
        {
          header: "Craftsmanship and Design:",
          points: [
            "At Gemlay, we take pride in the craftsmanship and artistry that goes into each Mangalsutra and Tanmaniya. Our skilled artisans delicately handcraft each piece, infusing it with intricate details and exquisite craftsmanship. Whether you prefer a traditional design with floral motifs, a lightweight variant for everyday wear, or a contemporary interpretation with modern elements, our collection offers a wide range of options to suit every bride's preferences.",
            ,
          ],
        },
        {
          header: "Customization Options:",
          points: [
            "We understand that every bride is different, and her jewellery should reflect her personality. That's why we offer customization options for our Mangalsutras and Tanmaniyas, allowing you to create a piece that is truly your own. From choosing the metal type and diamond quality to selecting the length and design elements, our expert team is dedicated to bringing your vision to life, ensuring that your jewellery is as special and unique as your love story.",
          ],
        },
      ],
    },
    necklaces: {
      heading: "Adorn Yourself: Exploring Diamond and Gold Necklaces",
      para1: [
        "A diamond and gold necklace is more than just an accessory; it's a statement of elegance, sophistication, and timeless beauty. At Garnet Lanee, we offer a stunning collection of diamond and gold necklaces, meticulously crafted to adorn you with the perfect blend of luxury and style. From lightweight designs to intricate florals, modern interpretations to traditional classics, our collection caters to every taste and occasion.",
      ],
      subHeading1: [
        {
          header: "Lightweight Necklaces:",
          points: [
            "Perfect for everyday wear, our lightweight necklaces are delicately crafted to provide effortless elegance. These dainty pieces are comfortable to wear and add a subtle touch of glamour to any outfit, whether you're dressing up for a special occasion or simply going about your day-to-day activities.",
          ],
        },
        {
          header: "Floral Necklaces:",
          points: [
            "Inspired by the beauty of nature, our floral necklaces feature intricate designs that capture the essence of blooming flowers. These enchanting pieces are adorned with sparkling diamonds and intricately crafted gold petals, creating a mesmerizing botanical-inspired aesthetic that's sure to turn heads.",
            ,
          ],
        },
        {
          header: "Modern Necklaces:",
          points: [
            "For the contemporary woman who loves sleek, minimalist designs, our modern necklaces offer the perfect blend of style and sophistication. With clean lines, geometric shapes, and innovative textures, these necklaces are designed to make a statement and elevate your look with modern flair.",
          ],
        },
        {
          header: "Traditional Necklaces:",
          points: [
            "Steeped in heritage and timeless elegance, our traditional necklaces pay homage to classic designs that have stood the test of time. From intricate Kundan and Polki sets to exquisite temple jewellery, these pieces exude regal charm and grace, making them perfect for weddings, festivals, and other special occasions.",
          ],
        },
      ],
    },
    solitaire: {
      heading: "Your Solo Solitaire Diamond Love",
      para1: [
        "Solitaire jewellery epitomises timeless elegance and sophistication, boasting a single, stunning diamond as its centrepiece. At Gemlay, our collection of solitaire jewellery transcends traditional boundaries, offering exquisite pieces meticulously crafted with the finest quality diamonds and precious metals. Whether you seek a classic solitaire ring, a graceful necklace, or a pair of captivating earrings, our selection promises unmatched beauty and versatility for any occasion.",
        "Our solitaire rings boast a variety of styles and settings, from the iconic solitaire setting to modern designs featuring intricate detailing. With options ranging from traditional round-cut diamonds to unique fancy shapes like princess, emerald, or pear, finding the perfect ring to complement your style and personality is effortless at Gemlay.",
        "In addition to rings, our solitaire necklaces and earrings exude understated glamour, featuring mesmerising diamonds set in sleek and stylish designs. Whether worn alone for a touch of elegance or layered with other pieces for added allure, our solitaire jewellery elevates any ensemble with its timeless allure and impeccable craftsmanship.",
        "At Gemlay, we recognize the growing preference for lab-grown diamonds, which offer ethical and sustainable alternatives to mined diamonds. Our collection includes exquisite lab-grown solitaire diamonds of exceptional quality, meticulously cultivated to showcase the same brilliance and fire as their natural counterparts. With lab-grown solitaires, you can adorn yourself with breathtaking beauty while making an eco-conscious choice.",
        "As you explore our stunning selection of solitaire jewellery, you may have questions about diamond quality, setting options, and care instructions. To address your queries, we've compiled a list of frequently asked questions (FAQs) below:",
      ],
    },
    nosepins: {
      heading: "Radiant Revelations: Adorn Your Nose with Diamond Elegance",
      para1: [
        "Diamond nosepins exude an enchanting allure, adding a touch of elegance and sophistication to any ensemble. At Gemlay, we offer a breathtaking collection of diamond nose pins crafted with precision and care, showcasing the natural brilliance and sparkle of these exquisite gemstones. Whether you prefer a classic diamond stud or a more intricate design, our nose pins are sure to captivate and elevate your look.",
        "Our diamond nose pins are available in a variety of styles and settings, allowing you to find the perfect piece to suit your unique taste and personality. From timeless solitaire studs to delicate floral designs, each nosepin is meticulously crafted to enhance your natural beauty and accentuate your features.",
        "In addition to their stunning aesthetics, our diamond nose pins are crafted with the highest quality materials to ensure durability and longevity. Each diamond is carefully selected for its exceptional clarity, colour, and cut, resulting in a piece of jewellery that radiates unmatched brilliance and allure.",
        "Whether you're looking for a timeless piece of jewellery to wear every day or a stunning accent for a special occasion, our collection of diamond nose pins offers something for every style and occasion. Browse our selection today and discover the perfect nosepin to adorn your nose with elegance and grace.",
      ],
    },
    chains: {
      heading: "Gleaming Elegance: Discover the Timeless Beauty of Gold Chains",
      para1: [
        "Gold chains are a timeless accessory that adds a touch of elegance and sophistication to any outfit. At Gemlay, we offer a stunning collection of gold chains crafted with precision and care, showcasing the natural beauty and allure of this precious metal. Whether worn alone as a statement piece or layered with other necklaces for a trendy look, our gold chains are designed to complement any style and occasion. Our collection features a variety of chain styles, each with its own unique characteristics and appeal. Some of the most popular types of gold chains include:",
      ],
      //   {
      //     // header: "Figaro Chain:           ",
      //     points: [
      //       "Figaro Chain :- This classic chain features alternating small circular links with larger elongated links, creating a distinctive and eye-catching pattern.            ",
      //     ],
      //   },
      //   {
      //     header: "Curb Chain:           ",
      //     points: [
      //       "With uniformly sized, interlocking links, the curb chain offers a sleek and sophisticated look that is perfect for everyday wear.",
      //       ,
      //     ],
      //   },
      //   {
      //     header: "Rope Chain:           ",
      //     points: [
      //       "Known for its twisted, rope-like appearance, this chain style exudes a sense of luxury and opulence, making it ideal for special occasions.",
      //     ],
      //   },
      //   {
      //     header: "Box Chain:           ",
      //     points: [
      //       "Characterised by square-shaped links that are joined together seamlessly, the box chain offers a clean and modern aesthetic that pairs well with both casual and formal attire.",
      //     ],
      //   },
      //   {
      //     header: "Singapore Chain:           ",
      //     points: [
      //       "Featuring tightly interwoven links that form a unique pattern resembling the coils of a spring, the Singapore chain offers a delicate and feminine look that is perfect for layering.",
      //     ],
      //   },
      //   {
      //     header: "Snake Chain: ",
      //     points: [
      //       "With its smooth, sleek design and flexible structure, the snake chain drapes effortlessly around the neck, creating a chic and understated look that is both versatile and stylish.",
      //     ],
      //   },
      // ],
      subHeading1: [
        {
          points: [
            "Figaro Chain :- This classic chain features alternating small circular links with larger elongated links, creating a distinctive and eye-catching pattern.",
            "Curb Chain :- With uniformly sized, interlocking links, the curb chain offers a sleek and sophisticated look that is perfect for everyday wear.",
            "Rope Chain :- Known for its twisted, rope-like appearance, this chain style exudes a sense of luxury and opulence, making it ideal for special occasions.",
            "Box Chain :- Characterised by square-shaped links that are joined together seamlessly, the box chain offers a clean and modern aesthetic that pairs well with both casual and formal attire.",
            "Singapore Chain :- Featuring tightly interwoven links that form a unique pattern resembling the coils of a spring, the Singapore chain offers a delicate and feminine look that is perfect for layering.",
            "Snake Chain :- With its smooth, sleek design and flexible structure, the snake chain drapes effortlessly around the neck, creating a chic and understated look that is both versatile and stylish."
          ],
        }
      ],

      para2: [
        "In addition to our diverse selection of chain styles, we also offer customization options to ensure that you find the perfect gold chain to suit your preferences. Whether you prefer a specific chain length, width, or gold karat, our experienced artisans will work with you to create a personalised piece that reflects your unique style and personality.",
        "As you explore our collection of gold chains, you may have questions about their care, durability, and other aspects. To address your queries, we've compiled a list of frequently asked questions (FAQs) below:"
      ],
    },
    "watch+accessory": {
      heading: "Enhance Your Elegance: Diamond and Gold Watch Accessories",
      para1: [
        "Discover the epitome of sophistication with Gemlay's collection of diamond and gold watch accessories. Elevate your timepiece to new heights of luxury with our exquisite range of accessories designed to complement your style and add a touch of glamour to any occasion.",
        "Our diamond and gold watch accessories are meticulously crafted with precision and attention to detail. Each piece is adorned with sparkling diamonds and crafted from the finest quality gold, ensuring timeless elegance and enduring beauty.",
        "From dazzling watch bands to elegant watch charms, our accessories are versatile and customizable, allowing you to create a unique look that reflects your individuality and taste. Whether you prefer classic designs or contemporary styles, our collection offers something for every discerning taste.",
      ],
    },
    stones: {
      heading: "Gemlay: Where Exquisite Diamonds, Gold, and Gemstones Converge",
      para1: [
        "Introducing Gemlay, the epitome of luxury and elegance in the world of fine jewellery. Our exquisite collection boasts an array of breathtaking pieces, each meticulously crafted with the finest diamonds, gold, and an assortment of precious gemstones.",
        "At Gemlay, we understand the allure of rare and captivating gemstones. Our skilled artisans expertly set stunning rubies, emeralds, sapphires, and more, creating designs that are truly one-of-a-kind. The depth of colour and brilliance of these precious stones is a testament to our unwavering commitment to quality and craftsmanship.",
        "Our diamond jewellery collection is a true masterpiece, featuring exceptional cuts, clarity, and radiance. From timeless solitaire rings to intricate tennis bracelets, each piece is a testament to the enduring beauty and elegance of diamonds. complemented by the warmth of luxurious gold settings, these pieces are designed to be cherished for generations.",
        "For those seeking a touch of colour and vibrancy, our gemstone jewellery collection is a true revelation. Vibrant rubies, captivating emeralds, and mesmerising sapphires adorn necklaces, earrings, and rings, creating a harmonious fusion of hues and textures. These pieces are not only visually stunning but also imbued with symbolic meanings and cultural significance, making them truly special investments.",
        "At Gemlay, we believe that jewellery is more than just an accessory; it is a reflection of one's personal style and a celebration of life's most precious moments. Our commitment to excellence ensures that every piece is a work of art, crafted with the utmost care and attention to detail.",
      ],
    },
    jewellery: {
      heading:
        " Discover the Timeless Elegance of Gemlay: A Guide to Diamond and Gold jewellery",
      para1: [
        "At Gemlay, we believe in celebrating life's moments with exquisite jewellery that reflects your unique style and personality. From dazzling rings to intricate pendants, each piece is meticulously crafted to adorn you with timeless elegance and sophistication. Let's explore the enchanting world of Gemlay jewellery, where luxury meets craftsmanship.",
      ],
      subHeading1: [
        {
          header: "Types of jewellery:",
          points: [
            "Rings:- Whether it's a symbol of eternal love or a fashion statement, Gemlay offers a stunning collection of diamond and gold rings. From classic solitaires to intricate designs, our rings are crafted to perfection, symbolising love and commitment.",
            "Earrings:- Adorn your ears with the shimmering beauty of Gemlay earrings. From elegant studs to glamorous hoops, our collection features a variety of designs to suit every occasion and style preference.",
            "Pendants:- Make a statement with a dazzling pendant from Gemlay. Whether you prefer a classic solitaire or a modern design, our pendants are crafted with precision and attention to detail, adding a touch of glamour to any outfit.",
            "Gold Coins:- Invest in timeless elegance with Gemlay gold coins. Crafted from the finest quality gold, our coins are a symbol of wealth and prosperity, making them a cherished addition to any collection.",
            "Mangalsutra:- Embrace tradition with Gemlay mangalsutras. Intricately designed and embellished with diamonds, our mangalsutras are a symbol of marital bliss and eternal love, perfect for celebrating the sacred bond of marriage.",
            "Necklaces:- Elevate your neckline with a stunning necklace from Gemlay. Whether it's a delicate chain or a statement piece, our necklaces are designed to dazzle and delight, adding a touch of luxury to any ensemble."
          ],

        },
      ],
      subHeading2:[
        {
          header:"Lab-Grown Diamonds:",
          points:[
            "At Gemlay, we are committed to ethical and sustainable practices. That's why we offer a selection of lab-grown diamonds that are indistinguishable from natural diamonds. These diamonds are created in a controlled environment, ensuring the same brilliance and beauty without the environmental impact."
          ]
        }
      ]
    },
    goldcoins: {
      heading: "Shine Bright with Gemlay's Exquisite Gold Coins",
      para1: [
        "Elevate your investment portfolio or gift a symbol of prosperity with Gemlay's stunning collection of gold coins. Crafted with precision and care, each coin embodies the timeless allure and intrinsic value of this precious metal.",
        "Our gold coins are available in a range of sizes and designs, from classic motifs to intricate patterns, ensuring there's something for every occasion and preference. Whether you're commemorating a special milestone, celebrating a cultural tradition, or simply adding to your collection, our gold coins make a meaningful and enduring statement.",
      ],
    },
  },
];

export const jewelleryFaqs: any = [
  {
    _id: 1,
    question: "Are lab-grown diamonds real diamonds?",
    answer: "Yes, lab-grown diamonds have the same chemical and physical properties as natural diamonds."
  },
  {
    _id: 2,
    question: "How do I care for my diamond and gold jewellery?",
    answer: "Clean your jewellery regularly with a mild detergent and a soft brush. Store them in a separate jewellery box to prevent scratches."
  },
  {
    _id: 3,
    question: "Can I customise the design of my jewellery?",
    answer: "Yes, Gemlay offers custom design services to create a unique piece tailored to your preferences."
  },
  {
    _id: 4,
    question: "What is the difference between 14k and 18k gold?",
    answer: "14k gold contains 58.3% pure gold, while 18k gold contains 75% pure gold, making it more valuable."
  },
  {
    _id: 5,
    question: "Do you offer financing options for purchasing jewellery?",
    answer: "No, we do not offer financing options."
  },
  {
    _id: 6,
    question: "Are your products ethically sourced?",
    answer: "Yes, we ensure that our diamonds and gold are sourced from reputable suppliers who adhere to ethical and sustainable practices."
  },
  {
    _id: 7,
    question: "Can I return or exchange my jewellery?",
    answer: "Yes, Gemlay offers a return or exchange policy within a specified timeframe, provided the jewellery is in its original condition."
  },
  {
    _id: 8,
    question: "Do you provide certification for your diamond jewellery?",
    answer: "Yes, all our diamond jewellery comes with a certificate of authenticity from reputable gemological laboratories."
  },
  {
    _id: 9,
    question: "How can I determine my ring size?",
    answer: "You can visit a jewellery store to get your ring size professionally measured, or use online ring size guides for accuracy."
  },
  {
    _id: 10,
    question: "Can I request a personalised inscription on my jewellery piece?",
    answer: "Yes, Gemlay offers engraving services to add a personal touch to your jewellery."
  },
]
