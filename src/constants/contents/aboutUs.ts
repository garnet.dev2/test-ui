export const OUR_VISION = {
  mainHeading: "OUR VISION",
  subHeading: "Vision To Scintillate the World",
  contents: [
    "Gemlay envisions a world where every individual, regardless of their purchasing power, can experience the joy of giving and receiving exquisite diamond and gold jewellery. At the heart of our brand lies the belief that the true value of jewellery lies not just in its material worth, but in the emotions it evokes and the memories it creates.",
    "We recognise that each one of us possesses the innate desire to bring happiness to our loved ones, to witness their smiles light up with joy. It is this sentiment that drives us to redefine the landscape of the jewellery industry and make luxury accessible to all.",
    "Our vision is to lead a revolution in the world of jewellery, where affordability meets unparalleled craftsmanship and timeless elegance. We strive to break barriers and democratise the experience of owning fine jewellery, ensuring that everyone has the opportunity to adorn themselves or their loved ones with pieces that exude beauty, sophistication, and sentimentality.",
    "With Gemlay, we promise to empower each and every individual to create beautiful memories that will endure for a lifetime. Whether it's a sparkling diamond ring to commemorate a special milestone, a radiant gold necklace to celebrate love and friendship, or a dazzling pair of earrings to uplift spirits, our jewellery serves as a symbol of cherished moments and meaningful connections.",
    "Our commitment goes beyond just offering exquisite pieces; it encompasses fostering a culture of inclusivity, empowerment, and celebration. We envision Gemlay as more than just a jewellery brand; we aspire to be a catalyst for joy, a conduit for love, and a beacon of elegance in the lives of our customers.",
    "As we embark on this journey, we invite you to join us in our mission to make luxury attainable, to redefine the essence of beauty, and to inspire moments of pure delight. With Gemlay, let us illuminate the world with the sparkle of joy and the radiance of love, one exquisite piece at a time.",
  ],
};

export const WHY_GEMLAY = {
  mainHeading: "WHY GEMLAY?",
  contents: [
    "“Dazzling you today, tomorrow and forever”",
    "With a vision to dazzle each and every person's life with celebration, making their special moments extra special, Gemlay is here with affordable real diamond and gold jewellery to make your special occasions scintillate.",
    "We're on a quest to make our jewellery buyable to every person regardless of what sect he/she belongs to.",
    "Is it really possible?",
    "Of course, it is. We have a range of designs that starts from a minimum 6.5 k, for natural diamonds, and for Lab grown diamonds reduced to 4.5 k minimum. Although, you may wonder if we're blabbering as other retailers never sell real diamonds at low costs. So, what enabled us to make it possible?",
  ],
  lists: [
    "We have our alliance manufacturing unit that enables us to cut the making charge cost to almost 50% less",
    "Minimum staff at our manufacturing and delivery units cuts down the cost, enabling us to reduce our price more than usual market prices.",
    "Gemlay owners and partners are designers and technical experts. That means the costliest affair has been taken care of by in-house people.",
  ],
  conslusion:
    "Viola! The above all sums up a REDUCED COST than the market Activity .",
};

export const OUR_STORY = {
  mainHeading: "OUR STORY",
  c1: "Established in January 2021, Gemlay is an online and offline jewellery shop delivering exclusive diamond-studded gold jewellery (Natural and Lab Grown Diamonds). Our brilliant clan of craftsmen, young and as well as experienced, work in synchronicity to make designs that are",
  c2: "Without any doubt, the sole credit goes to the amalgamation of youth and experienced staff to create jewellery that literally anyone can buy to wear, and stand out from the crowd.",
  c3: "Well aware of the emotions attached to celebrations of different occasions, jewellery doesn't solely symbolise asset holding. Rather, it is the love and emotion one expresses for the close ones.",
  lists: [
    "Luxurious yet Economical",
    "Modern yet Evergreen",
    "Simple yet Artistic",
  ],
};

export const OUR_TEAM = {
  mainHeading: "OUR TEAM",
  contents: [
    "An offspring of a 23-year-old Gold jewellery Brand - A K Jewellers (in Sirhind Punjab) - Gemlay is a new-age shopping stop to buy precious ornaments safely.",
    "Gemlay has been started by a young man who has been in the jewellery-making and retail business, for years (ancestral inheritance). Continuing his legacy and goodwill, Ashish Sood, has stepped up with all learning and firm support to expand the business all over India and further.",
    "He had done a diploma in diamond jewellery making from a renowned institute and worked as an intern to get hands-on experience in dealing with customers and retail services.",
  ],
};
