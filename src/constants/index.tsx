export const AWS_S3_URL = process.env.NEXT_PUBLIC_AWS_S3_BUCKET_URL;
export const API_BASE_URL = process.env.NEXT_PUBLIC_API_URL;
export const NEXT_PUBLIC_API_NAME = process.env.NEXT_PUBLIC_API_NAME;
export const BEARER_TOKEN = "BEARER_TOKEN";
export const AUTH_DATA = "AUTH_DATA";
export const AUTH_HEADER_NAME = "Authorization";
export const SECRET_KEY = process.env.SECRET_KEY;
export const CLIENT_URL = process.env.CLIENT_URL;
export const API_NAME = process.env.NEXT_PUBLIC_API_NAME;
export const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const toastSettings2: any = {
  position: "bottom-center",
  autoClose: 1500,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: false,
  progress: undefined,
  theme: "dark",
};

export const toastSettings: any = {
  duration: 2000,
  position: "bottom-center",

  // Styling
  style: {
    border: '1px solid green',
    padding: '16px',
    background: '#dcffdc',
    color: 'green',
  },
  className: "",

  // Custom Icon
  // icon: "👏",

  // Change colors of success/error/loading icon
  iconTheme: {
    primary: "#007A64",
    secondary: "#fff",
  },

  // Aria
  ariaProps: {
    role: "status",
    "aria-live": "polite",
  },
};
