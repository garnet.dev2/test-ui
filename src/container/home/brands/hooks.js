import { api, apiEndPoints } from "@/api";
import useForm from "@/hooks/useForm";
import { useMutation } from "react-query";
import toast from "react-hot-toast";
import { toastSettings } from "@/constants";

export const subscription = async (payload) => {
  const { data } = await api.post(apiEndPoints.subscribe, payload);
  return {
    data,
  };
};

const useSubscription = () => {
  const { form, setForm, handleChange } = useForm();

  const submitChanges = () => {
    if (form?.email && form?.gender) {
      subscribe(form);
    } else {
      toast.error(`Please fill in all Details`, toastSettings);
    }
  };

  const { mutate: subscribe } = useMutation(
    (payload) => subscription(payload),
    {
      onSuccess: (data) => {
        if (data?.data?.isSuccess) {
          toast.success(`Thank you for Subscribing!`, toastSettings);
        } else {
          toast.error(`${data?.data?.msg}`, toastSettings);
        }
      },
    }
  );

  return { submitChanges, form, handleChange, setForm };
};

export default useSubscription;
