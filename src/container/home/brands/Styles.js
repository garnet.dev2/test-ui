import styled from "styled-components";

export const Brand = styled.div`
  padding: 50px 0px;
  /* background: linear-gradient(
    180deg,
    rgba(248, 248, 248, 0.59) 0%,
    rgba(248, 248, 248, 0.53) 100%
  ); */
  background: linear-gradient(180deg, #FAFAFA 0%, #FAFAFA 81%, #ecfff7a3 100%);
  margin-bottom: 5px;
  .product-card-title {
    display: inline-block;
    width: 100%;
    height: 60px;
    margin-bottom: 20px;
    .title {
      background: #fbfbfb;
    }
  }
  @media only screen and (max-width: 640px) {
    padding: 30px 0px;
    .product-card-title {
      height: 28px;
      margin-bottom: 10px;
    }
  }
`;

export const BrandsInfo = styled.div`
  border-radius: 10px;
  border: 1px solid ${(props) => props.theme.colors.orange};
  background: ${(props) => props.theme.colors.white};
  padding: 30px;
  margin: 15px;
  display: flex;
  gap: 30px;
  flex-flow: column;
  align-items: center;
  .brand-logo {
    height: 70px;
    display: flex;
    /* width: 200px; */
    align-items: center;
  }

  .brand-logo .full-size img {
    width: 100%;
    height: 100%;
  }

  .brand-info {
    color: #302f2f;
    text-align: center;
    font-size: 14px;
    font-weight: 500;
    line-height: 20px; /* 76.923% */
    letter-spacing: 1px;
  }
  .read-more {
    background: ${(props) => props.theme.colors.secondary};
    border-radius: 5px;
    color: ${(props) => props.theme.colors.white};
    display: inline-block;
    height: 45px;
    line-height: normal;
    &:hover {
      background: ${(props) => props.theme.colors.orange};
    }
  }

  @media only screen and (max-width: 767px) {
    padding: 15px;
    margin: 8px;
  }
`;

export const Video = styled.div`
  padding: 50px 0px;
  position: relative;
  .video-icon {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  @media only screen and (max-width: 640px) {
    padding: 30px 0px;
  }
`;
export const SignupForm = styled.div`
  padding: 50px 0px;
  background: linear-gradient(
    180deg,
    rgba(248, 248, 248, 0.59) 0%,
    rgba(248, 248, 248, 0.53) 100%
  );
  position: relative;
  margin-bottom: 3px;
  .form-info {
    display: flex;
    align-items: center;
    gap: 30px;
    .left,
    .right {
      width: 50%;
    }
    h2 {
      color: ${(props) => props.theme.colors.orange};
      font-size: 20px;
      font-weight: 600;
      line-height: 18px;
      letter-spacing: 1px;
      margin-bottom: 20px;
    }
    span {
      color: #302f2f;
      font-size: 13px;
      font-weight: 500;
      line-height: 20px;
    }
    .field {
      display: flex;
      align-items: center;
      justify-content: center;
      gap: 20px;
      margin-bottom: 20px;
      &.mobile_only {
        display: none;
      }
      .email-input {
        border-radius: 10px;
        border: 1px solid #e6e6e6;
        background: rgba(255, 255, 255, 0.86);
        height: 45px;
        width: 100%;
        padding: 15px;
        color: #302f2f;
        &::placeholder {
          color: #302f2f;
          font-size: 11px;
          font-weight: 500;
          line-height: normal;
        }
      }
      button {
        border-radius: 10px;
      }
    }
    .radio-field {
      display: flex;
      align-items: center;
      gap: 15px;
      position: relative;
      cursor: pointer;
      input[type="radio"] {
        position: absolute;
        left: 0;
        z-index: 9;
        width: 100%;
        height: 20px;
        opacity: 0;
        &:not(:checked) + label:after {
          opacity: 0;
          -webkit-transform: scale(0);
          transform: scale(0);
        }
        &:not(:checked) {
          position: absolute;
          left: 0;
          z-index: 9;
          width: 100%;
          height: 20px;
          opacity: 0;
        }
        &:checked + label:after {
          opacity: 1;
          -webkit-transform: scale(1);
          transform: scale(1);
        }
      }
      label {
        color: #302f2f;
        font-size: 13px;
        font-weight: 500;
        line-height: normal;
        padding-right: 30px;
        position: relative;
        cursor: pointer;
        display: flex;
        align-items: center;
        gap: 10px;
        &:before {
          content: "";
          position: absolute;
          right: 0;
          top: 4px;
          width: 16px;
          height: 16px;
          border: 1px solid #ddd;
          border-radius: 100%;
          background: ${(props) => props.theme.colors.white};
        }
        &:after {
          content: "";
          width: 8px;
          height: 8px;
          background: ${(props) => props.theme.colors.orange};
          position: absolute;
          top: 8px;
          right: 4px;
          border-radius: 100%;
          -webkit-transition: all 0.2s ease;
          transition: all 0.2s ease;
        }
      }
    }
  }
  .mobile_only {
    display: none;
  }
  .desktop_only {
    display: block;
  }
  @media only screen and (max-width: 767px) {
    margin: 20px auto 0px;
    padding: 0px;
    .mobile_only {
      display: block;
    }
    .desktop_only {
      display: none;
    }
    .form-info {
      gap: 15px;
      flex-wrap: wrap;
      .radio-field label {
        gap: 5px;
        padding-right: 40px;
        &:before {
          top: 3px;
          width: 13px;
          height: 13px;
        }
        &:after {
          top: 7px;
          width: 5px;
          height: 5px;
        }
      }
      .left {
        display: flex;
        align-items: center;
        width: 100%;
        h2 {
          margin: 0px;
          width: 60%;
          padding-right: 10px;
          font-size: 14px;
        }
        .field {
          margin: 0px;
          width: 30%;
          display: flex;
          flex-flow: column;
          gap: 5px;
          .radio-field {
            width: 100%;
            img {
              width: 17px;
            }
          }
        }
        .line {
          position: relative;
          width: 55%;
          margin: 0px 15px;
          &:before {
            content: "";
            height: 1px;
            width: 100%;
            background: #ccc;
            position: absolute;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
          }
        }
      }
      .right {
        width: 100%;
        .desktop_only {
          display: none;
        }
        .field {
          margin: 0px;
        }
      }
    }
  }
  @media only screen and (max-width: 640px) {
    padding: 30px 0px;
  }
`;
