/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-unescaped-entities */
"use client";
import Slider from "react-slick";
import { BrandsInfo, SignupForm, Brand } from "./Styles";
import { BtnWhite, DiscoverBtn } from "../../../styled/button/index";
import SocialMedia from "../socialmedia/social";
import Collection from "@/container/home/collection";
import Link from "next/link";
import Image from "next/image";
import useSubscription from "./hooks";
import { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";

export default function Brands() {
  const { form, handleChange, submitChanges, setForm } = useSubscription();

  const [selectedOption, setSelectedOption] = useState("");

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
    setForm({ ...form, gender: event.target.value });
  };

  var settings = {
    mobileFirst: true,
    slidesToShow: 4,
    speed: 2000,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 7000,
    arrows: false,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: "50px",
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 3,
          swipeToSlide: true,
        },
      },
    ],
  };

  const BrandsImages = [
    {
      image:
        "https://bsmedia.business-standard.com/include/_mod/site/html5/images/business-standard-logo.png",
      info: "Gemlay brings range of diamonds jewellery at affordable prices.",
      url: "https://www.business-standard.com/content/press-releases-ani/garnet-lanee-brings-range-of-diamonds-jewellery-at-affordable-prices-122092300439_1.html",
    },
    {
      image: "https://fullforms.com/images/image/ANI_5911.jpg",
      info: "Gemlay is a new age shopping stop to buy precious ornaments.",
      url: "https://www.aninews.in/news/business/business/garnet-lanee-brings-range-of-diamonds-jewellery-at-affordable-prices20220923120032/",
    },
    {
      image: "https://aniportalimages.s3.amazonaws.com/media/details/IEO.png",
      info: "A renaissance in the world of Diamond and Gold Jewellery is on its way.",
      url: "https://www.indianeconomicobserver.com/news/garnet-lanee-brings-range-of-diamonds-jewellery-at-affordable-prices20220923120027/",
    },
    {
      image: "https://www.webindia123.com/shared_files/ssl/images/logo.png",
      info: "Our products are our backbone. No compromise in design and quality.",
      url: "https://news.webindia123.com/news/articles/Business/20220923/3986106.html",
    },
    {
      image:
        "https://yt3.googleusercontent.com/tIorpmB0LjZQfWMeUzj829GAQtiYMjLFMLj-TWsECQfltwZsTqa2Ar1U9M9oYV8AUhyQ8tYx9A=s176-c-k-c0x00ffffff-no-rj",
      info: "Launch of Gemlay one of the biggest online store for jewellery.",
      url: "https://www.youtube.com/watch?v=ptgGHjE9qKw&themeRefresh=1",
    },
  ];

  const router = useRouter();
  const scrollPressToDiv = () => {
    const section = document.getElementById("press-release");
    window.scrollTo({
      top: section.offsetTop + 400,
      behavior: "smooth",
    });
  };

  const { key } = router.query;

  const scrollToDiv = () => {
    // router.push("/return-and-refund-policy");

    reviewRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    if (key == "press-release") {
      scrollPressToDiv();
    }
  }, [router]);

  return (
    <>
      <Brand id="press-release">
        <div className="container" >
          <div className="product-card-title">
            <Collection
              heading="WE GOT FEATURED HERE"
              title="EXPLORE OUR PRESENCE"
            />
          </div>
          <Slider {...settings}>
            {BrandsImages?.map((Brands, key) => {
              return (
                <div key={key}>
                  <BrandsInfo>
                    <div
                      className={`brand-logo${key === 1 ? " full-size" : ""}`}
                      // ref={pressReleaseRef}
                    >
                      <img src={Brands?.image} />
                    </div>
                    <div className="brand-info">{Brands?.info}</div>
                    <Link href={Brands?.url} target="_blank">
                      <BtnWhite className="read-more">Read More</BtnWhite>
                    </Link>
                  </BrandsInfo>
                </div>
              );
            })}
          </Slider>
        </div>
      </Brand>

      <SignupForm>
        <div className="container">
          <div className="form-info">
            <div className="left">
              <h2 className="mobile_only">GEMLAY INSIDER</h2>
              <div className="line mobile_only">&nbsp;</div>
              <div className="field mobile_only">
                <div className="radio-field">
                  <input
                    type="radio"
                    id="radio1"
                    name="gender"
                    value="Female"
                    checked={selectedOption === "Female"}
                    onChange={handleOptionChange}
                  />
                  <label id="radio1">
                    <Image src="../assets/woman.svg" width={25} height={23} />
                    Female
                  </label>
                </div>
                <div className="radio-field">
                  <input
                    type="radio"
                    id="radio2"
                    name="gender"
                    value="Male"
                    checked={selectedOption === "Male"}
                    onChange={handleOptionChange}
                  />
                  <label id="radio2">
                    <Image src="../assets/man.svg" width={25} height={23} />
                    Male
                  </label>
                </div>
              </div>
              <h2 className="desktop_only">SIGN UP TO BE A GEMLAY INSIDER</h2>
              <span className="desktop_only">
                Get Promotions and the latest news directly in your inbox
              </span>
            </div>
            <div className="right">
              <form>
                <div className="field">
                  <input
                    type="email"
                    placeholder="Email Address"
                    className="email-input"
                    name="email"
                    value={form?.email}
                    onChange={handleChange}
                  />
                  <DiscoverBtn onClick={() => submitChanges()}>
                    SUBMIT
                  </DiscoverBtn>
                </div>
                <div className="field desktop_only">
                  <div className="radio-field">
                    <input
                      type="radio"
                      id="radio1"
                      name="gender"
                      value="Female"
                      checked={selectedOption === "Female"}
                      onChange={handleOptionChange}
                    />
                    <label id="radio1">
                      <Image src="../assets/woman.svg" width={25} height={23} />
                      Female
                    </label>
                  </div>
                  <div className="radio-field">
                    <input
                      type="radio"
                      id="radio2"
                      name="gender"
                      value="Male"
                      checked={selectedOption === "Male"}
                      onChange={handleOptionChange}
                    />
                    <label id="radio2">
                      <Image src="../assets/man.svg" width={25} height={23} />
                      Male
                    </label>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </SignupForm>
      <div className="top_social">
        <SocialMedia />
      </div>
    </>
  );
}
