import styled from "styled-components";
import { Button } from "@nextui-org/react";

export const ProductGrid = styled.div`
  .product-grid-inner {
    margin: 12px;
  }

  @media only screen and (max-width: 425px) {
    .product-grid-inner {
      margin: 10px 5px;
    }
  }
`;

export const MainDiv = styled.div`
  display: grid;
  place-items: center;
`;

export const MainCard = styled.div`
  background-color: ${(props) => props.theme.colors.white};
  padding-bottom: 0;
  border-radius: 20px;
  margin-bottom: 0px;
  overflow: hidden;
  width: 100% !important;
  box-shadow: 0px 3px 12px 0px #0000001f;

  .slick-dots {
    bottom: 0;
    padding: 0px 10px;
    text-align: right;
    li {
      width: 5px;
      height: 5px;
      margin: 0 2px;
      button {
        line-height: 0;
        width: 5px;
        height: 5px;
        padding: 0;
        &:before {
          right: 10px;
          line-height: 5px;
          left: 0;
          width: 5px;
          height: 5px;
        }
      }
    }
  }
  @media only screen and (max-width: 767px) {
    height: auto;
    .gallery-slider-bottom {
      display: none;
    }
  }
  .gallery-slider-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    padding: 5px;
    right: 0;
    .arrows {
      display: flex;
      align-items: center;
      justify-content: flex-end;
      gap: 5px;
      z-index: 999;
    }
  }
`;

export const WishlistBtn = styled(Button).attrs({
  // size:'xs'
})`
  cursor: pointer;
  background: none;
  padding: 0;
  width: auto;
  z-index: 99;
  display: inline-block;
  min-width: auto;
  height: initial;

  @media only screen and (max-width: 370px) {
    .thumb {
      img {
        width: 20px;
      }
    }
  }
`;

export const UpperSection = styled.div`
  height: 250px;
  .product-photos {
    position: relative;
  }
  @media only screen and (max-width: 767px) {
    height: auto;
  }
  @media only screen and (max-width: 370px) {
    position: relative;
  }
  @media only screen and (min-width: 768px) {
    .hover-wishlist {
      .thumb {
        opacity: 0;
        visibility: hidden;
        transition: 0.5s ease-in-out;
      }
    }
    &:hover {
      .hover-wishlist {
        .thumb {
          opacity: 1;
          visibility: visible;
        }
      }
    }
  }
`;

export const ImgTopDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 30px;
  padding-top: 14px;
  padding-right: 0.5rem !important;
  z-index: 10;
  position: relative;
  .thumb {
    z-index: 5;
  }
  ${
    "" /* @media only screen and (max-width: 767px) {
    .thumb {
      height: 19px;
      img {
        width: 16px;
      }
    }
  } */
  }

  @media only screen and (max-width: 370px) {
    padding-top: 0;
    padding-right: 0 !important;
    position: absolute;
    top: 5px;
    right: 5px;
  }
`;

export const ImgContainer = styled.div`
  margin-top: -24px;
  overflow: hidden;

  .card-img {
    transition: filter 0.5s ease-in-out;
    filter: blur(8px);
    background: url('/listing_loader.webp') no-repeat center;
  }
  .card-img.loaded {
    filter: blur(0);
    transition: 0.5s ease-in-out;
  }
  @media only screen and (min-width: 768px) {
    .slick-dots {display: none !important;}
  }

  @media only screen and (max-width: 500px) {
    margin-top: 0px;
  }
`;

export const BottomCardName = styled.div`
  height: 91px;
  margin-top: 20px;
  .color-btn {
    display: flex;
    flex-flow: column;
    gap: 10px;
    .card-btn {
      height: 16px;
      width: 16px;
      display: block;
      background: red;
      border-radius: 100px;
      position: relative;
      border: 0px;
      padding: 0;
      &.yellow {
        background: #ffde6a;
      }
      &.grey {
        background: #c2c0c0;
      }
      &.orange {
        background: #dc8686;
      }
      &:before {
        content: "";
        border: 1px solid #ffde6a;
        width: 23px;
        height: 23px;
        position: absolute;
        border-radius: 100px;
        position: relative;
        border: 0px;
        padding: 0;
        &.yellow {
          background: #ffde6a;
        }
        &.grey {
          background: #c2c0c0;
        }
        &.orange {
          background: linear-gradient(
            89deg,
            ${(props) => props.theme.colors.orange} -7.64%,
            ${(props) => props.theme.colors.secondary} 88.49%
          );
        }
        &:before {
          opacity: 1;
          visibility: visible;
        }
      }
    }
  }
  @media only screen and (max-width: 767px) {
    height: 75px;
    margin-top: 0px;
    .color-btn {
      gap: 5px;
      .card-btn {
        height: 13px;
        width: 13px;
        &:before {
          width: 19px;
          height: 19px;
          top: -3px;
          left: -3px;
        }
      }
    }
  }
`;

export const TxtDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 9px;
`;

export const MinHeightDiv = styled.div`
  min-height: 23px;
`;

export const PriceHeading = styled.h1`
  font-size: 20px;
  color: #302f2f;
  font-weight: 500;
  display: flex;
  align-items: center;
  gap: 5px;
  margin: 0;
  .old-price {
    text-decoration: line-through;
    font-size: 14px;
  }
  @media only screen and (max-width: 767px) {
    font-size: 14px;
    .old-price {
      font-size: 10px;
    }
  }
`;

export const TxtDiv2 = styled.div`
  padding-left: 9px;
  padding-right: 9px;
  padding-top: 4px;
  display: flex;
  justify-content: space-between;

  .product-price-info {
    width: 80%;
  }
  .color-btn {
    display: flex;
    flex-flow: column;
    align-items: flex-end;
    gap: 10px;
    width: 20%;
  }
`;
export const PriceHeading2 = styled.h4`
  white-space: nowrap;
  display: block;
  text-overflow: ellipsis;
  overflow: hidden;
  font-size: 13px;
  color: #302f2f;
  line-height: 18px;
  font-weight: 500;
  margin: 0px;
  padding: 4px 0px;
  @media only screen and (max-width: 767px) {
    font-size: 11px;
    padding: 0;
  }
`;
export const BtnDivCard = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media only screen and (max-width: 370px) {
    flex-flow: column;
    .buy-now {
      width: 100%;
      a {
        width: 100%;
      }
    }
  }
`;

export const SmallTxtBottom = styled.div`
  padding-left: 6px;
  padding-top: 0;
  align-items: center;
  display: flex;
  gap: 5px;
  .thumb {
    width: 13px;
    height: 14px;
  }

  @media only screen and (max-width: 370px) and (min-width: 320px) {
    width: 100%;
  }
`;

export const SmallTxtBottomTxt = styled.span`
  color: rgb(48, 47, 47);
  font-weight: 500;
  font-style: normal;
  line-height: 18px;
  font-size: 13px;
  white-space: nowrap;
  @media only screen and (max-width: 767px) {
    font-size: 9px;
    vertical-align: middle;
    margin-left: 0px;
  }

`;
export const CardBottomBtn = styled.div`
  cursor: pointer;
  position: relative;
  height: 45px;
  width: 127px;
  @media only screen and (max-width: 767px) {
    height: 32px;
    width: 100px;
  }
`;

export const ImgTg = styled.img`
  height: 100% !important;
  width: 100% !important;
  position: absolute;
  transition: opacity 0.1s ease-in-out;
  &:hover {
    opacity: 0;
  }
  @media only screen and (max-width: 767px) {
    height: 110% !important;
  }
`;

export const BottomBtnSpan = styled.span`
  font-size: 13px;
  color: ${(props) => props.theme.colors.white};
  font-weight: 700;
  font-style: normal;
  line-height: 18px;
  position: absolute;
  top: 45%;
  left: 52%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  @media only screen and (max-width: 767px) {
    font-size: 11px !important;
  }
`;

export const GreenTxt = styled.span`
  color: #E72929;
  font-size: 11px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;
  letter-spacing: 1px;
  display: flex;
  @media only screen and (max-width: 767px) {
    font-size: 9px;
    vertical-align: top;
  }
`;

export const ViewSimilarStyle = styled.div`
  .similar-product {
    display: flex;
    flex-wrap: wrap;
  }
  .view-similar {
    display: none;
  }
  .view-similar-btn {
    justify-content: flex-end;
  }
  .filter-subtitle {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 25px;
    font-size: 25px;
    gap: 20px;
    background: linear-gradient(180deg, #007a64 0%, #012433 100%);
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }
  .product-main-card {
    width: 33.333%;
  }

  @media only screen and (max-width: 991px) {
    .product-main-card {
      width: 50%;
    }
    .filter-subtitle {
      display: flex;
      align-items: center;
      justify-content: center;
      padding: 25px;
      font-size: 25px;
      gap: 20px;
      background: linear-gradient(
        180deg,
        ${(props) => props.theme.colors.orange} 0%,
        ${(props) => props.theme.colors.secondary} 100%
      );
      background-clip: text;
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
    .product-main-card {
      width: 33.333%;
    }
  }

  @media only screen and (max-width: 425px) {
    .filter-subtitle {
      font-size: 14px;
      padding: 10px 0px;
    }
    .product-grid-inner {
      margin: 5px;
    }
    //--added--
    .product-main-card {
      width: 50%;
    }
  }
`;
