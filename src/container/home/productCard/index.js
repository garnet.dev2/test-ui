/* eslint-disable react/jsx-key */
/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
"use client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {
  MainDiv,
  MainCard,
  UpperSection,
  ImgTopDiv,
  ImgContainer,
  BottomCardName,
  PriceHeading,
  TxtDiv2,
  PriceHeading2,
  BtnDivCard,
  SmallTxtBottom,
  SmallTxtBottomTxt,
  ProductGrid,
  GreenTxt,
  WishlistBtn,
} from "./trending";
import { CardBadgeOne } from "../../../styled/button/index";
import { StyledProductButton } from "./style";
import { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { useDisclosure } from "@nextui-org/react";
import ViewSimillar from "./viewSimillar";
import { AWS_S3_URL, toastSettings } from "@/constants/index";
import useCardHook from "./hooks";
import { useRouter } from "next/router";
import { toIndianCurrency } from "@/utils/priceHelper";
import { findMissingKeys } from "@/utils/colorHelper";
import Image from "next/image";
import { useSelector } from "react-redux";
import socket from "../../../../socket";
import toast from "react-hot-toast";

export default function ProductCard(props) {
  const { product, cardKey: indx } = props;
  const {
    removeErrorImage,
    handleRecentlyView,
    viewSimilarData,
    setCategory,
    isFetching,
    handleWishlist,
    wishlistPids,
    checkItemInWishlist
  } = useCardHook(product);

  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [modalPlacement, setModalPlacement] = useState(0);
  const [imageType, setImageType] = useState(0);

  //  0=y,1-w,2-r
  const onViewSimillar = (type, slug) => {
    setModalPlacement(type);
    onOpen();
    setCategory(slug);
  };

  const Router = useRouter();

  const { slug } = Router?.query;

  useEffect(() => {
    if (slug?.split("-")[0] == "yellow") {
      setImageType(0);
    } else if (slug?.split("-")[0] == "white") {
      setImageType(1);
    } else if (slug?.split("-")[0] == "rose") {
      setImageType(2);
    }
  }, [Router.query]);

  const settingsMain = {
    dots: true,
    horizontal: true,
    infinite: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: false,
    horizontalSwiping: true,
    mobileFirst: true,
  };

  const [sliderImages, setsliderImages] = useState("");
  const [slider1, setSlider1] = useState(null);

  // console.log(wishlistPids, "wishihihih");

  useEffect(() => {
    if (imageType === 0) {
      setsliderImages("yellowGold");
    }
    if (imageType === 1) {
      setsliderImages("whiteGold");
    }
    if (imageType === 2) {
      setsliderImages("roseGold");
    }
  }, [imageType]);

  const prevSlide = () => {
    slider1.slickPrev();
  };

  const nextSlide = () => {
    slider1.slickNext();
  };

  const [imagesLoaded, setImagesLoaded] = useState(false);

  const handleImageLoad = () => {
    setImagesLoaded(true);
  };

  return (
    <>
      <ProductGrid className="product-main-card">
        <div className="product-grid" key={indx}>
          <div className="product-grid-inner">
            <MainDiv>
              <MainCard onClick={() => handleRecentlyView(product?._id)}>
                <UpperSection>
                  <ImgTopDiv className="hover-wishlist">
                    {product?.isTrending && (
                      <CardBadgeOne>TRENDING</CardBadgeOne>
                    )}
                    <div>&nbsp;</div>
                    <WishlistBtn>
                      <div className="thumb" onClick={() => handleWishlist()}>
                        {checkItemInWishlist(product?._id) ? (
                          <>
                            {Router.pathname === "/wishlist" ? (
                              <img
                                role="button"
                                src="/img/card/delete.svg"
                                width="20"
                                height="17.3"
                                // onClick={() => handleMoveToWishlist(product)}
                              />
                            ) : (
                              <>
                                {/* <BsFillHeartFill color="#FF5500" /> */}
                                <img
                                  src="/wishlist-fill.svg"
                                  width="30"
                                  height="30"
                                />
                              </>
                            )}
                          </>
                        ) : (
                          <img
                            src="/wishlist-circle.svg"
                            width="30"
                            height="30"
                          />
                        )}
                      </div>
                    </WishlistBtn>
                  </ImgTopDiv>
                  <div>
                    <div></div>
                  </div>
                  <div className="product-photos">
                    <Link
                      target="_blank"
                      href={`/${product?.category?.slug}/${product?.slug}`}
                    >
                      <ImgContainer>
                        <div className="">
                          <Slider
                            {...settingsMain}
                            ref={(slider) => setSlider1(slider)}
                          >
                            {Router.pathname === "/profile/recentlyview" ? (
                              <img
                                // className="card-img"
                                className={`card-img ${
                                  imagesLoaded ? "loaded" : ""
                                }`}
                                width={230}
                                height={230}
                                src={`${AWS_S3_URL}${product?.bannerImage?.yellowGold}`}
                                onLoad={handleImageLoad}
                                onError={(e) => {
                                  if (e && removeErrorImage) {
                                    removeErrorImage(e.target);
                                  }
                                  e.target.onerror = null;
                                  e.target.src = "/listing_loader.webp";
                                }}
                                priority
                              />
                            ) : (
                              product?.images?.[sliderImages]?.map(
                                (img, index) => (
                                  <img
                                    // className="card-img"
                                    className={`card-img ${
                                      imagesLoaded ? "loaded" : ""
                                    }`}
                                    width={230}
                                    height={230}
                                    src={`${AWS_S3_URL}${img}`}
                                    onLoad={handleImageLoad}
                                    onError={(e) => {
                                      if (e && removeErrorImage) {
                                        removeErrorImage(e.target);
                                      }
                                      e.target.onerror = null;
                                      e.target.src = "/listing_loader.webp";
                                    }}
                                    priority
                                  />
                                )
                              )
                            )}
                          </Slider>
                        </div>
                      </ImgContainer>
                    </Link>
                    {Router.pathname !== "/profile/recentlyview" && (
                      <div className="gallery-slider-bottom">
                        <div className="arrows ">
                          <div
                            className="left-arrow cursor-pointer"
                            onClick={prevSlide}
                          >
                            <Image
                              src="/left-arrow.svg"
                              alt=""
                              width={30}
                              height={30}
                            />
                          </div>
                          <div
                            className="right-arrow cursor-pointer"
                            onClick={nextSlide}
                          >
                            <Image
                              src="/right-arrow.svg"
                              alt=""
                              width={30}
                              height={30}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </UpperSection>
                <BottomCardName>
                  <TxtDiv2>
                    <div className="product-price-info">
                      <PriceHeading>
                        <div className="special-price">
                          ₹{toIndianCurrency(product?.grandTotal).split(".")[0]}
                        </div>
                        {product?.discount && (
                          <div className="old-price">
                            ₹
                            {
                              toIndianCurrency(product?.crossPrice).split(
                                "."
                              )[0]
                            }
                            /-
                          </div>
                        )}
                      </PriceHeading>
                      <PriceHeading2>{product?.title}</PriceHeading2>
                      <GreenTxt>{product?.discount?.title}</GreenTxt>
                    </div>
                    <div className="color-btn">
                      {!findMissingKeys(product?.bannerImage).includes(
                        "yellowGold"
                      ) && (
                        <button
                          onClick={() => setImageType(0)}
                          className="card-btn yellow"
                        ></button>
                      )}
                      {!findMissingKeys(product?.bannerImage).includes(
                        "whiteGold"
                      ) && (
                        <button
                          onClick={() => setImageType(1)}
                          className="card-btn grey"
                        ></button>
                      )}
                      {!findMissingKeys(product?.bannerImage).includes(
                        "roseGold"
                      ) && (
                        <button
                          onClick={() => setImageType(2)}
                          className="card-btn orange"
                        ></button>
                      )}
                    </div>
                  </TxtDiv2>
                </BottomCardName>
                <BtnDivCard className="view-similar-btn">
                  <SmallTxtBottom className="view-similar">
                    <div className="thumb">
                      <img
                        src="/img/card/viewSimilar.svg"
                        height="13"
                        width="14"
                      />
                    </div>
                    <div
                      className="mobile_only cursor-pointer"
                      onClick={() => onViewSimillar("bottom")}
                    >
                      <SmallTxtBottomTxt>View Similar</SmallTxtBottomTxt>
                    </div>
                    <div className="desktop_only cursor-pointer">
                      <SmallTxtBottomTxt
                        onClick={() =>
                          onViewSimillar("auto", product?.category?.slug)
                        }
                      >
                        View Similar
                      </SmallTxtBottomTxt>
                    </div>
                  </SmallTxtBottom>
                  <div className="buy-now flex justify-center items-center text-center">
                    <Link
                      target="_blank"
                      href={`/${product?.category?.title}/${product?.slug}`}
                    >
                      <StyledProductButton>Buy Now</StyledProductButton>
                    </Link>
                  </div>
                </BtnDivCard>
              </MainCard>
            </MainDiv>
          </div>
        </div>
      </ProductGrid>
      <ViewSimillar
        onOpenChange={onOpenChange}
        isOpen={isOpen}
        modalPlacement="auto"
        size="4xl"
        viewSimilarData={viewSimilarData}
        slug={product?.category?.slug}
        isFetching={isFetching}
      />
    </>
  );
}
