import { api, apiEndPoints } from "../../../api";

export const addToFireStoreWishlist = async ({
  userId,
  pid,
  prodCollectionId,
}) => {
  const response = await api.post(apiEndPoints.addToWishlist, {
    key: userId,
    pid: pid,
    prodCollectionId: prodCollectionId,
  });
  return { response, prodCollectionId, userId };
};

export const postVisitor = async (payload) => {
  console.log(payload, "visitor");
  const response = await api.post(apiEndPoints.addVisitor, payload);
  return response;
};

export const createLogWishlist = async (payload) => {
  console.log(payload, "logWishlist");
  const response = await api.post(apiEndPoints.logWishlist, payload);
  return response;
};

export const removeCouponcode = async (coupon) => {
  const response = coupon && (await api.put(apiEndPoints.deleteCoupon, coupon));
  return response;
};

export const AddRecent = async (payload) => {
  const response = await api.post(apiEndPoints.addRecentView, payload);
  return response;
};
