import { useDispatch, useSelector } from "react-redux";
// import {
//   collection,
//   onSnapshot,
//   Wishlist,
//   firebaseDb,
// } from "../../../utils/firebase.utils";
import { useRouter } from "next/router";
import { useCart } from "react-use-cart";
import { useWishlist } from "react-use-wishlist";
import {
  // addToFireStoreWishlist,
  // deleteFirebaseStoreCart,
  removeCouponcode,
  AddRecent,
  postVisitor,
  createLogWishlist,
} from "./api";
import { useEffect, useState, useRef, useContext } from "react";
import { useMutation, useQuery } from "react-query";
import toast from "react-hot-toast";
import { toastSettings } from "@/constants";
// import { getSimilarProductsz } from "@/container/productpage/api";
import socket from "../../../../socket";
import AppContext from "@/constants/context";
import generatePicker from "antd/es/date-picker/generatePicker";
import { generateObjectId } from "@/utils/randomString";

const useCardHook = (product) => {
  const Router = useRouter();
  const [category, setCategory] = useState(Router?.slug || "");
  const catref = useRef(Router?.slug || "");
  const user = useSelector((state) => state.loggedInDetailsReducer.user);
  const [fireStoreWishlistdata, setFirestoreWishlistData] = useState(null);
  const { removeItem, emptyCart, updateItem, totalItems } = useCart();
  const [isWishlist, setIsWishlist] = useState(false);
  const {
    wishlistPids,
    setWishlistPids,
    setWishListCount,
    setWishListItems: setWishlistProducts,
  } = useContext(AppContext);

  const AddRef = useRef(false);
  const removeRef = useRef(false);

  const {
    setWishlistItems,
    inWishlist,
    removeWishlistItem,
    items,
    emptyWishlist,
  } = useWishlist();
  useEffect(() => {
    catref.current = category;

    catref.current && viewSimilarRefetch();
  }, [category]);

  const cartData = useSelector((state) => state.cartsReducer.cartData);
  const wishlistData = useSelector(
    (state) => state.wishistReducer.wishlistData
  );
  const dispatch = useDispatch();

  const handleWishlist = (cartItem, frmCart) => {
    if (user?.id) {
      if (frmCart) {
        // Router.push("/wishlist");

        socket.emit("addToWishlist", user?.id, cartItem?.productId);
        socket.on("wishlistUpdated", (response) => {
          if (!AddRef.current) {
            toast.success(response?.msg, toastSettings);
            AddRef.current = true;
          }

          const visitorId = localStorage.getItem("visitorId");
          logWishlist({
            visitorId: visitorId,
            productId: cartItem?.productId,
            userId: user?.id,
          });
          dispatch({
            type: "GET_WISHLIST",
            payload: response,
          });

          socket.emit("removeFromCart", user?.id, cartItem?._id, cartData?._id);
          socket.on("cartRemoved", (res) => {
            if (res?.success) {
              dispatch({
                type: "GET_CARTS",
                payload: res?.data[0],
              });
            }
            // toast.success(res?.msg, toastSettings);
          });
        });

        return;
      } else {
        if (Router.pathname == "/wishlist") {
          socket.emit("removeFromWishlist", user?.id, product?._id);
          socket.on("wishlistRemoved", (response) => {
            if (!removeRef.current) {
              toast.success(response?.msg, toastSettings);
              removeRef.current = true;
            }

            dispatch({
              type: "GET_WISHLIST",
              payload: response,
            });
          });
        } else {
          if (wishlistData?.productIds.includes(product?._id)) {
            socket.emit("removeFromWishlist", user?.id, product?._id);
            socket.on("wishlistRemoved", (response) => {
              if (!removeRef.current) {
                toast.success(response?.msg, toastSettings);
                removeRef.current = true;
              }

              dispatch({
                type: "GET_WISHLIST",
                payload: response,
              });
            });
          } else {
            socket.emit("addToWishlist", user?.id, product?._id);
            socket.on("wishlistUpdated", (response) => {
              if (!AddRef.current) {
                toast.success(response?.msg, toastSettings);
                AddRef.current = true;
              }
              const visitorId = localStorage.getItem("visitorId");
              logWishlist({
                visitorId: visitorId,
                productId: product?._id,
                userId: user?.id,
              });

              dispatch({
                type: "GET_WISHLIST",
                payload: response,
              });
            });
          }
        }
      }
    } else {
      if (inWishlist(product?._id) == true) {
        if (Router.route === `/cart`) {
          removeItem(product?._id);
          totalItems == 1 ? Router.push("/wishlist") : Router.push("/cart");
        } else {
          removeWishlistItem(product?._id);
          toast.success("Removed from Wishlist", toastSettings);
        }
      } else {
        const randomId = generateObjectId();
        let hasRandomId = "";
        hasRandomId = localStorage.getItem("visitorId");
        if (!hasRandomId?.length > 0) {
          setIsWishlist(true);
          localStorage.setItem("visitorId", randomId);
          addVisitor({ visitorId: randomId });
        }

        if (items?.length > 0) {
          let wArr = items;
          wArr.push({ ...product });
          setWishlistItems(wArr);
          removeItem(product?._id);
          const visitorId = localStorage.getItem("visitorId");
          logWishlist({
            visitorId: visitorId,
            productId: product?._id,
            userId: null,
          });
          toast.success("Added to Wishlist", toastSettings);
        } else {
          setWishlistItems([{ ...product }]);
          removeItem(product?._id);
          const visitorId = localStorage.getItem("visitorId");
          logWishlist({
            visitorId: visitorId,
            productId: product?._id,
            userId: null,
          });
          toast.success("Added to wishlist", toastSettings);
        }
      }
    }
  };

  const { mutate: addVisitor } = useMutation(
    (payload) => postVisitor(payload),
    {
      onSuccess: (data) => {
        const { data: { isSuccess, msg } = {} } = data;
        if (isSuccess) {
          localStorage.setItem("visitorId", data?.data?.vid);
          if (isWishlist) {
            logWishlist({
              visitorId: data?.data?.vid,
              productId: product?._id,
              userId: null,
            });
          } else {
            AddRecentlyView({
              vid: data?.data?.vid,
              pid: product?._id,
              uid: null,
            });
          }
        }
      },
    }
  );

  const { mutate: logWishlist } = useMutation(
    (payload) => createLogWishlist(payload),
    {
      onSuccess: (data) => {
        const { data: { isSuccess, msg } = {} } = data;
        // if (isSuccess) {
        //   localStorage.setItem("visitorId", data?.data?.vid);
        // }
      },
    }
  );

  const {
    data: { product: viewSimilarData } = {},
    refetch: viewSimilarRefetch,
    isFetching,
  } = useQuery(
    "similarProduct",
    () => getSimilarProductsz(product?._id, product?.grandTotal),
    {
      enabled: false,
    }
  );

  // const handleMoveToWishlist = (item) => {
  //   let firebasePayload = {
  //     userId: user?.id,
  //     pid: item?.pid ? item?.pid : item?._id,
  //     prodCollectionId: item?.prodCollectionId || "",
  //   };
  //   if (user?.id) {
  //     if (Router.route === `/cart`) {
  //       if (
  //         _.some(fireStoreWishlistdata, {
  //           pid: item?.pid ? item?.pid : item?.id,
  //         })
  //       ) {
  //         let remPayload = {
  //           userId: user.id,
  //           pid: item?.prodCollectionId,
  //         };
  //         removeFirebaseCart(remPayload);
  //         // Router.push("/wishlist");
  //       } else {
  //         addFirebaseWishlist(firebasePayload);
  //       }
  //     } else {
  //       addFirebaseWishlist(firebasePayload);
  //     }
  //   } else {
  //     if (inWishlist(item?.id) == true) {
  //       if (Router.route === `/cart`) {
  //         removeItem(item?.id);
  //         totalItems == 1 ? Router.push("/wishlist") : Router.push("/cart");
  //       } else {
  //         removeWishlistItem(item?.id);
  //         toast.success("Removed from Wishlist", toastSettings);
  //       }
  //     } else {
  //       if (items?.length > 0) {
  //         let wArr = items;
  //         wArr.push({ ...item });
  //         setWishlistItems(wArr);
  //         removeItem(item?.id);
  //         toast.success("Added to Wishlist", toastSettings);
  //       } else {
  //         setWishlistItems([{ ...item }]);
  //         removeItem(item?.id);
  //         toast.success("Added to wishlist", toastSettings);
  //       }
  //     }
  //   }
  // };

  const checkItemInWishlist = (productId) => {
    if (user?.id) {
      if (wishlistData?.productIds?.includes(productId)) {
        return true;
      } else {
        return false;
      }
    } else {
      const idsArray = items.map((obj) => obj._id);
      if (idsArray?.includes(productId)) {
        return true;
      } else {
        return false;
      }
    }
  };

  // const { mutate: addFirebaseWishlist } = useMutation(
  //   (payload) => addToFireStoreWishlist(payload),
  //   {
  //     onSuccess: (data) => {
  //       const {
  //         response: { data: { is_success, msg } = {} } = {},
  //         prodCollectionId,
  //       } = data;
  //       if (is_success) {
  //         const firebasePayload = {
  //           userId: user.id,
  //           pid: prodCollectionId,
  //         };
  //         if (Router.route === `/cart`) {
  //           removeFirebaseCart(firebasePayload);
  //           if (localStorage.getItem("couponName")) {
  //             handleRemovePromocode(localStorage.getItem("couponName"));
  //           }
  //           // Router.push(`/wishlist`);
  //         }
  //         toast.success(msg, toastSettings);
  //       }
  //     },
  //   }
  // );

  // const removeCartItemFromFirebase = (cartObj) => {
  //   if (user.id) {
  //     const firebasePayload = {
  //       userId: user?.id,
  //       pid: cartObj?.prodCollectionId,
  //     };
  //     removeFirebaseCart(firebasePayload);
  //   } else {
  //     removeItem(cartObj?.id);
  //   }
  //   // setShowModal(false);
  // };

  const handleRemovePromocode = (couponName) => {
    let payload = {
      isCoupon: false,
      couponName: couponName,
      user_id: user?.id,
    };
    removeCoupon(payload);
  };
  const { mutate: removeCoupon } = useMutation(
    (data) => removeCouponcode(data),
    {
      onSuccess: (data) => {
        const { data: { isSuccess = "", msg = "" } = {} } = data;
        if (isSuccess) {
          localStorage.removeItem("coupon");
          localStorage.removeItem("couponName");
        }
      },
    }
  );

  const handleRecentlyView = (productId) => {
    let tempId = "";

    if (typeof window !== "undefined") {
      tempId = JSON.parse(localStorage.getItem("LoggedInUser"));
    }

    let payload = {};
    const visitorId = localStorage.getItem("visitorId");
    if (tempId?.userId && visitorId) {
      payload = {
        uid: tempId?.userId,
        pid: productId,
        vid: visitorId,
      };
      AddRecentlyView(payload);
    } else if (visitorId) {
      payload = {
        vid: visitorId,
        pid: productId,
      };
      AddRecentlyView(payload);
    } else {
      const randomId = generateObjectId();
      addVisitor({ visitorId: randomId });
    }
  };

  const { mutate: AddRecentlyView } = useMutation((data) => AddRecent(data), {
    onSuccess: (data) => {
      console.log("added");
    },
  });

  // wishlist firestore
  // useEffect(() => {
  //   if (user?.id) {
  //     const wishlistCollectionRef = collection(
  //       firebaseDb,
  //       `WishList/${user?.id}/products`
  //     );
  //     const unsubscribeWishlist = onSnapshot(
  //       wishlistCollectionRef,
  //       (querySnapshot) => {
  //         const newData = querySnapshot.docs.map((doc) => ({
  //           ...doc.data(),
  //           id: doc.id,
  //         }));
  //         setFirestoreWishlistData((prevTodos) => [...newData]);
  //       }
  //     );
  //     return () => {
  //       unsubscribeWishlist();
  //     };
  //   } else {
  //     setFirestoreWishlistData(items);
  //   }
  // }, [Wishlist, user?.id, items]);

  return {
    // handleMoveToWishlist,
    fireStoreWishlistdata,
    // removeCartItemFromFirebase,
    checkItemInWishlist,
    handleRecentlyView,
    viewSimilarData,
    setCategory,
    isFetching,
    handleWishlist,
    wishlistPids,
  };
};

export default useCardHook;
