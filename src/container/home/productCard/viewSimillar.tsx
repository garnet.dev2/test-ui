import { ViewSimilarStyle } from "./trending";
import { Modal, ModalContent, ModalBody, Spinner } from "@nextui-org/react";

import ProductCard from ".";

const ViewSimillar = (props: any) => {
  const { isOpen, onOpenChange, size, viewSimilarData, isFetching } = props;

  return (
    <>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        size={size}
        scrollBehavior="inside"
        motionProps={{
          variants: {
            enter: {
              y: 0,
              opacity: 1,
              transition: {
                duration: 0.3,
                ease: "easeOut",
              },
            },
            exit: {
              y: -20,
              opacity: 0,
              transition: {
                duration: 0.2,
                ease: "easeIn",
              },
            },
          },
        }}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalBody>
                <ViewSimilarStyle>
                  <strong className="filter-subtitle">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="57"
                      height="6"
                      viewBox="0 0 57 6"
                      fill="none"
                    >
                      <path
                        d="M25.1027 0H33.8429L31.0438 6H22.3921L25.1027 0Z"
                        fill="#007A64"
                      />
                      <path
                        d="M36.8971 0H56.9997L53.9462 6H33.5891L36.8971 0Z"
                        fill="#012433"
                      />
                      <path
                        d="M36.8971 0H56.9997L53.9462 6H33.5891L36.8971 0Z"
                        fill="url(#paint0_linear_507_5741)"
                      />
                      <path
                        d="M3.30802 0H23.4106L20.3571 6H0L3.30802 0Z"
                        fill="#012433"
                      />
                      <path
                        d="M3.30802 0H23.4106L20.3571 6H0L3.30802 0Z"
                        fill="url(#paint1_linear_507_5741)"
                      />
                      <defs>
                        <linearGradient
                          id="paint0_linear_507_5741"
                          x1="33.5891"
                          y1="6"
                          x2="57.0734"
                          y2="5.22054"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop stopColor="#007A64" />
                          <stop
                            offset="1"
                            stopColor="#012433"
                            stopOpacity="0"
                          />
                        </linearGradient>
                        <linearGradient
                          id="paint1_linear_507_5741"
                          x1="18.3214"
                          y1="-4.8202e-07"
                          x2="-4.58034"
                          y2="-5.79556e-07"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop stopColor="#007A64" />
                          <stop
                            offset="1"
                            stopColor="#012433"
                            stopOpacity="0"
                          />
                        </linearGradient>
                      </defs>
                    </svg>
                    VIEW SIMILAR
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="57"
                      height="6"
                      viewBox="0 0 57 6"
                      fill="none"
                    >
                      <path
                        d="M25.1027 0H33.8429L31.0438 6H22.3921L25.1027 0Z"
                        fill="#007A64"
                      />
                      <path
                        d="M36.8971 0H56.9997L53.9462 6H33.5891L36.8971 0Z"
                        fill="#012433"
                      />
                      <path
                        d="M36.8971 0H56.9997L53.9462 6H33.5891L36.8971 0Z"
                        fill="url(#paint0_linear_507_5741)"
                      />
                      <path
                        d="M3.30802 0H23.4106L20.3571 6H0L3.30802 0Z"
                        fill="#012433"
                      />
                      <path
                        d="M3.30802 0H23.4106L20.3571 6H0L3.30802 0Z"
                        fill="url(#paint1_linear_507_5741)"
                      />
                      <defs>
                        <linearGradient
                          id="paint0_linear_507_5741"
                          x1="33.5891"
                          y1="6"
                          x2="57.0734"
                          y2="5.22054"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop stopColor="#007A64" />
                          <stop
                            offset="1"
                            stopColor="#012433"
                            stopOpacity="0"
                          />
                        </linearGradient>
                        <linearGradient
                          id="paint1_linear_507_5741"
                          x1="18.3214"
                          y1="-4.8202e-07"
                          x2="-4.58034"
                          y2="-5.79556e-07"
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop stopColor="#007A64" />
                          <stop
                            offset="1"
                            stopColor="#012433"
                            stopOpacity="0"
                          />
                        </linearGradient>
                      </defs>
                    </svg>
                  </strong>
                  <div className="similar-product">
                    <>
                      {isFetching ? (
                        <Spinner
                          color="success"
                          className="mx-auto py-3"
                        />
                      ) : (
                        <>
                          {viewSimilarData
                            ?.slice(0, 9)
                            ?.map((product: any, key: number) => {
                              return (
                                <>
                                  <ProductCard key={key} product={product} />
                                </>
                              );
                            })}
                        </>
                      )}
                    </>
                  </div>
                </ViewSimilarStyle>
              </ModalBody>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
};
export default ViewSimillar;
