/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
"use client";
import {
  MainDiv,
  MainCard,
  UpperSection,
  ImgTopDiv,
  ImgContainer,
  BottomCardName,
  PriceHeading,
  TxtDiv2,
  PriceHeading2,
  BtnDivCard,
  SmallTxtBottom,
  SmallTxtBottomTxt,
  ProductGrid,
  GreenTxt,
} from "./trending";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { CardBadgeOne } from "../../../styled/button/index";
import { StyledProductButton } from "./style";
import ProgressiveImg from "@/utils/progressiveImg";
import Image from "next/image";
import React from "react";

export default function productCardSkelton(props: any) {
  const { count, product } = props;
  const [src, { blur }] = ProgressiveImg(
    "https://garnetlanee.s3.amazonaws.com/media/catalog/product/LRB-1125-GL/Dainty-Crown-Diamond-Band-Ring_YA.webp",
    "https://garnetlanee.s3.amazonaws.com/media/catalog/product/LRB-1125-GL/Dainty-Crown-Diamond-Band-Ring_YA.webp"
  );

  return (
    <div className="product-items">
      {Array.from({ length: count })?.map((count: any, key: number) => {
        return (
          <React.Fragment key={key}>
            <SkeletonTheme>
              <ProductGrid className="product-main-card">
                <div className="product-grid">
                  <div className="product-grid-inner">
                    <MainDiv>
                      <MainCard>
                        <UpperSection>
                          <ImgTopDiv>
                            <CardBadgeOne>
                              <Skeleton count={1} />
                            </CardBadgeOne>
                            <div className="thumb">
                              <Skeleton count={1} />
                            </div>
                          </ImgTopDiv>
                          <div>
                            <div></div>
                          </div>
                          <a href="#!">
                            <ImgContainer>
                              <div className="center">
                                <Image
                                  className="card-img"
                                  src={src}
                                  width={160}
                                  height={160}
                                  style={{
                                    filter: blur ? "blur(20px)" : "none",
                                    transition: blur
                                      ? "none"
                                      : "filter 0.3s ease-out",
                                  }}
                                  alt={"demo"}
                                />
                              </div>
                            </ImgContainer>
                          </a>
                        </UpperSection>
                        <BottomCardName>
                          <TxtDiv2>
                            <div className="product-price-info">
                              <Skeleton count={1} />
                              <PriceHeading>
                                {/* <div className="special-price"></div>
                              <div className="old-price">₹65251</div> */}
                              </PriceHeading>
                              <PriceHeading2>
                                <Skeleton count={1} />
                              </PriceHeading2>
                              <GreenTxt>
                                <Skeleton count={1} />
                              </GreenTxt>
                            </div>
                            <div className="color-btn">
                              <button className="card-btn yellow"></button>
                              <button className="card-btn grey"></button>
                              <button className="card-btn orange"></button>
                            </div>
                          </TxtDiv2>
                        </BottomCardName>
                        <BtnDivCard>
                          <SmallTxtBottom>
                            <div>
                              <Image
                                src="/img/card/viewSimilar.svg"
                                height={13}
                                width={14}
                                alt="text"
                              />
                            </div>
                            <div>
                              <SmallTxtBottomTxt>
                                View Similar
                              </SmallTxtBottomTxt>
                            </div>
                          </SmallTxtBottom>
                          <div className="flex justify-center items-center text-center">
                            <StyledProductButton>Buy now</StyledProductButton>
                          </div>
                        </BtnDivCard>
                      </MainCard>
                    </MainDiv>
                  </div>
                </div>
              </ProductGrid>
            </SkeletonTheme>
          </React.Fragment>
        );
      })}
    </div>
  );
}
