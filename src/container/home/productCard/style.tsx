
import {
    Button,
  } from "@nextui-org/react";
import styled from "styled-components";

export const StyledProductButton = styled(Button)
.attrs({
  // size:'xs'
})`
border-radius:35px 0px 0px 0px !important;
background: linear-gradient(90deg, ${(props) => props.theme.colors.orange} 7.87%, ${(props) => props.theme.colors.secondary} 100%) !important;
width: 127px;
height: 41px;
font-size: 13px;
font-style: normal;
font-weight: 600;
line-height: 18px;
color: ${(props) => props.theme.colors.white};
letter-spacing: 1px;
border: 0px;

@media only screen and (max-width: 767px){
  font-size: 10px !important;
  width: 95px !important;
  height: 38px;

}

@media only screen and (max-width: 370px) and (min-width: 320px) {
  border-radius: 0px 0px 0 0 !important;
  width: 100% !important;
}

`;