/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/anchor-is-valid */
import Image from "next/image";
import Link from "next/link";

const FooterCategories = () => (
  <div className="footer-categories">
    <ul>
      <li>
        <div className="thumb">
          <Image
            src="../assets/footer/gold-rate.svg"
            alt="Gold Rate"
            width={25}
            height={22}
          />
        </div>
        <Link href="/gold-rate" legacyBehavior>
          <a>GOLD RATE</a>
        </Link>
      </li>

      <li>
        <div className="thumb">
          <Image
            src="../assets/footer/payment-method.svg"
            alt="payment method"
            width={25}
            height={22}
          />
        </div>
        <Link href="/various-payment-options" legacyBehavior>
          <a>VARIOUS PAYMENT OPTIONS</a>
        </Link>
      </li>
    </ul>
  </div>
);

export default FooterCategories;
