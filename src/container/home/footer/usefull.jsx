/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/anchor-is-valid */
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

const FooterUsefull = () => {
  const router = useRouter();

  return (
    <div className="footer_useful">
      <ul>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/return.svg"
              alt="RETURNS"
              width={20}
              height={23}
            />
          </div>

          <div
            onClick={() =>
              router.push({
                pathname: "/return-and-refund-policy",
                query: { key: "30DayReturn" },
              })
            }
          >
            <a>30-DAY RETURNS</a>
          </div>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/shipping.svg"
              alt="FREE SHIPPING"
              width={25}
              height={25}
            />
          </div>
          <Link href="/free-shipping" legacyBehavior>
            <a> FREE SHIPPING</a>
          </Link>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/exchange.svg"
              alt="LIFETIME EXCHANGE POLICY"
              width={22}
              height={22}
            />
          </div>

          <div
            onClick={() =>
              router.push({
                pathname: "/return-and-refund-policy",
                query: { key: "lifetimeExchange" },
              })
            }
          >
            <a> LIFETIME EXCHANGE POLICY</a>
          </div>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/refund.svg"
              alt="RETURN AND REFUND POLICY"
              width={25}
              height={23}
            />
          </div>

          <div
            onClick={() =>
              router.push({
                pathname: "/return-and-refund-policy",
                query: { key: "returnAndRefund" },
              })
            }
          >
            <a> RETURN AND REFUND POLICY</a>
          </div>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="/assets/footer/gspLogo.svg"
              alt="TERMS & CONDITIONS FOR OFFERS"
              width={22}
              height={25}
            />
          </div>
          <Link href="/gsp-terms-and-condition" legacyBehavior>
            <a>GSP TERMS & CONDITIONS</a>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default FooterUsefull;
