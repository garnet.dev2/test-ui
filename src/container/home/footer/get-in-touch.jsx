import { toastSettings } from "@/constants";
import Image from "next/image";
import Link from "next/link";
import toast from "react-hot-toast";

/* eslint-disable @next/next/no-img-element */
const GetInTouch = () => {
  return (
    <>
      <div className="aboutAuto">
        <div className="footer-text">
          <div className="thumb">
            <Image
              src="../assets/footer/whatsapp.svg"
              alt="whatsapp"
              width={25}
              height={23}
            />
          </div>
          <Link
            href="https://api.whatsapp.com/send?phone=9988898866"
            target="_blank"
            rel="noopener noreferrer"
          >
            99888-98866
          </Link>
        </div>
        <div className="footer-text">
          <div className="thumb">
            <Image
              src="../assets/footer/email.svg"
              alt="email"
              width={25}
              height={23}
            />
          </div>
          <Link href="mailto: info@gemlay.com">info@gemlay.com</Link>
        </div>
        <div className="footer-text">
          <div className="thumb">
            <Image
              src="../assets/footer/navigation.svg"
              alt="navigation"
              width={30}
              height={30}
            />
          </div>
          <span>
            Sco 15, Ground Floor, Jubilee Junction, Sector 66, Main Mohali
            Airport Road.
          </span>
        </div>
      </div>
      <ul className="social-media">
        <li
          role="button"
          onClick={() =>
            window.open("https://www.facebook.com/profile.php?id=61558457590288&sk=about_details", "_blank")
          }
        >
          <Image
            src="../assets/footer/fb.svg"
            alt="Facebook"
            width={30}
            height={30}
          />
        </li>
        <li
          role="button"
          onClick={() =>
            window.open(
              "https://www.instagram.com/gemlay_com?igsh=dmZlNmJwd3lseWU5",
              "_blank"
            )
          }
        >
          <Image
            src="../assets/footer/insta.svg"
            alt="Instagram"
            width={30}
            height={30}
          />
        </li>
        <li
          role="button"
          onClick={() =>
            window.open(
              "https://www.youtube.com/@Gemlayjewellery",
              "_blank"
            )
          }
        >
          <Image
            src="../assets/footer/youtube.svg"
            alt="youtube"
            width={30}
            height={30}
          />
        </li>
      </ul>
      <div className="application desktop_only">
        Download Our Application
        <div className="thumb">
          <img
            src="../assets/footer/google-play.png"
            alt="google-pay"
            onClick={() => toast.success("Comming Soon", toastSettings)}
          />
          <img
            src="../assets/footer/apple-pay.png"
            alt="apple-pay"
            onClick={() => toast.success("Comming Soon", toastSettings)}
          />
        </div>
      </div>
    </>
  );
};

export default GetInTouch;
