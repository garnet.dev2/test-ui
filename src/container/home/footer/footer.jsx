/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/no-danger */
import React, { useState } from "react";
import { FooterContainer, FooterLinks } from "./Styles";
import FooterCategories from "./categories";
import FooterUsefull from "./usefull";
import FooterCopyRight from "./copyRight";
import FooterSupport from "./support";
import FooterSocial from "./socialmedia";
import GetInTouch from "./get-in-touch";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import { toastSettings } from "@/constants";
const FooterInfo = [
  {
    id: 1,
    header: "OUR SERVICES",
    component: <FooterCategories />,
  },
  {
    id: 2,
    header: "OUR POLICIES",
    component: <FooterUsefull />,
  },
  {
    id: 3,
    header: "JEWELLERY KNOWLEDGE",
    component: <FooterSupport />,
  },
  {
    id: 4,
    header: "ABOUT US",
    component: <FooterSocial />,
  },
  {
    id: 5,
    header: "Get In Touch",
    component: <GetInTouch />,
  },
];

const FooterItems = (props) => {
  const { handleToggle, active, FooterItem } = props;
  const { header, id, component } = FooterItem;

  return (
    <>
      <div
        className={`footerLink ${active === id ? "active" : ""}`}
        onClick={() => handleToggle(id)}
      >
        <h3>{header}</h3>
        <div className="footer-content">{component}</div>
      </div>
    </>
  );
};

const Footer = () => {
  const Route = useRouter();
  const [active, setActive] = useState(null);

  const handleToggle = (index) => {
    if (active === index) {
      setActive(null);
    } else {
      setActive(index);
    }
  };

  return (
    <>
      {Route.pathname !== "/checkout" && Route.pathname !== "/cart" && (
        <FooterContainer>
          <div className="footer-content container">
            <FooterLinks>
              {FooterInfo.map((FooterItem, index) => {
                return (
                  <FooterItems
                    key={index}
                    active={active}
                    handleToggle={handleToggle}
                    FooterItem={FooterItem}
                  />
                );
              })}

              <div className="footerLink mobile_only">
                <div className="application">
                  Download Our Application
                  <div className="thumb">
                    <img
                      src="../assets/footer/google-play.png"
                      alt="google-pay"
                      onClick={() =>
                        toast.success("Comming Soon", toastSettings)
                      }
                    />
                    <img
                      src="../assets/footer/apple-pay.png"
                      alt="apple-pay"
                      onClick={() =>
                        toast.success("Comming Soon", toastSettings)
                      }
                    />
                  </div>
                </div>
              </div>
            </FooterLinks>
            <ul className="footer-links">
              <li>
                <a href="/sitemap.xml">SITE MAP</a>
              </li>
              <li>|</li>
              <li>
                <a href="/privacy-policy">PRIVACY POLICY</a>
              </li>
              <li>|</li>
              <li>
                <a href="/terms-and-condition">TERMS AND CONDITIONS</a>
              </li>
              <li>|</li>
              <li>
                <a href="/faqs">FAQs</a>
              </li>
            </ul>
          </div>
        </FooterContainer>
      )}
      <FooterCopyRight />
    </>
  );
};

export default Footer;
