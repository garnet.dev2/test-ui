import { CopyRight } from "./Styles";

const FooterCopyRight = () => (
  <CopyRight>
    Copyright © Gemlay 2024 
  </CopyRight>
);

export default FooterCopyRight;
