/* eslint-disable jsx-a11y/anchor-is-valid */
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

const FooterSocial = () => {
  const router = useRouter();
  const scrollToPress = () => {
    const section = document.getElementById("press-release");

    // Scroll to the section
    if (router.pathname === "/") {
      window.scrollTo({
        top: section.offsetTop,
        behavior: "smooth", // Optional: for smooth scrolling
      });
    }
  };

  const scrollToReview = () => {
    console.log("Scroll");
    // Find the section to scroll to
    const section = document.getElementById("review");

    // Scroll to the section
    if (router.pathname === "/") {
      window.scrollTo({
        top: section.offsetTop,
        behavior: "smooth", // Optional: for smooth scrolling
      });
    }
  };

  return (
    <div className="footer_useful">
      <ul>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/vision.svg"
              alt="OUR VISION"
              width={25}
              height={23}
            />
          </div>
          <Link href="/our-vision" legacyBehavior>
            <a>OUR VISION</a>
          </Link>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/garnet.svg"
              alt="WHY GARNET LANEE"
              width={25}
              height={23}
            />
          </div>
          <Link href="/why-gemlay" legacyBehavior>
            <a>WHY GEMLAY ?</a>
          </Link>
        </li>

        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/story.svg"
              alt="OUR STORY"
              width={25}
              height={23}
            />
          </div>
          <Link href="/our-story" legacyBehavior>
            <a>OUR STORY</a>
          </Link>
        </li>

        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/team.svg"
              alt="OUR TEAM"
              width={25}
              height={23}
            />
          </div>
          <Link href="/our-team" legacyBehavior>
            <a>OUR TEAM</a>
          </Link>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/review.svg"
              alt="REVIEWS AND RATINGS"
              width={25}
              height={23}
            />
          </div>
          <div
            onClick={() => {
              if (router.pathname == "/") {
                scrollToReview();
              } else {
                router.push({
                  pathname: "/",
                  query: { key: "review" },
                });
              }
            }}
          >
            <a>REVIEWS AND RATINGS</a>
          </div>
        </li>
        <li>
          <div className="thumb">
            <Image
              src="../assets/footer/press.svg"
              alt="PRESS RELEASE"
              width={12}
              height={12}
            />
          </div>
          <div
            onClick={() => {
              if (router.pathname == "/") {
                scrollToPress();
              } else {
                router.push({
                  pathname: "/",
                  query: { key: "press-release" },
                });
              }
            }}
          >
            <a>PRESS RELEASE</a>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default FooterSocial;
