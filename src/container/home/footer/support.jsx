/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/anchor-is-valid */
import Image from "next/image";
import Link from "next/link";

const FooterSupport = () => (
  <div className="footer_useful">
    <ul>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/gold.svg" alt="GOLD GUIDE" width={25} height={23}/>
      </div>
        <Link href="/gold-guide" legacyBehavior>
          <a>GOLD GUIDE </a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/diamond.svg" alt="DIAMOND GUIDE" width={25} height={23}/>
      </div>
        <Link href="/diamond-guide" legacyBehavior>
          <a>DIAMOND GUIDE </a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/JEWELLERY.svg" alt="JEWELLERY GUIDE" width={25} height={23}/>
      </div>
        <Link href="/jewellery-guide" legacyBehavior>
          <a>JEWELLERY GUIDE</a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/SOLITIARE-DIAMOND.svg" alt="SOLITAIRE DIAMOND GUIDE" width={25} height={23}/>
      </div>
        <Link href="/solitaire-diamond-guide" legacyBehavior>
          <a>SOLITAIRE DIAMOND GUIDE</a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/GEMSTONE.svg" alt="GEMSTONE GUIDE" width={25} height={23}/>
      </div>
        <Link href="/gemstone-guide" legacyBehavior>
          <a>GEMSTONE GUIDE</a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/CERTIFICATION.svg" alt="CERTIFICATION GUIDE" width={25} height={23}/>
      </div>
        <Link href="/certification-guide" legacyBehavior>
          <a>CERTIFICATION GUIDE</a>
        </Link>
      </li>
      <li>
      <div className="thumb">
      <Image src="../assets/footer/JEWELLERY1.svg" alt="JEWELLERY GIFTING" width={25} height={23}/>
      </div>
        <Link href="/jewellery-gifting" legacyBehavior>
          <a>JEWELLERY GIFTING</a>
        </Link>
      </li>
    </ul>
  </div>
);

export default FooterSupport;
