import styled from "styled-components";

export const FooterContainer = styled.footer`
  padding: 40px 0px 30px;
  border-radius: 5px;
  background: #eafffb87;

  .footer-links {
    display: flex;
    gap: 5px;
    align-items: center;
    font-size: 10px;
    color: rgba(48, 47, 47, 0.70);
    font-weight: 600;
    line-height: normal;
    a {
      font-size: 10px;
      color: rgba(48, 47, 47, 0.70);
      font-weight: 600;
      line-height: normal;
      &:hover {
        color: ${(props) => props.theme.colors.orange};
      }
    }
  }
  .footer-text {
    font-size: 12px;
    color: rgba(48, 47, 47, 0.70);
    font-weight: 600;
    line-height: normal;
    display: flex;
    align-items: center;
    gap: 10px;
    position: relative;
    padding-bottom: 20px;
    padding-left: 30px;
    &.text {
        padding-left: 0px;
        padding-bottom: 10px;
    }
    a {
      color: rgba(48, 47, 47, 0.70);
      &:hover {
        color: ${(props) => props.theme.colors.orange};
      }
    }
    .thumb {
        width: 22px;
        text-align: center;
        display: flex;
        justify-content: center;
        position: absolute;
        left: 0;
    }
  }
  .aboutAuto {
    padding-top: 10px;
  }
  .application {
    color: rgba(48, 47, 47, 0.70);
    font-size: 14px;
    font-weight: 600;
    line-height: normal;
    &.desktop_only {display: block;}
    .thumb {
        display: flex;
        align-items: center;
        box-sizing: border-box;
        gap: 10px;
        padding-top: 10px;
        img {
            width: 50%;
            border-radius: 6px;
        }
    }
  }
.social-media {
    display: flex;
    align-items: center;
    gap: 20px;
    padding: 0px;
    li {
        a {
            border: 1px solid rgba(247, 116, 51, 1);
            border-radius: 100px;
            width: 50px;
            height: 50px;
            background: ${(props) => props.theme.colors.white};
            line-height: 50px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        img {
          width: 45px;
          height: 45px;
          line-height: 45px;
          text-align: center;
          background: ${(props) => props.theme.colors.white};
          padding: 10px;
          border-radius: 100px;
        }
    }
  }

  @media (max-width: 768px) {
    padding: 10px 0px 20px;
    .application {
      text-align: center;
      margin-top: 20px;
      &.desktop_only {display: none;}
      .thumb {
        justify-content: center;
        img {
            width: 130px;
        }
      }
    }
    .footer-links {
      justify-content: center;
      margin-top: 20px;
    }
  }
`;

export const FooterLinks = styled.footer`
  display: flex;
  gap: 15px;
  .footerLink {
    width: 20%;
    padding: 0px 20px;
    position: relative;
    &.mobile_only {display: none;}
    &::after {
        content: "";
        height: 80%;
        background: #D9D9D9;
        width: 1px;
        position: absolute;
        right: 0;
        top: 0;
    }
    &:nth-child(5n) {
        &:after {
            content: none;
        }
    }
  }
  h3 {
    color: ${(props) => props.theme.colors.orange};
    font-size: 13px;
    font-weight: 600;
    line-height: normal;
    margin: 0px;
  }
  .ready-to-buy {
    padding-top: 10px;
    display: flex;
    flex-direction: column;
    gap: 10px;
  }
  ul {
    padding: 20px 0px 0px;
    margin: 0px;
    li {
      padding-bottom: 10px;
      margin: 0px;
      display: flex;
        align-items: center;
        gap: 8px;
        .thumb {
            width: 18px;
            text-align: center;
            display: flex;
            justify-content: center;
        }
    }
    a {
        font-size: 10px;
        color: rgba(48, 47, 47, 0.70);
        font-weight: 600;
        line-height: normal;
        &:hover {
            color: ${(props) => props.theme.colors.orange};
        }
    }
  }

  @media (max-width: 1024px) {
    flex-wrap: wrap;
    .footerLink {
      width: 32%;
    }
  }
  @media (max-width: 768px) {
    gap: 0;
    .footerLink {
      width: 100%;
      padding: 0;
      &.mobile_only {display: block;}
      &:after {content: none;}
      &.active {
        .footer-content {display: block;}
        h3 {
          &:before {
            transform: rotate(90deg);
          }
        }
      }
    }
    .footer-content {display: none;padding: 0px;}
    h3 {
      padding: 15px 20px 15px 0px;
      border-bottom: 1px solid #e1e1e1;
      position: relative;
      &:before {
          content: "";
          border-top: 5px solid transparent;
          border-bottom: 5px solid transparent;
          border-right: 5px solid ${(props) => props.theme.colors.orange};
          position: absolute;
          right: 10px;
          transform: rotate(-90deg);
          transition: 0.5s ease-in-out;
          top: 20px;
      }
    }
  }
`;

export const CopyRight = styled.footer`
    padding: 15px 20px;
    text-align: center;
    border-top: 1px solid #e7e7e7;
    background: linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%);
    color: ${(props) => props.theme.colors.white};
    font-size: 12px;
    font-weight: 500;
    line-height: normal;
`;
