import React, { useState } from "react";
import { FaqsStyleMobile } from "./Styles";
import Image from "next/image";
const faqs = [
  {
    id: 1,
    header: "Buy Gold Jewellery Online in India",
    h2: "Buy Gold Jewellery Online in India",
    text: "Buying Jewellery Online in India is becoming common these days. Though many people are still sceptical of doing so. Yet, the trend of shopping online for anything and everything is surging exponentially. Natural Diamonds/ Lab grown diamonds studded Gold jewellery is now easily accessible through online platforms. Gemlay, with its impeccable reputation, stands out as a beacon in this burgeoning industry, offering a diverse range of jewellery direct to your home – Rings, Necklaces, Bracelets, Pendants et al. History tells the folktales of the importance of jewellery in India. Running down through decades, trends and styles may have changed, but emotions attached emanate ditto affection and care. Gemlay ensures that every piece of jewellery carries not just beauty but also a legacy of craftsmanship and tradition.",
  },
  {
    id: 2,
    header: "Variety of options",
    h2: "Variety of options",
    text: "We offer a plethora of options that are sure to fit both your preferences and your budget, making your experience of buying gold jewellery online from our Indian website hassle-free. Relax in the comfort of your home as you simply scroll through our designs and add your favorites to your wish list or cart. Rest assured, your order will be delivered safely to your doorstep.",
  },
  {
    id: 3,
    header: "Pay as per your convenience",
    h2: "Pay as per your convenience",
    text: "We provide both – prepaid and Cash on Delivery – options to you. It is entirely up to you how you want to pay. Moreover, if you felt returning back or exchanging jewellery – no questions would be asked in regard to that. Straight away your command would be executed.",
  },
  {
    id: 4,
    header: "Customize as you like",
    h2: "Customize as you like",
    text: "Maybe you prefer Rose gold or White? Or perhaps you're interested in a more lustrous and expensive diamond? We now also offer the option to choose Lab grown diamonds, providing both quality and affordability. Whatever you desire, we've already anticipated it. You're the boss here, and we're fully prepared to meet your demands while also offering cost-effective solutions.",
  },
  {
    id: 5,
    header: "Crowd-free Buying Diamond Jewellery Online in India",
    h2: "Crowd-free Buying Diamond Jewellery Online in India",
    text: "No waiting for your turn in a jewellery shop during dhanteras or any other festival, when you need to look out for your turn to be shown jewellery by a sales person. Additionally, after the purchase, you need to bring safely yourself and jewellery back home. OMG! That’s a lot of stress. But, now the problem exists no more Search -> Select -> Pay. That’s all you have to do without waiting. Jewellery will be sent securely to your home.",
  },
];
const FaqsMobile = () => {
  const [openItem, setOpenItem] = useState(null);
  return (
    <>
      <FaqsStyleMobile>
        <div className="accordion">
          {faqs.map((faq) => (
            <div key={faq.id} className="accordion-item">
              <div
                className={`accordion-header ${
                  openItem === faq.id ? "active" : ""
                }`}
                onClick={() => setOpenItem(openItem === faq.id ? null : faq.id)}
              >
                {faq.header}
                <Image src="/assets/common/checkoutNavbar/triangel.svg" alt="arrow" width={10} height={10} className="arrow"/>
                <Image src="/assets/common/checkoutNavbar/triangle-g.svg" alt="arrow" width={10} height={10} className="arrow2"/>

              </div>
              {openItem === faq.id && (
                <div className="accordion-text">{faq.text}</div>
              )}
            </div>
          ))}
        </div>
      </FaqsStyleMobile>
    </>
  );
};

export default FaqsMobile;
