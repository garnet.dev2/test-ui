import styled from "styled-components";

export const FaqsStyle = styled.div`
  padding: 50px 0px;
  background: linear-gradient(180deg, #FAFAFA 0%, #FAFAFA 81%, #ecfff7a3 100%);
  .card-body {
    display: flex;
    gap: 50px;
    width: 100%;
  }
  .tabs {
    width: 24%;
  }
  .tab-content {
    width: 76%;
  }
  h3 {
    color: ${(props) => props.theme.colors.orange};
    font-size: 15px;
    font-weight: 600;
    line-height: 18px;
    letter-spacing: 1px;
    margin-bottom: 20px;
  }
  .rc-collapse {
    display: none;
    width: 100%;
    right: 0;
    top: 0;
    color: #302f2f;
    font-size: 15px;
    font-style: normal;
    font-weight: 500;
    line-height: 28px;
    text-align: justify;
    &.show {
      display: block;
    }
  }
  .rc-accordion-toggle.active {
    .rc-accordion-title {
      color: ${(props) => props.theme.colors.secondary};
      &:before {
        border-left: 6px solid ${(props) => props.theme.colors.secondary};
      }
    }
  }
  .rc-accordion-title {
    color: #302f2f;
    font-size: 15px;
    font-style: normal;
    font-weight: 500;
    line-height: 18px;
    padding: 15px 0px;
    border-bottom: 1px solid rgba(217, 217, 217, 0.68);
    cursor: pointer;
    position: relative;
    padding-left: 20px;
    &:before {
      content: "";
      border-top: 6px solid transparent;
      border-bottom: 6px solid transparent;
      border-left: 6px solid #302f2f;
      position: absolute;
      left: 0;
      top: 17px;
    }
    &:hover {
      color: ${(props) => props.theme.colors.orange};
      &:before {
        border-left: 8px solid ${(props) => props.theme.colors.orange};
      }
    }
  }

  @media (max-width: 640px) {
    padding: 30px 0px;
    .rc-accordion-title {
      padding-left: 0px;
      padding-right: 20px;
      &:before {
        left: auto;
        right: 10px;
        transform: rotate(-90deg);
        transition: 0.5s ease-in-out;
      }
    }
    .rc-accordion-toggle.active {
      .rc-accordion-title {
        &:before {
          transform: rotate(90deg);
        }
      }
    }
    .tabs_mobile {
      width: 100%;
    }
    .rc-collapse {
      padding-top: 20px;
    }
  }
`;
export const FaqsStyleMobile = styled.div`
  font-size: 15px;
  .accordion {
    background: linear-gradient(180deg, #FAFAFA 0%, #FAFAFA 81%, #ecfff7a3 100%);
    padding: 30px 15px;
  }
  .accordion-item{
    /* padding-right: 8px; */
  }
  .accordion-header {
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #e3e3e3;
    padding-block: 15px;
    &.active {
      color: #007a64;
      .arrow {
        display: none;
      }
      .arrow2 {
        display: block;
        transform: rotate(180deg);
      }
    }
    .arrow2 {
      display: none;
    }
    .arrow {
      display: block;
    }
  }
  .accordion-text {
    text-align: justify;
    padding-top: 15px;
  }
`;
