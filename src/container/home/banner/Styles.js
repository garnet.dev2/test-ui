import styled from 'styled-components'

export const MainBanner = styled.div`
    position: relative;
    line-height: 0px;
    .slick-dots {
        bottom: 15px;
        width: auto;
        right: 25px;
        li {
            margin: 0px 2px;
            button:before {
                width: 14px;
                height: 14px;
                content: "";
                opacity: 1;
                background: ${(props) => props.theme.colors.white};
                border-radius: 100px;
            }
            &.slick-active {
                button {
                    &:before {
                        opacity: 1;
                    }
                    &:after {
                        width: 8px;
                        height: 8px;
                        content: "";
                        opacity: 1;
                        background: ${(props) => props.theme.colors.orange};
                        border-radius: 100px;
                        position: absolute;
                        top: 3px;
                        left: 3px;
                    }
                }
            }
        }
    }
`