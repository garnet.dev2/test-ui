"use client";
import { useMobile } from "@/utils/useMobile";
import Slider from "react-slick";
import { useState } from "react";
import { Image } from "@nextui-org/react";
import { nextImage } from "next/image";
import { MainBanner } from "./Styles";
import Link from "next/link";
import { AWS_S3_URL } from "@/constants";

export default function Banner({ banner }) {
  const [bannerd, setBanner] = useState(banner);
  var settings = {
    dots: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: false,
    fade: true,
    autoplaySpeed: 3000,
  };
  const isMobileScreen = useMobile();

  const sortedBanner = bannerd?.banner?.sort(
    (a, b) => a?.priority - b?.priority
  );

  return (
    <>
      <MainBanner>
        <Slider {...settings}>
          {sortedBanner?.map((banner, key) => {
            return (
              <Link href={banner?.url} target="_blank" key={key}>
                <div>
                  {!isMobileScreen && (
                    <div className="desktop_only">
                      <Image
                        as={nextImage}
                        disableAnimation={true}
                        disableSkeleton={true}
                        radius={"none"}
                        src={`${AWS_S3_URL}${banner?.image}`}
                        alt="test"
                        className="w-full "
                      />
                    </div>
                  )}
                  <div className="mobile_only">
                    <Image
                      as={nextImage}
                      disableAnimation={true}
                      disableSkeleton={true}
                      radius={"none"}
                      src={`${AWS_S3_URL}${banner?.mobileUrl}`}
                      alt="test"
                      className=""
                    />
                  </div>
                </div>
              </Link>
            );
          })}
        </Slider>
      </MainBanner>
      {/* </Link> */}
    </>
  );
}
