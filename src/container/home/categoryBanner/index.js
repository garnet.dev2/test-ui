/* eslint-disable react/jsx-key */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import Slider from "react-slick";
import { HomeCategory } from "./Styles";
import Link from "next/link";
import { useMobile } from "@/utils/useMobile";
import { AWS_S3_URL } from "@/constants";

export default function SimpleSlider({ categories }) {
  const isMobileScreen = useMobile();
  var settings = {
    slidesToShow: isMobileScreen ? 3 : 7,
    speed: 1000,
    arrows: true,
    mobileFirst: true,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 4,
          arrows: false,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 5,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 5,
          swipeToSlide: true,
        },
      },
    ],
  };
  return (
    <HomeCategory>
      <div className="container">
        <Slider {...settings}>
          {categories?.category?.map((category, key) => {
            return (
              <div key={key}>
                <Link
                  className="cat_thumb"
                  href={`/jewellery/${encodeURIComponent(category?.slug)}`}
                >
                  <img
                    src={`${AWS_S3_URL}${category?.thumbnailImage}`}
                    alt="category"
                  />
                </Link>
                <div className="cat_name">{category?.title}</div>
              </div>
            );
          })}
        </Slider>
      </div>
    </HomeCategory>
  );
}
