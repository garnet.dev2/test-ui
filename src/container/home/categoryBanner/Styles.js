import styled from "styled-components";

export const HomeCategory = styled.section`
    text-align: center;
    padding: 60px 0px 30px;
    background: rgb(0, 213, 172, 0.02);
    margin-bottom: 5px;
    &.pagenot-found-cat {
        display: flex;
        gap: 20px;
        padding: 20px 0px;
        overflow: auto;
        background: transparent;
        > div {
            width: 25%;
        }
    }
    .slick-slider {
        padding: 0px 50px;
    }
    .slick-prev, .slick-next {
        left: 0px;
        background-size: 81px;
        width: 81px;
        height: 53px;
        background-repeat: no-repeat;
        z-index: 99;
        cursor: pointer;
        &:before {content: none;}
    }
    .slick-prev {
        background: url('../assets/categories/left.svg');
    }
    .slick-next {
        right: 0;
        left: auto;
        background: url('../assets/categories/right.svg');
    }
    .cat_thumb {
        width: 105px;
        height: 105px;
        border-radius: 100%;
        background-image: linear-gradient(white, white), linear-gradient(to right, ${(props) => props.theme.colors.secondary}, ${(props) => props.theme.colors.orange});
        background-origin: border-box;
        background-clip: content-box, border-box;
        padding: 4px;
        margin: 0 auto;
        cursor: pointer;
        display: flex;
        align-items: center;
        justify-content: center;
        img {
            border-radius: 100px;
        }
        &:hover {
            box-shadow: 0px 4px 8px 0px #ccc;
        }
    }
    .cat_name {
        color: #302F2F;
        font-size: 15px;
        font-weight: 500;
        line-height: normal;
        padding: 10px 0px 15px;
        position: relative;
        &:after {
            content: "";
            height: 5px;
            width: 30px;
            background: linear-gradient(90deg, ${(props) => props.theme.colors.orange} , ${(props) => props.theme.colors.secondary});
            position: absolute;
            bottom: 0px;
            border-radius: 100px;
            left: 0;
            right: 0;
            margin: 0 auto;
        }
    }

    @media (max-width: 768px) {
        padding: 20px 0px;
        .slick-slider {
            padding: 0px 30px;
        }
        .cat_thumb {
            width: 70px;
            height: 70px;
        }

        .cat_name {
            font-size: 13px;
        }
    }

    @media (max-width: 575px) {
        .container {
            padding: 0px;
        }
        .slick-slider {
            padding: 0px;
        }
        .cat_thumb {
            padding: 3px;
        }
        .cat_name {
            font-size: 11px;
            padding: 5px 0px 8px;
            &:after {
                height: 3px;
            }
        }
    }

`;