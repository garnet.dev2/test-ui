import Image from "next/image";
import { CmsContent } from "./Style";

export default function JewelleryGuideCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h2 className="bottom-title">
              <Image
                src={
                  "/assets/footer/footer-logo-orange/solitaire-diamond-guide.svg"
                }
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            {data?.content1?.map((item: any, index: any) => (
              <h4 key={index}>
                <p>{item}</p>
              </h4>
            ))}

            <h3>{data?.subHeading2} </h3>
            {data?.content2?.map((item: any, index: any) => (
              <h4 key={index}>
                <p>{item}</p>
              </h4>
            ))}
          </div>
        </div>
      </CmsContent>
    </>
  );
}
