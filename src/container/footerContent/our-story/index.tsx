import Image from "next/image";
import { CmsContent } from "./Style";

export default function OurStoryCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h2 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/our-story.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>{data?.c1}</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                {data?.lists?.map((list: any, index: number) => {
                  return (
                    <li key={index}>
                      <p>{list}</p>
                    </li>
                  );
                })}
              </ul>
            </h4>
            <h4>
              <p>{data?.c2}</p>
            </h4>
            <h4>
              <p>{data?.c3}</p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
