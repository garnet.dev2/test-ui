import Image from "next/image";
import { CmsContent } from "./Style";

export default function FreeShippingCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/free-shipping.svg"}
                alt="bottom-line"
                width={30}
                height={30}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h2>{data?.mainHeading} PAN INDIA</h2>
            {data?.content?.map((item: any, index: number) => (
              <h4 key={index}>
                <p>{item}</p>
              </h4>
            ))}
          </div>
        </div>
      </CmsContent>
    </>
  );
}
