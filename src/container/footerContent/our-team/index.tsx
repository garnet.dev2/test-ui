import Image from "next/image";
import { CmsContent } from "./Style";

export default function OurTeamCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h2 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/our-team.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>
                <b>Ashish Sood</b>
              </p>
              <p>
                <b>Founder and MD</b>
              </p>
            </h4>
            {data?.contents?.map((item: any, index: number) => (
              <h4 key={index}>
                <p>{item}</p>
              </h4>
            ))}
          </div>
        </div>
      </CmsContent>
    </>
  );
}
