import Image from "next/image";
import { CmsContent } from "./Style";

export default function GoldRatesCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/gold-rates.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h2>{data?.subHeading1}</h2>
            <h3>
              <p>{data?.content1}</p>
            </h3>
            <h2>{data?.subHeading2}</h2>
            <h3>
              <p>{data?.content2}</p>
            </h3>
            <h2>{data?.subHeading3}</h2>
            <h3>
              {data?.content3?.map((item: any, index: any) => {
                return (
                  <p key={index}>
                    <b>{item?.boldText}</b>
                    {item?.content}
                  </p>
                );
              })}
            </h3>
            <h2>{data?.subHeading4}</h2>
            <p>{data?.content4}</p>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
