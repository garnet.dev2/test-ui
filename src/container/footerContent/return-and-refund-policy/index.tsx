import Image from "next/image";
import { CmsContent, TableContainer } from "./Style";
import React, { useEffect, useRef } from "react";
import { useRouter } from "next/router";

export default function ReturnAndRefundPolicyCms({ data }: any) {
  const lifetimeRef: any = useRef();
  const returnRef: any = useRef();
  const router = useRouter();
  const scrollLifetimeToDiv = () => {
    // router.push("/return-and-refund-policy");
    lifetimeRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const { key } = router.query;

  const scrollToDiv = () => {
    // router.push("/return-and-refund-policy");
    returnRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    if (key == "30DayReturn" || key == "returnAndRefund") {
      scrollToDiv();
    }

    if (
      key == "lifetimeExchange" &&
      router.pathname == "/return-and-refund-policy"
    ) {
      scrollLifetimeToDiv();
    } else if (key == "lifetimeExchange") {
      scrollLifetimeToDiv();
    }
  }, []);

  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={
                  "/assets/footer/footer-logo-orange/return-and-exchange-policy.svg"
                }
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container" ref={returnRef}>
            <br /> <br />
            <h2>{data?.subHeading}</h2>
            <h3>
              <h4>
                <p>
                  {data?.content1}
                  <b> 30-days without any questions asked! </b>
                </p>
              </h4>
              <h4>
                <p>
                  <b>{data?.subHeading2}</b>
                </p>
              </h4>
              <h4>
                <ul className="unorder-list">
                  {data?.lists?.map((list: any, index: any) => (
                    <li key={index}>
                      <p>{list}</p>
                    </li>
                  ))}
                </ul>
              </h4>
            </h3>
            <TableContainer>
              <table className="table">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>
                      <b>Amount Refund</b>
                    </th>
                    <th>
                      <b>Shipping Charges</b>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {data?.tableData1?.map((row: any, index: any) => (
                    <tr key={index}>
                      <td>
                        <p>
                          <b>{row?.title}</b>
                        </p>
                      </td>
                      <td>
                        <div className="many-text">
                          {row?.content1?.map((content: any, index: any) => (
                            <React.Fragment key={index}>
                              <p>{content}</p>
                            </React.Fragment>
                          ))}
                        </div>
                      </td>
                      <td>
                        <p>{row?.content2}</p>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </TableContainer>
          </div>
          <div className="container" ref={lifetimeRef}>
            <br />
            <h2>{data?.subHeading3}</h2>
            <h4>
              {" "}
              <p>
                {/* At Gemlay, we offer{" "} */}
                <b>lifetime exchange on the jewellery you bought from us.</b>
                {data?.content2}
              </p>
            </h4>
            <TableContainer>
              <table className="table">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>
                      <b>Amount Refund</b>
                    </th>
                    <th>
                      <b>Shipping Charges</b>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {data?.tableData2?.map((row: any, index: any) => (
                    <tr key={index}>
                      <td>
                        <p>
                          <b>{row?.title}</b>
                        </p>
                      </td>
                      <td>
                        <div className="many-text">
                          {row?.content1?.map((content: any, index: any) => (
                            <React.Fragment key={index}>
                              <p>{content}</p>
                            </React.Fragment>
                          ))}
                        </div>
                      </td>
                      <td>{row?.content2}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </TableContainer>
            <h3>
              <h4>
                <p>
                  <b>{data?.subHeading2}</b>
                </p>
              </h4>
              <h4>
                <ul className="unorder-list">
                  {data?.lists2?.map((list: any, index: any) => (
                    <li key={index}>
                      <p>{list}</p>
                    </li>
                  ))}
                </ul>
              </h4>
            </h3>
          </div>
          <div className="container">
            <h2>{data?.subHeading4}</h2>
            <h4>
              <p>
                At Gemlay, we offer
                <b>
                  {" "}
                  lifetime buy back on the jewellery you bought from us.
                </b>{" "}
                We arrange the pickup, you pick the new Diamond or Gold
                Jewellery and get the brand new product delivered to your home.
                A hassle-free procedure.
              </p>
            </h4>

            <TableContainer>
              <table className="table">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>
                      <b>Amount Refund</b>
                    </th>
                    <th>
                      <b>Shipping Charges</b>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {data?.tableData3?.map((row: any, index: any) => (
                    <tr key={index}>
                      <td>
                        <p>
                          <b>{row?.title}</b>
                        </p>
                      </td>
                      <td>
                        <div className="many-text">
                          {row?.content1?.map((content: any, index: any) => (
                            <React.Fragment key={index}>
                              <p>{content}</p>
                            </React.Fragment>
                          ))}
                        </div>
                      </td>
                      <td>{row?.content2}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </TableContainer>
            <h3>
              <h4>
                <p>
                  <b>T&C on Lifetime Buy Back</b>
                </p>
              </h4>
              <ul className="unorder-list">
                {data?.lists4?.map((list: any, index: any) => (
                  <li key={index}>
                    {" "}
                    <p>{list}</p>
                  </li>
                ))}
              </ul>
            </h3>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
