import Image from "next/image";
import { CmsContent } from "./Style";

export default function JewelleryGiftingCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/jewellery-gifting.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            {data?.contents?.map((item: any, index: number) => (
              <h4 key={index}>
                <p>{item}</p>
              </h4>
            ))}
          </div>
        </div>
      </CmsContent>
    </>
  );
}
