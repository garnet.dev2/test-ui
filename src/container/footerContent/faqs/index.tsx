import Image from "next/image";
import { CmsContent } from "./Style";

export default function FaqsCms(props: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">FAQs</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/question.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              FAQs
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>
                1. What types of diamond and gold jewellery does Gemlay offer?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Gemlay offers a wide range of diamond and gold jewellery,
                    including rings, necklaces, earrings, bracelets, pendants,
                    and more, crafted with exquisite designs and high-quality
                    materials.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>2. Are the diamonds used in Gemlay jewellery conflict-free?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    {" "}
                    Yes, all diamonds used in Gemlay jewellery are sourced from
                    reputable suppliers, ensuring that they are conflict-free
                    and ethically sourced, both natural as well as lab-grown
                    diamonds.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>3. Can I customise the design of my jewellery piece?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers customization options for all jewellery
                    pieces, allowing you to create a personalised desire that
                    reflects your unique style and preferences.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>4. What is the difference between 14k and 18k gold?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    The main difference lies in the gold purity, with 18k gold
                    containing a higher percentage of gold compared to 14k gold,
                    resulting in a richer colour and greater value.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>5. Do you offer international shipping?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    No, Gemlay does not offer international shipping. It
                    provides free shipping PAN India
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>4. What is the return policy for Gemlay jewellery?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Gemlay offers a hassle-free 30–days return policy from the
                    date of purchase. Please refer to our Returns & Exchanges
                    page for more information on our return policy.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>5. Are Gemlay products covered by a warranty?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay products are covered by a warranty against
                    manufacturing defects. Please contact our customer service
                    team for assistance with warranty claims.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>6. How do I care for and maintain my Gemlay jewellery?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    To maintain the beauty and shine of your Gemlay jewellery,
                    we recommend cleaning it regularly with a soft cloth and
                    mild detergent. Avoid exposing your jewellery to harsh
                    chemicals or abrasive cleaners, as this may damage the metal
                    and gemstones.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>7. Do you offer financing options for jewellery purchases?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Not yet, but Gemlay will offer financing options through
                    select third-party providers soon. Please contact our
                    customer service team for more information on available
                    financing options.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>8. Can I track the status of my order online?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, once your order has been processed and shipped, you
                    will receive a tracking number via email and message,
                    allowing you to track the status of your order online.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>9. Are the diamonds certified?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay diamonds are certified by reputable gemological
                    laboratories such as GIA (Gemological Institute of America)
                    or IGI (International Gemological Institute), ensuring their
                    quality and authenticity.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>10. Do you offer resizing services for rings?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers resizing services for rings. Please
                    contact our customer service team for assistance with ring
                    resizing.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>11. What is the estimated delivery time for orders?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    The estimated delivery time for orders varies between 10-15
                    days, depending on the shipping method selected and the
                    destination. Please refer to our Shipping & Delivery page
                    for more information on estimated delivery times.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                12. Are there any promotions or discounts available for Gemlay
                products?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay occasionally offers promotions and discounts on
                    select jewellery pieces. Please sign up for our newsletter
                    or follow us on social media to stay updated on our latest
                    offers and promotions.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                13. Can I request a specific diamond cut for my jewellery piece?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers a variety of diamond cuts including
                    round, princess, emerald, and more. Please contact our
                    customer service team for assistance with selecting the
                    perfect diamond cut for your jewellery piece.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>14. Do you offer engraving services for jewellery?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers engraving services for select jewellery
                    pieces. Please contact our customer service team for more
                    information on available engraving options.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                15. What is the difference between lab-grown diamonds and
                natural diamonds?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Lab-grown diamonds are created in a controlled laboratory
                    environment, while natural diamonds are formed in the earth
                    over millions of years. Both types of diamonds have similar
                    physical and chemical properties but differ in their origin.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                16. Are lab-grown diamonds more affordable than natural
                diamonds?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, lab-grown diamonds are typically more affordable than
                    natural diamonds, making them a popular choice for
                    budget-conscious shoppers. Additionally, lab-grown diamonds
                    offer a sustainable and eco-friendly alternative to natural
                    diamonds.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>17. Can I purchase loose diamonds from Gemlay?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>No, Gemlay does not offer loose diamonds for purchase,</p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                18. What is the process for resizing a bracelet or necklace?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    To resize a bracelet or necklace after purchasing, please
                    contact our customer service team for assistance. Depending
                    on the design and material of the jewellery piece, resizing
                    may require additional time and resources.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                19. Do you offer gift wrapping services for jewellery purchases?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers gift wrapping services for jewellery
                    purchases. Inform customer care to the needful.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                20. What is the difference between yellow, white, and rose gold?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    The main difference lies in the metal alloy used to create
                    the gold colour. Yellow gold is alloyed with copper and
                    silver, white gold is alloyed with nickel and zinc, and rose
                    gold is alloyed with copper and a small amount of silver.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>21. Do you offer repair services for damaged jewellery?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers repair services for damaged jewellery.
                    Please contact our customer service team for assistance with
                    repairing your jewellery piece.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                22. What is the process for returning or exchanging a jewellery
                piece?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    To initiate a return or exchange, please contact our
                    customer service team within the 30-days time frame from the
                    date of purchase. We will provide you with instructions on
                    how to return or exchange your jewellery piece.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>23. Can I purchase a matching jewellery set from Gemlay?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, Gemlay offers matching jewellery sets including
                    earrings, necklaces, and bracelets. Please browse our
                    collections or contact our customer service team for
                    assistance with selecting the perfect matching set.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>24. Are Gemlay jewellery pieces suitable for everyday wear?</p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Yes, many of our jewellery pieces are designed for everyday
                    wear, including rings, earrings, and necklaces. However, we
                    recommend removing your jewellery before engaging in
                    activities such as swimming or exercising to prevent damage.
                  </p>
                </li>
              </ul>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
