import Image from "next/image";
import { CmsContent } from "./Style";

export default function LifetimeExchangePolicyCms(props: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">GOLD GUIDE</h1>
            <h2 className="bottom-title">
              <Image
                src={"../cms.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              GOLD GUIDE
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <p>
              A privacy policy illustrates how personal information is collected
              from users and then used by the website/application when
              registering or purchasing a product using the website or mobile
              application.
            </p>
            <h3>What data do we collect from you?</h3>
            <p>
              When our site is visited, it automatically collects your device s
              information which includes your device s Internet Protocol address
              (IP address), time zone, your location (if active), Internet
              service provider, type of web browser you use, and cookies
              installed on your device. Apart from the above, we also record the
              web pages you visited on the website while registering on the
              website or mobile application, and/or while purchasing a product
              from the website or mobile application.
            </p>
            <p>
              When you register or purchase a product from our website or mobile
              application, we store your name, email id, phone number, billing
              address, and shipping address. Please note that we never store
              your payment information, which includes your credit card number,
              internet banking credentials, and passwords.
            </p>
            <h3>Usage of your personal information by us</h3>
            <p>
              Name, address, and other personal information is used by us to
              deliver the product to you. This information is provided to the
              third party handling the postal service with a contract of not
              divulging the information and keeping it confidential.
            </p>
            <p>
              We also use your personal information to communicate to you via
              SMS, Whatsapp, phone call, or email. This is primarily done to
              provide you newsletters and updates about our products. Please
              note that all this activity is done as per law.
            </p>
            <p>
              Data collected from your device is used to tackle any fraud or
              malfunctions. Moreover, this data is collected to analyze our
              website and mobile application s optimization.
            </p>
            <p>
              Also, we plan our marketing strategies and Ad campaigns based on
              the personal information collected. Again, we may share this
              information with a third party handling our marketing. But, they
              will keep all information confidential. We have been using google
              analytics to monitor our display ads. A user can anytime opt out
              of the ads by modifying their google ads settings.{" "}
            </p>
            <p>
              If you wish to discontinue receiving such communications, you have
              the option to unsubscribe at any time.
            </p>
            <h3>Sharing information with the third party</h3>
            <p>
              Please note that we will be sharing your personal information as
              per the law only with third parties like razor pay payment
              gateway. Also, as we provide your bills with a stamp of the
              company, the information is forwarded to the concerned government
              regulatory body.
            </p>
            <p>
              We also use certain tools like Google analytics to see the
              performance and statistics of our website. A few website links
              could be found on our website for advertisement purposes. But, we
              are not responsible for any use of your information by them.
              Kindly see their privacy policy for more information.
            </p>
            <h3>Information that we may receive from other sources</h3>
            <p>
              As per law, we can use the information received from a third
              party, public and/or commercial, to whom you may have provided
              your information and we are linked with them. That information
              will be combined with information collected by us through the
              website and mobile application and will be used for advertising
              purposes.
            </p>
            <h3>Security of the Data</h3>
            <p>
              It is our responsibility to secure your personal information. To
              do so, anti virus and anti phishing tools are installed on the
              servers where the data is retained. But, no system is completely
              secure. Therefore, the possibility of data theft is plausible.
            </p>
            <h3>Removal of data</h3>
            <p>
              At GarnetLane.com, we prioritize the privacy and data security of
              our users. We acknowledge that individuals may wish to exercise
              their right to permanently delete their data from our systems
              website and mobile application. Users who have made previous
              purchases can opt to delete their data, with the exception of
              billing information. To initiate this process, kindly send an
              email to info@gemlay.com, including your Name, customer ID,
              phone number, and email address. In the email, specify the
              particular data you want permanently removed from our system. We
              assure our users that prompt action will be taken on these
              requests, and the data deletion process will be completed within
              15 working days. Your trust and privacy are of utmost importance
              to us.
            </p>
            <h3>Change of the privacy policy</h3>
            <p>
              Please note that the company is prerogative of making any changes
              to the privacy policy without any intimation to the customers. It
              is a customer s responsibility to check the policies regularly for
              an update.
            </p>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
