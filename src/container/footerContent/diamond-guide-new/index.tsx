import Image from "next/image";
import { CmsContent } from "./Style";

export default function DiamondGuideNewCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/diamond1.svg"}
                alt="bottom-line"
                width={30}
                height={30}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>{data?.desc}</p>
            </h4>
            <h2>{data?.subHeading1}</h2>
            <h4>
              <p>{data?.content_p1_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p1_2}</p>
            </h4>
            <h2>{data?.subHeading2}</h2>
            <h4>
              <p>{data?.content_p2_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_2}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_3}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_4}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_5}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_6}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_7}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_8}</p>
            </h4>
            <h2>{data?.subHeading3}</h2>
            <h4>
              <p>{data?.content_p3_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_2}</p>
            </h4>
            <h2>{data?.subHeading4}</h2>
            <h4>
              <p>{data?.content_p4_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p4_2}</p>
            </h4>
            <h2>{data?.subHeading5}</h2>
            <h4>
              <p>{data?.content_p5_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_2}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_3}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_4}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_5}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_6}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_7}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_8}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_9}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_10}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_11}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_12}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_13}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_14}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_15}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_16}</p>
            </h4>

            <h4>
              <p>{data?.content_p5_17}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_18}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_19}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_20}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_21}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_22}</p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
