/* eslint-disable react/no-unescaped-entities */
import Image from "next/image";
import { CmsContent } from "../privacypolicy/Style";

export default function Termsconditioncms(props: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">TERMS & CONDITIONS</h1>
            <h2 className="bottom-title">
              <Image
                src={"/assets/footer/terms.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              TERMS & CONDITIONS
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <p>
              This website is owned by Gemlay, which is referred to as we,
              us or our in the content of the website respectively. Our
              headquarters is located in Mohali, Punjab (Full Address : Ground
              Floor, Jubilee Junction, Unit no:15, Airport Road, Sector 66,
              Sahibzada Ajit Singh Nagar, Punjab 160055)
            </p>
            <p>
              It is considered that you are aware of all the terms and
              conditions of the website, as you have availed of the services
              offered by us on our website or mobile application. Till you
              access the services offered by the website or mobile application,
              you are deemed to agree to all the terms and conditions that
              pertain to it.
            </p>
            <p>
              We have the full authority to change any term or condition as and
              when required without any commitment to inform you about the
              change. Thereby, the responsibility to be known about the terms
              and conditions lies with its user, who could be merely visiting
              the website or mobile application or could be a registered member
              who had purchased our merchandise.
            </p>
            <p>
              Please note that all the rights of the website lie with Gemlay. Thus, the company has copyrights on all the content
              published on the website or mobile application. And any usage of
              the content of the website or mobile application, whole or
              partial, is prohibited. This means any type of activity such as
              storing or copying the content or pictures from the website or
              mobile application is not at all allowed. The company can take
              legal action against the perpetrator as per the law.
            </p>
            <p>
              In case you require any clarification in regard to the terms and
              conditions, then please approach by phone or email provided in the
              “contact us” tab.
            </p>
            <h3>Who is eligible to visit the site or mobile application?</h3>
            <p>
              Only persons above 18 years of age are eligible to visit and use
              the website and mobile application. Any person faking his/her age
              anyhow will fall on the list of fraudulent and his immediate
              guardian or parent will be responsible for the act, which pertains
              to any action against the wrongdoing as per law.
            </p>
            <p>
              If you are accessing the website out of India, then laws of that
              state that pertains to accessing any website outside the state are
              applicable, along with the laws on accessing the website or mobile
              application in India. Accessing the website could be just visiting
              the website or mobile application, or it could be purchasing a
              product using the website or mobile application.
            </p>
            <h3>Registration</h3>
            <p>
              Registration is not a mandatory step to be performed to buy online
              jewellery from Gemlay website or mobile application.
            </p>
            <p>
              On performing the registration, you shall be divulging your
              personal information- email, phone number, email, et al. This
              information shall not be leaked by us to any one not associated
              with Gemlay. Through, the 3rd party vendors like courier
              service partner/s would be provided with some or all information.
              Any misuse by them, then Gemlay won’t be responsible for the
              same. But, in such a discrepancy, is ready to help the customer or
              any concerned legal department to track the perpetrator.
            </p>
            <p>
              We shall be sending regular updates to the customers about new
              schemes, new collections, new offers, etc on their email and
              WhatsApp messages without confirmation. The customer can stop the
              notifications anytime she/he wishes to by clicking the
              unsubscribe/stop button provided in all of the messages.
            </p>
            <h3>Terms and conditions relating to pricing and payment</h3>
            <p>
              As per the Indian payment process, CGST, SGST, and IGST are
              applicable on any purchase of merchandise from our website or
              mobile application. A proper bill against the purchase will be
              generated and supplied to the purchaser along with the product
              purchased.
            </p>
            <p>
              For any purchase done from outside India, a paypal account needs
              to be there with the purchaser, or it could be done using an
              international credit card. Please note that our company will
              receive the money in INR only. Thus, the foreign currency will be
              converted into Indian rupees in accordance with the currency
              conversion rate on the day of purchase. And the purchaser has to
              bear all the conversion expenses, as well as interest rates levied
              on using an international credit card. Also, charges levied on the
              transport of the package in India or outside India have to be paid
              by the purchaser who has used an international credit card.
            </p>
            <p>
              For any purchase using an international credit card, the person
              has to provide a soft copy of the residence proof of the country
              where this card was issued to the purchaser. Proofs could be a
              passport along with that country’s visa, driving license of that
              country, work permit of that country, or any other government
              proof showing that you reside in that country.
            </p>
            <p>
              Also, any other import or export duty levied on the shipping of
              the product has to be paid by the purchaser.
            </p>
            <h3>Privacy Policy</h3>
            <p>
              Privacy of the data we receive from you is always kept
              confidential. We may share it with a third party for various
              processes with an assurance that they, too, do not divulge it to
              anyone outside.
            </p>
            <p>
              Various information is collected and stored on our server for the
              processes involved in online business and to maintain the
              technical movement of the website and mobile application. 
            </p>
            <p>
              We never store the pay details viz., credit card details, net
              banking credentials, or any other banking detail with us.
            </p>
            <h3>How an order is delivered?</h3>
            <p>
              As soon as you place an order on our website or mobile
              application, after finishing the payment process, a date will be
              provided to you pertaining to delivery at the shipping address.
              But, please note that this date is not fixed until the order is
              dispatched by us. Once the order is dispatched to the mentioned
              shipping address, an email and SMS/Whatsapp message will be sent.
            </p>
            <p>
              We keep your personal information viz., name, phone number, email
              id and shipping address, will be kept confidential by us, and even
              by the courier company handling the dispatch. 
            </p>
            <p>
              You shall be provided with a unique courier id against the
              dispatch to your email, and phone via SMS/Whatsapp message. This
              ID could be used to track the movement of the dispatch anytime by
              you. To do the same, you need to login to Courier Company’s
              website and input the ID, which will provide all the necessary
              information you may require.
            </p>
            <p>
              In case of more information, kindly contact the courier company.
            </p>
            <p>
              Fast delivery option is available for the metropolitan cities in
              India, which includes Delhi, Mumbai, Kolkata, Bangalore, Chennai,
              Hyderabad and Pune only, at an extra charge.
            </p>
            <p>
              For COD, the above process will be the same, except the payment
              would be done after receiving the order by you. Please note that
              COD is only available in limited cities and only for up to Rs.
              50,000 purchases.
            </p>

            <h3>Express Delivery</h3>
            <p>
              Many of our products have Fast delivery options available, where
              the customer could get the order delivered within a week or even
              less than that. Usually, products having different sizes don't
              have fast delivery available for all sizes – rings, bracelets,
              bangles, etc.
            </p>
            <p>
              Usually, the conventional size ( as per Indian standards) is
              available for fast delivery. This option is available on our
              website as well as Gemlay’s mobile application.
            </p>
            <h3>Website and Mobile Application Security</h3>
            <ul>
              <li>
                We can take strict legal action against a perpetrator trying to
                barge into the security of our website or mobile application.
                Various scenarios where such action could be taken by us include
                Storing or copying the content or pictures posted on our website
                or mobile application partially or whole.
              </li>
              <li>
                Unsolicited meddling into the security system of the respective
                website or mobile application, which could be trying to access
                our data servers or attempting to hack our network, anyhow, or
                by sending a virus to crash our systems.
              </li>
              <li>
                All the rights to use content or pictures on the website or
                mobile application are reserved with Gemlay, which cannot
                be used by any outsider without our permission for any other
                website or mobile application, or for any marketing portfolios,
                and campaigns
              </li>
            </ul>
            <h3>Your responsibilities</h3>
            <p>
              It is totally the responsibility of the user of the website or
              mobile application to provide appropriate and accurate
              information, which includes Name, address, phone number, email Id,
              shipping address, billing address and other necessary details.
            </p>
            <p>
              Also, it is your responsibility to update the personal information
              in case any change is done to keep it current and accurate. In
              case, we find your information incomplete partially or completely,
              then as an act of prudence we procure the right to terminate your
              membership with us and may prohibit you from using our website or
              mobile application.
            </p>
            <p>
              We reserve the right to validate the information provided by you
              and in case of its inaccuracy, re-ask you to provide your correct
              information via email and SMS. Failure to provide accurate
              information can lead to the termination of your membership with
              Gemlay without any intimation from our side.
            </p>
            <p>
              Please note that if the delivery of the merchandise gets delayed
              due to any natural calamity or public holiday, then we are not
              liable for any extra expense of the same. You only have to bear
              such extra expenses. 
            </p>
            <p>
              In case you are purchasing something/s from our website or mobile
              application that bears a price of more than one lakh rupees then
              it is mandatory to provide your PAN card details. Failure to do so
              will lead to the cancellation of the order placed.
            </p>
            <p>
              International clients or clients paying using an international
              credit card need to submit a copy of the residence proof of the
              country to which they belong, which could be a Passport with a
              visa, work permit, residence proof, or a driving license.
            </p>
            <h3>Limiting order purchase</h3>
            <p>
              Please note that Gemlay has full authority to limit the
              purchase of all or for a few of its members anytime with regard to
              any discrepancy deemed or any instruction from the government of
              India. 
            </p>
            <h3>Cancellation of order by us</h3>
            <p>At times, we may cancel your order due to</p>
            <ul>
              <li>Error found in pricing</li>
              <li>An occurrence of a natural calamity</li>
              <li>Issues found in the information provided by you</li>
              <li>
                {" "}
                Immediate amendments are proposed by the government on online
                sales or on some other prospects.
              </li>
              <li>System malfunction</li>
              <li>Website hacked</li>
              <li>Downtime of server</li>
              <li>Courier company not accepting any order</li>
              <li>
                The government imposed a curfew in the nation or in some states
              </li>
              <li>Barred to deliver an order to a country</li>
            </ul>
            <p>
              In all the above cases, we can cancel an order placed and shall
              intimate about same to the order placer. Please note that there
              could be other unavoidable circumstances, which may not be
              mentioned here, during which we can cancel an order.
            </p>
            <h3>Cancellation of order by you</h3>
            <p>
              You can track the order placed by you using the unique courier id
              provided in your mail and via SMS/Whatsapp. In case, you have
              canceled an order due to any reason, the amount that had been paid
              will be refunded to you in the same mode you used to pay it prior.
            </p>
            <p>
              In the case of COD, the amount will be refunded to the User’s Bank
              Account Only.
            </p>
            <p>
              The process of refunding will be initiated as soon as you cancel
              the order. And it may take 5-6 working days to complete the
              process.
            </p>
            <p>
              In case you have the amount via net banking, debit card or credit
              card, and the transaction failed, then the amount will be refunded
              to you within 7-10 working days.
            </p>
            <p>
              In case you face any issues in getting the amount back please
              contact your bank for it.
            </p>
            <h3>Return and Exchange of the product</h3>
            <p>
              In case, you want to return or exchange the product received by
              you, then you need to intimate us about the same on our website or
              mobile application. The process of return or exchange of the
              product will be initiated as soon as possible.
            </p>
            <p>
              For detailed information about return and exchange, please visit
              the page dedicated to it.
            </p>
            <h3>Repairing a product in warranty</h3>
            <p>
              For customer delight and satisfaction, we provide free repairing
              of the product within the warranty period i.e. 1 year.
            </p>
            <p>How to initiate the process?</p>
            <ul>
              <li>
                The prime and foremost prerequisite for this is that you should
                have the original bill and stated warranty proof with you.
              </li>
              <li>
                Moreover, The jewellery certificate provided by Gemlay
                must be provided to us.
              </li>
              <li>Kindly call us to let us know about the damage.</li>
              <li>
                Our expert will listen to your problem and will assess if the
                damage is eligible for repair under warranty or not.
              </li>
              <li>
                An expert may ask you to send a softcopy of the bill and a
                picture of the damaged product on our email.
              </li>
              <li>
                Post all the above, a confirmation email will be received by you
                if the damage is considered to be repaired under warranty legit.
              </li>
              <li>
                Please note that we will not bear the cost of transporting the
                damaged product from you to us and vice-versa. This cost will be
                borne by you only.
              </li>
              <li>
                After the product gets repaired, we will send you the same via
                courier.
              </li>
              <li>
                Shipping Charges could be levied by us depending on the repair
                or tenure(after purchase) you approached us
              </li>
            </ul>
            <p>
              To know more about the privacy policy, please visit its dedicated
              page.
            </p>
            <h3>Reviews and Feedback</h3>
            <p>
              Gemlay can ask you to submit a review or feedback form.
              There is no obligation to submit the same to us. But, once
              submitted to us, then it becomes the property of Gemlay to
              use it as part of a promotional campaign anywhere on our website
              or social media platform.
            </p>
            <p>
              Gemlay reserve the right to remove the comment or review
              from the website or mobile application in case the connotation is
              found derogatory.
            </p>
            <p>
              Moreover, the company is not liable to pay you against the
              feedback provided. It is entirely your choice to provide a review
              to us.
            </p>
            <h3>Changes to terms and conditions</h3>
            <p>
              We can change terms and conditions as when we wish to without
              intimating the user about the same. It is the responsibility of
              the users of the website or mobile application to be updated of
              the terms and conditions on a regular basis. Intimation about the
              purchase or personal information submitted by you will be checked
              by the technical tools and experts. In case any additional
              information is required from you, then an email will be sent by
              the expert, or the system automatically
            </p>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
