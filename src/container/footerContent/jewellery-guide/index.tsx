import Image from "next/image";
import { CmsContent } from "./Style";

export default function JewelleryGuideCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/jewellery-guide.svg"}
                alt="bottom-line"
                width={25}
                height={25}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            {data?.content1?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading2}</h2>
            {data?.content2?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading3}</h2>
            {data?.content3?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading4}</h2>
            {data?.content4?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading5}</h2>
            {data?.content5?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading6}</h2>
            {data?.content6?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading7}</h2>
            {data?.content7?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading8}</h2>
            {data?.content8?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading9}</h2>
            {data?.content9?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading10}</h2>
            {data?.content10?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading11}</h2>
            {data?.content11?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}

            <h2>{data?.subHeading12}</h2>
            {data?.content12?.map((content: any, index: number) => (
              <h4 key={index}>
                <p>{content}</p>
              </h4>
            ))}
          </div>
        </div>
      </CmsContent>
    </>
  );
}
