import styled from "styled-components";

export const CmsContent = styled.div`
  .page-title {
    background: url("/cms-bg.png");
    position: relative;
    background-size: cover;
    background-repeat: no-repeat;
    box-sizing: border-box;
    width: 100%;
    background-position: bottom left;
    height: 200px;
    padding-inline: 20px;
    .title {
      position: absolute;
      top: 38px;
      font-size: 42px;
      color: ${(props) => props.theme.colors.lightgray};
      opacity: 0.5;
    }
    .bottom-title {
      font-size: 24px;
      color: #323232;
      font-weight: 700;
      border-radius: 0px 100px 0px 0px;
      position: relative;
      display: flex;
      align-items: center;
      gap: 10px;
      position: absolute;
      bottom: -10px;
    }
  }
  .diamond-guide {
    display: flex;
    max-width: 1000px;
    margin: 0 auto;
    position: relative;
    padding: 0;
    li {
      padding: 10px;
      text-align: center;
      position: static;
      &:after {
        content: "";
        border: 1px dashed ${(props) => props.theme.colors.black};
        position: absolute;
        left: 0;
        right: 0;
        width: 100%;
        bottom: 50px;
      }
      img {
        margin: 0 auto;
      }
      .text {
        font-size: 15px;
        font-weight: 600;
        padding: 50px 0px 0px;
        display: inline-block;
        width: 100%;
        position: relative;
        &:before {
          content: none;
          background: ${(props) => props.theme.colors.orange};
          height: 10px;
          width: 10px;
          position: absolute;
          border-radius: 100px;
          top: 27px;
          z-index: 1;
          left: 50%;
          transform: translateX(-50%);
        }
        &:after {
          content: none;
          position: absolute;
          bottom: -11px;
          border-left: 15px solid transparent;
          border-right: 15px solid transparent;
          border-bottom: 15px solid #fef5f5;
          left: 50%;
          transform: translateX(-50%);
        }
      }
      &:before {
        content: none;
      }
      &:hover {
        .hover-content {
          display: block;
        }
        .text {
          &:before {
            content: "";
          }
          &:after {
            content: "";
          }
        }
      }
    }
    .hover-content {
      position: absolute;
      top: 100%;
      left: 0;
      right: 0;
      background: #fef5f5;
      padding: 10px;
      display: none;
      text-align: left;
    }
  }
  .accordian-tabs {
    margin-top: 30px;
  }
  .tab-content {
    padding-top: 30px;
    padding-bottom: 50px;
    .rc-collapse {
      display: none;
      &.show {
        display: block;
      }
    }
  }
  .tabs {
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #d9d9d9;
    .rc-accordion-toggle {
      font-size: 15px;
      font-weight: 600;
      color: #302f2f;
      position: relative;
      padding-bottom: 10px;
      width: 25%;
      cursor: pointer;
      &.active {
        color: ${(props) => props.theme.colors.orange};
        &:before {
          content: "";
          background: ${(props) => props.theme.colors.orange};
          height: 2px;
          width: 100px;
          position: absolute;
          bottom: 0px;
          left: 0%;
        }
      }
    }
  }

  .cms-content {
    padding: 70px;
  }
  .h2-custom{
    text-align: left;
  }
  p {
    display: inline-block;
    width: 100%;
    font-size: 13px;
    color: ${(props) => props.theme.colors.black};
    /* padding-bottom: 20px; */
    text-align: justify;
  }
  h2 {
    display: inline-block;
    width: 100%;
    font-size: 20px;
    color: ${(props) => props.theme.colors.orange};
    font-weight: 500;
    padding-bottom: 20px;
  }
  h4 {
    display: inline-block;
    width: 100%;
    font-size: 20px;
    color: ${(props) => props.theme.colors.black};
    font-weight: 500;
    padding-bottom: 20px;
  }
  ul {
    padding-bottom: 20px;
    &.logo {
      display: flex;
      align-items: center;
      gap: 20px;
      li {
        padding-left: 0px;
        width: auto;
        &:before {
          content: none;
        }
      }
    }
    &.dismond-shap {
      display: flex;
      align-items: center;
      flex-wrap: wrap;
      gap: 0px;
      li {
        padding: 10px;
        width: 25%;
        display: flex;
        align-items: center;
        flex-flow: column;
        gap: 5px;
        &:before {
          content: none;
        }
      }
    }
    li {
      font-size: 13px;
      color: ${(props) => props.theme.colors.black};
      padding-bottom: 10px;
      display: inline-block;
      width: 100%;
      position: relative;
      padding-left: 15px;
      &:before {
        content: "";
        background: ${(props) => props.theme.colors.orange};
        height: 7px;
        width: 7px;
        border-radius: 100px;
        position: absolute;
        left: 0;
        top: 5px;
      }
    }
  }
  .diamons {
    display: flex;
    flex-wrap: wrap;
    li {
      width: 33.333%;
      padding: 20px;
      &:before {
        content: none;
      }
      .diamond-title {
        font-size: 15px;
        color: #302f2f;
        font-weight: 700;
        margin-bottom: 10px;
        display: inline-block;
        width: 100%;
        border-bottom: 1px solid #d9d9d9;
      }
      .diamond-info {
        display: flex;
        gap: 20px;
      }
      .content {
        font-size: 13px;
        color: #302f2f;
        width: 70%;
      }
    }
  }
  .guide-content {
    display: flex;
    gap: 30px;
    &.diamond-guid {
      margin-top: 30px;
    }
    .left,
    .right {
      width: 50%;
    }
  }
  .unorder-list {
    padding-inline: 20px;
    padding-bottom: 0px;
  }
  @media (min-width: 1600px) {
    h3 {
      font-size: 28px;
    }
    p {
      font-size: 20px;
    }
    .page-title {
      .title {
        top: 38px;
      }
      .bottom-title {
        font-size: 35px;
      }
    }
  }

  @media (max-width: 991px) {
    .diamons li {
      width: 50%;
      padding: 15px;
    }
  }

  @media (max-width: 575px) {
    .diamons li {
      width: 100%;
      padding: 15px 0px;
    }
    .guide-content {
      flex-flow: column;
      .left,
      .right {
        width: 100%;
      }
    }
  }

  @media (max-width: 768px) {
    .page-title {
      height: 150px;
      .title {
        font-size: 25px;
        top: 40px;
      }
      .bottom-title {
        font-size: 14px;
      }
    }
    h3 {
      font-size: 16px;
    }
    .guide-content.diamond-guid {
      margin-top: 0;
      gap: 0;
    }
    ul.dismond-shap li {
      width: 33.333%;
    }
    .tabs .rc-accordion-toggle {
      font-size: 13px;
      &.active:before {
        width: 60px;
      }
    }
    .tab-content {
      padding-top: 20px;
      padding-bottom: 0;
    }
    .diamond-guide {
      flex-flow: column;
      li {
        display: flex;
        align-items: center;
        gap: 10px;
        &:after {
          content: none;
        }
        img {
          margin: 0 auto;
        }
        .thumb {
          width: 80px;
        }
        .text {
          font-size: 11px;
          padding: 0;
          width: 110px;
          position: relative;
          text-align: left;
          &:before {
            content: none;
          }
          &:after {
            content: none;
          }
        }
        &:before {
          content: none;
        }
      }
      .hover-content {
        position: relative;
        top: 0;
        display: block;
        width: 80%;
        &:before {
          content: "";
          position: absolute;
          top: 50%;
          border-left: 15px solid transparent;
          border-right: 15px solid transparent;
          border-bottom: 15px solid #fef5f5;
          left: -20px;
          transform: translateY(-50%) rotate(-90deg);
        }
      }
    }
    .cms-content {
      padding: 40px 20px;
      text-align: justify;
    }
    .container {
      padding: 0;
    }
  }
`;