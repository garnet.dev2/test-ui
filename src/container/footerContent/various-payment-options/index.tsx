import Image from "next/image";
import { CmsContent } from "./Style";
import Item from "antd/es/list/Item";

export default function VariousPaymentOptionsCms({ data }: { data: any }) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={
                  "/assets/footer/footer-logo-orange/various-payment-method.svg"
                }
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h2 className="h2-custom">{data?.subHeading1}</h2>
            <h4>
              <p>{data?.content1}</p>
            </h4>
            {data?.content2?.map((item: any, index: number) => (
              <h4 key={index}>
                <p>
                  <b>{item?.boldText}</b>
                  {item?.content}
                </p>
              </h4>
            ))}
            <h4>
              <p>{data?.content3}</p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
