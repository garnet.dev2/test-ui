import Image from "next/image";
import { CmsContent } from "./Style";

export default function WhyGarnetCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h2 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/why-gemlay.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            {data?.contents?.map((contents: any, index: any) => (
              <h4 key={index}>
                <p>{contents}</p>
              </h4>
            ))}

            <h4>
              <ul className="unorder-list">
                {data?.lists?.map((lists: any, index: any) => (
                  <li key={index}>
                    <p>{lists}</p>
                  </li>
                ))}
              </ul>
            </h4>
            <h4>
              <p>
                <b>{data?.conslusion}</b>
              </p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
