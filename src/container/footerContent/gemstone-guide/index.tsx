import Image from "next/image";
import { CmsContent } from "../privacypolicy/Style";

export default function GemstoneGuidePage(props: any) {

    return (
        <>
            <CmsContent>
                <div className="page-title">
                    <div className="container">
                        <h1 className="title">GEMSTONE GUIDE</h1>
                        <h2 className="bottom-title">
                            <Image
                                src={"assets/filter/diamond.svg"}
                                alt="GEMSTONE GUIDE"
                                width={30}
                                height={30}
                            />
                           GEMSTONE GUIDE
                        </h2>
                    </div>
                </div>
                <div className="cms-content">
                    <div className="container">
                        <p>Here’s your guide to Everything-Gemstones! With more than a rainbow of colors to choose from - you can go with your very own birthstone, or just pick your favourite colour too!</p>
                        <ul className="diamons">
                            <li>
                                <span className="diamond-title">Garnet</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/garnet.png"}
                                            alt="Garnet"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Bold oranges, royal greens and hues of purples and pinks - are some of the hues of a Garnet. If you’re a January baby, you’re in for a treat! There are so many varieties to pick from, so go ahead uplift your spirit with your favorite Garnet gemstone.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Amethyst</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/amethyst.png"}
                                            alt="Amethyst"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">The Amethyst gemstone comes in different shades of the spectacular purple colour that ranges from a blend of a deep violet to a lighter lilac hue. This gemstone is as dreamy as the come, so be prepared to be transported to a soothing state of mind.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Aquamarine</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/aquamarine.png"}
                                            alt="Aquamarine"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Sea, salt, sun and everything fun! These aqua-blue colored babies are a perfect ode to the ocean. So if you’re a total beach baby or if you just love the mesmerising hues of blue, go ahead and pick one.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Diamond</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/diamond.png"}
                                            alt="Diamond"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">hey say diamonds are a girl’s best friend. Well, they’re absolutely right. :) A diamond is the perfect gesture that there is, and honestly, who can say no to it? Wearing a diamond is also said to bring benefits such as balance, clarity and abundance. So go ahead, and give it a try.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Emerald</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/emerald.png"}
                                            alt="Emerald"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Royal. Bold. Elegant. For all our Queens out there, there’s no other way to slay the royalty quotient than with a glorious Emerald. The deep hues of green will make you weak in the knees. :)</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Pearl</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/pearl.png"}
                                            alt="Pearl"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Be it a pair of ripped denim, a blazer or a formal gown: Pearl is a one stop solution to give you that charming look you’ve been wanting. They’re sophisticated, classy and will be sure to make heads turn.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Ruby</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/ruby.png"}
                                            alt="Ruby"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">The Ruby is said to be the reigning gemstone, and is said to bring an abundance of love. The deeper and more glorious the hue of the gemstone, the more valuable it is. Pick a gorgeous Ruby out, we promise you won’t regret it.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Peridot</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/peridot.png"}
                                            alt="Peridot"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">This mystical gemstone is said to have healing powers, and is known as the Gem of the Sun. After all, green does have a soothing effect on us. The refreshing hue of a Peridot is something we bet you’ll always, always love.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Sapphire</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/sapphire.png"}
                                            alt="Sapphire"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Nothing speaks royal like a true blue sapphire. Go pick out a dark blue hued beauty, if you’re a September born, or you love accessorising like the royalty you are.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Tourmaline</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/tourmaline.png"}
                                            alt="Tourmaline"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">The beauty of a Tourmaline gemstone is that it comes in a variety of gorgeous colours. Tourmaline is known to ward off negative energy. So you can totally pick one out, in your favourite colour too!</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Citrine</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/citrine.png"}
                                            alt="Citrine"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">If you’re all for ‘fun in the sun’, the Citrine gemstone is meant for you. Known to be a healing gemstone, citrine is said to be a gift from the Sun. So go and get yourself some sunshine.</span>
                                </div>
                            </li>
                            <li>
                                <span className="diamond-title">Blue Topaz</span>
                                <div className="diamond-info">
                                    <div className="thumb">
                                        <Image
                                            src={"/cms/blue-topaz.png"}
                                            alt="Blue Topaz"
                                            width={100}
                                            height={100}
                                        />
                                    </div>
                                    <span className="content">Blue Topaz is known for its amazing aqua blue shade. It also is the perfect gemstone for all things ‘Love’ - as it represents eternal romance. So if you’re a December baby, or you just love all things cheesy, this gemstone is the one for you.</span>
                                </div>
                            </li>
                        </ul>
                        <div className="guide-content">
                            <div className="left">
                                <div className="thumb">
                                    <Image
                                        src={"/cms/gemstone-guide.png"}
                                        alt="Gemstone Guide"
                                        width={608}
                                        height={290}
                                    />
                                </div>
                            </div>
                            <div className="right">
                                <h3>How to Care for Your Gems:</h3>
                                <ul>
                                    <li>Clean your gemstone jewellery with some warm water and gentle soap.</li>
                                    <li>Do not soak your pearls, corals or turquoise jewellery in water. We repeat, don’t do it.</li>
                                    <li>Humidity and moisture? Trust us, they are the enemy.</li>
                                    <li>Don’t wear your jewellery while swimming, in the shower or at the gym.</li>
                                    <li>Your gemstones aren’t perfume-friendly or lotion-friendly. So apply them first, and wear your jewellery after they settle into your skin.</li>
                                    <li>Got yourself a pretty string of pearls? Don’t forget to get them restrung every other year or so.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </CmsContent>
        </>
    )
}