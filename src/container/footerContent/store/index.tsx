import Image from "next/image";
import { StoreContent } from "./Style";

export default function Stores(props: any) {
  return (
    <>
      <StoreContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">Stores</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/faqs.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              Stores
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>
                1. What types of diamond and gold jewellery does Gemlay offer?
              </p>
            </h4>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Gemlay offers a wide range of diamond and gold jewellery,
                    including rings, necklaces, earrings, bracelets, pendants,
                    and more, crafted with exquisite designs and high-quality
                    materials.
                  </p>
                </li>
              </ul>
            </h4>
        

          </div>
        </div>
      </StoreContent>
    </>
  );
}
