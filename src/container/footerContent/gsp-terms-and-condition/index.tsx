import Image from "next/image";
import { CmsContent, TableContainer } from "./Style";

export default function GspTermsAndConditionCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h1 className="bottom-title">
              <Image
                src={"/assets/footer/gspLogo.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h2>{data?.subHeading1}</h2>

            <h4>
              <ul className="unorder-list">
                {data?.lists?.map((item: any, index: number) => (
                  <li key={index}>
                    <p>{item}</p>
                  </li>
                ))}
              </ul>
            </h4>
            <h2>{data?.subHeading2}</h2>
            <h4>
              <ul className="unorder-list">
                {data?.lists2?.map(
                  (item: any, index: number) =>
                    item && (
                      <li key={index}>
                        <p>{item}</p>
                      </li>
                    )
                )}
              </ul>
            </h4>
            <TableContainer>
              <table className="table">
                <thead>
                  <tr>
                    <th>You Pay for</th>
                    <th>Redemption Discount</th>
                  </tr>
                </thead>
                <tbody>
                  {data?.tableData?.map((item: any, index: number) => (
                    <tr key={index}>
                      <td>
                        <p>{item?.title}</p>
                      </td>
                      <td>
                        <p>{item?.content}</p>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </TableContainer>
            <h2>{data?.subHeading3}</h2>
            <h4>
              <ul className="unorder-list">
                {data?.lists3?.map((list: any, index: number) => (
                  <li key={index}>
                    <p>{list}</p>
                  </li>
                ))}
              </ul>
            </h4>
            <h2>{data?.subHeading4}</h2>
            <h4>
              <ul className="unorder-list">
                {data?.lists4?.map(
                  (list: any, index: number) =>
                    list && (
                      <li key={index}>
                        <p>{list}</p>
                      </li>
                    )
                )}
              </ul>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
