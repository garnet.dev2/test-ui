import Image from "next/image";
import { CmsContent } from "./Style";

export default function GoldGuideCms({ data }: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">{data?.mainHeading}</h1>
            <h2 className="bottom-title">
              <Image
                src={"/assets/footer/footer-logo-orange/gold-guide.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              {data?.mainHeading}
            </h2>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>{data?.desc}</p>
            </h4>

            <h3>{data?.subHeading1}</h3>
            <h4>
              <p>{data?.content_p1_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p1_2}</p>
            </h4>
            <h4>
              <p>{data?.content_p1_3}</p>
            </h4>
            <h3>{data?.subHeading2}</h3>
            <h4>
              <p>{data?.content_p2_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_2}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_3}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_4}</p>
            </h4>
            <h4>
              <p>{data?.content_p2_5}</p>
            </h4>

            <h3>{data?.subHeading3}</h3>
            <h4>
              <p>{data?.content_p3_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_2}</p>
            </h4>

            <h4>
              <p>{data?.content_p3_3}</p>
            </h4>

            <h4>
              <p>{data?.content_p3_4}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_5}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_6}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_7}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_8}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_9}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_10}</p>
            </h4>
            <h4>
              <p>{data?.content_p3_11}</p>
            </h4>

            <h4>
              <p>{data?.content_p3_12}</p>
            </h4>

            <h4>
              <p>{data?.content_p3_13}</p>
            </h4>

            <h3>{data?.subHeading4}</h3>
            <h4>
              <p>{data?.content_p4_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p4_2}</p>
            </h4>

            <h3>{data?.subHeading5}</h3>
            <h4>
              <p>{data?.content_p5_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_2}</p>
            </h4>
            <h4>
              <p>{data?.content_p5_3}</p>
            </h4>

            <h3>{data?.subHeading6}</h3>
            <h4>
              <p>{data?.content_p6_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p6_2}</p>
            </h4>

            <h3>{data?.subHeading7}</h3>
            <h4>
              <p>{data?.content_p7_1}</p>
            </h4>
            <h4>
              <p>{data?.content_p7_2}</p>
            </h4>

            <h3>{data?.subHeading8}</h3>
            <h4>
              <p>{data?.content_p8_1}</p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
