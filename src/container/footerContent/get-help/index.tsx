import Image from "next/image";
import { CmsContent } from "./Style";

export default function GetHelpCms(props: any) {
  return (
    <>
      <CmsContent>
        <div className="page-title">
          <div className="container">
            <h1 className="title">GET HELP</h1>
            <h1 className="bottom-title">
              <Image
                src={"../cms.svg"}
                alt="bottom-line"
                width={20}
                height={20}
              />
              GET HELP
            </h1>
          </div>
        </div>
        <div className="cms-content">
          <div className="container">
            <h4>
              <p>
                Having trouble logging into Gemlay? We're here to assist you!
                Below are some resources to help you troubleshoot and resolve
                your login issues:
              </p>
            </h4>
            <h2>Troubleshooting Steps:</h2>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    <b> Troubleshooting Steps: </b>
                    Check your connection: Check your phone connection if you do
                    not receive the OTP.
                  </p>
                </li>
                <li>
                  <p>
                    <b> Check your Phone number: </b>
                    Ensure you're entering the correct credentials.
                  </p>
                </li>
                <li>
                  <p>
                    <b> Clear browser cache: </b>
                    Learn how to clear your browser cache to resolve caching
                    issues.
                  </p>
                </li>
                <li>
                  <p>
                    <b> Enable cookies: </b>
                    Instructions on how to enable cookies in your browser for
                    seamless login.
                  </p>
                </li>
              </ul>
            </h4>
            <h2>Contact Information</h2>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    <b> Customer service phone: </b>
                    Call our dedicated support team at 99888-98866.
                  </p>
                </li>
                <li>
                  <p>
                    <b>Email support: </b>
                    Reach out to us via email at info@gemlay.com.
                  </p>
                </li>
              </ul>
            </h4>
            <h2>System Status Updates:</h2>
            <h4>
              <ul className="unorder-list">
                <li>
                  <p>
                    Check for any ongoing system maintenance or outages
                    affecting the login process, along with estimated resolution
                    times.
                  </p>
                </li>
              </ul>
            </h4>
            <h4>
              <p>
                If you're still experiencing problems logging in, don't hesitate
                to reach out to our support team for personalised assistance.
                We're here to ensure you have a smooth experience with Gemlay!
              </p>
            </h4>
          </div>
        </div>
      </CmsContent>
    </>
  );
}
