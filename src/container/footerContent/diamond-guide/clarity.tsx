import Image from "next/image";
import { CmsContent } from "../privacypolicy/Style";

export default function Clarity(props: any) {
    return (
        <CmsContent>
            <p>When light passes through a diamond, it bounces back, making the diamond sparkle! Some diamonds have imperfections in the form of spots and blemishes. The more the imperfections, the more difficult it is for light to pass through, thereby reducing the sparkle. If you’re lost, here’s all you need to remember: Lesser imperfection and inclusions = Better clarity</p>
            <p>If you’re buying a diamond that’s under a carat, don’t obsess over the spots or blemishes because they aren’t really visible to an untrained eye. It’s better to invest in the cut and sparkle on!</p>
            <ul className="diamond-guide">
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">FL</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">IF</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">VVS1/VVS2</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">VS1/VS2</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">S1/S2</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
                <li>
                    <div className="thumb">
                        <Image
                            src={"/diamond.png"}
                            alt="DIAMOND GUIDE"
                            width={102}
                            height={96}
                        />
                    </div>
                    <span className="text">l1/l2/l3</span>
                    <div className="hover-content">
                        FL - Flawless Flawless diamonds have no inclusions or blemishes detectable under 10x magnification, and are extremely rare.
                    </div>
                </li>
            </ul>
        </CmsContent>
    );
}