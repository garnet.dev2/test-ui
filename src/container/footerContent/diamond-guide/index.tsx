import React, { useState } from "react";
import Image from "next/image";
import { CmsContent } from "../privacypolicy/Style";
import Clarity from "./clarity";

export default function DiamondGuidePage(props: any) {

    const Info = [
        {
          id: 1,
          header: "1. Cut",
          text: <Clarity />,
        },
        {
          id: 2,
          header: "2. Clarity",
          text: <Clarity />,
        },
        {
          id: 3,
          header: "3. Colour",
          text: <Clarity />,
        },
        {
          id: 4,
          header: "4. Carat",
          text: <Clarity />,
        },
    ];
    
    const AccordionItem = (props: { handleToggle: any; active: any; details: any; }) => {
        const { handleToggle, active, details } = props;
        const { header, id } = details;
    
        return (
          <div
            className={`rc-accordion-toggle ${active === id ? "active" : ""}`}
            onClick={() => handleToggle(id)}
          >
            <h5 className="rc-accordion-title">{header}</h5>
          </div>
        );
      };
    
      const AccordionContent = (props: { active: any; details: any; close: any; }) => {
        const { active, details, close } = props;
        const { id, text } = details;
    
        return (
          <div className={`rc-collapse ${active === id ? "show" : ""}`}>
            {text}
          </div>
        );
      };

    const [active, setActive] = useState(1);

    const handleToggle = (index: any) => {
        if (!active === index) {
          setActive(1);
        } else {
          setActive(index);
        }
      };

    return (
        <>
            <CmsContent>
                <div className="page-title">
                    <div className="container">
                        <h1 className="title">DIAMOND GUIDE</h1>
                        <h2 className="bottom-title">
                            <Image
                                src={"assets/filter/diamond.svg"}
                                alt="DIAMOND GUIDE"
                                width={30}
                                height={30}
                            />
                           DIAMOND GUIDE
                        </h2>
                    </div>
                </div>
                <div className="cms-content">
                    <div className="container">
                        <p>We know you’re here because you only want the best and we would love to help you find the perfect diamond for you. We also know that buying diamonds can be a little overwhelming, but that doesn’t mean that you should ever settle or stress! So just sit back with a cup of coffee and let us walk you through everything you need to know before you buy that sparking diamond!</p>
                        <strong>Back to the Basics - The 4 Cs of a Diamond</strong>
                        <div className="accordian-tabs">
                            <div className="tabs">
                            {Info.map((details, index) => {
                                return (
                                    <AccordionItem
                                        key={index}
                                        active={active}
                                        handleToggle={handleToggle}
                                        details={details}
                                    />
                                );
                            })}
                            </div>
                            <div className="tab-content">
                            {Info.map((details, index) => {
                                return (
                                    <AccordionContent
                                    key={index}
                                    active={active}
                                    // handleToggle={handleToggle}
                                    details={details} close={undefined}
                                    />
                                );
                            })}
                            </div>
                          </div>
                        <div className="guide-content diamond-guid">
                            <div className="left">
                                <h4>Shape</h4>
                                <p>Shape refers to the overall outline of the diamond when viewed from the top and that determines the price as well. The most expensive shape is the round diamond. That’s because they are best at reflecting light and shine like there’s no tomorrow! Although the choice of the shape mostly depends on your personal style, a princess, cushion or heart cut is what we would recommend if your diamond is bigger than 1 carat.</p>
                            </div>
                            <div className="right">
                                <h4>Diamond Grading</h4>
                                <ul className="inner">
                                    <li>Say Hello to Halo! It makes the centre stone look bigger and works as a protective shield as well Go fancy with cushion and emerald cuts.</li>
                                    <li>They’re usually priced lower and make ‘em eyes pop, too!</li>
                                    <li>They’re usually priced lower and make ‘em eyes pop, too!</li>
                                </ul>
                            </div>
                        </div>
                        <div className="guide-content diamond-guid">
                            <div className="left">
                                <h4>Certifications</h4>
                                <p>The Certification is basically your diamond’s birth certificate. All our solitaire diamonds are meticulously analyzed and graded by the Gemological Institute of America (GIA), Hoge Raadvoor Diamant (HRD) & the IGI, the most trusted (and known) labs in the world.</p>
                            </div>
                            <div className="right">
                                <h4>Diamond Grading</h4>
                                <ul className="logo">
                                    <li>
                                        <Image
                                            src={"/diamond-guide/american.png"}
                                            alt="American"
                                            width={178}
                                            height={77}
                                        />
                                    </li>
                                    <li>
                                        <Image
                                            src={"/diamond-guide/gia.png"}
                                            alt="Gia"
                                            width={127}
                                            height={42}
                                        />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="guide-content diamond-guid">
                            <div className="left">
                                <h4>How to Care for Your Diamond:</h4>
                                <div className="thumb">
                                    <Image
                                        src={"/diamond-guide/diamond-care.png"}
                                        alt="How to Care for Your Diamond"
                                        width={531}
                                        height={172}
                                    />
                                </div>
                                <br />
                                <ul className="inner">
                                    <li>Once every week, clean your diamond jewellery in lukewarm soap water and rub it down gently with a brush, to remove the dirt and grime from the surface and ensure a long lasting shine.</li>
                                    <li>Remove your diamond jewellery when you go swimming or while doing household chores, so that the grime doesn’t make your jewellery look dull.</li>
                                    <li>Keep your jewellery in separate boxes to reduce the chances of scratches on the metal.</li>
                                </ul>
                            </div>
                            <div className="right">
                                <h4>Shop by Diamond Shapes</h4>
                                <ul className="dismond-shap">
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/round.png"}
                                                alt="ROUND"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">ROUND</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/pear.png"}
                                                alt="PEAR"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">PEAR</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/heart.png"}
                                                alt="HEART"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">HEART</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/princess.png"}
                                                alt="PRINCESS"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">PRINCESS</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/oval.png"}
                                                alt="OVAL"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">OVAL</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/oval.png"}
                                                alt="CUSHION"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">CUSHION</span>
                                    </li>
                                    <li>
                                        <div className="thumb">
                                            <Image
                                                src={"/diamond-guide/emerald.png"}
                                                alt="EMERALD"
                                                width={80}
                                                height={80}
                                            />
                                        </div>
                                        <span className="text">EMERALD</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </CmsContent>
        </>
    )
}