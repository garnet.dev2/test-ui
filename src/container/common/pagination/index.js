import React from "react";
import Link from "next/link";
import { PaginationStyle } from "./Style";
import { Pagination } from "@nextui-org/react";
//  function PaginationPage(props:any) {
const PaginationPage = (props) => {
  const {
    currentPage,
    totalCount,
    // pageSize,
    // totalCount = 0,
    handlePageChange,
  } = props;
// const totalpages =Math.ceil(totalCount / pageSize);

  return (
    <>
      <PaginationStyle>
        <Pagination
          currentPage={currentPage ?? 1}
          onChange={handlePageChange}
          total={totalCount}
          size="sm"
          initialPage={1}
          color="success"
          variant="bordered"
          radius="sm"
        />
      </PaginationStyle>
    </>
  );
};
export default PaginationPage;
