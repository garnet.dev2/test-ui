import styled from "styled-components";

export const PaginationStyle = styled.div`
    margin-top: 30px;
    display: flex;
    justify-content: flex-end;

    @media (max-width: 767px) {
        margin-top: 10px;
    }
`;