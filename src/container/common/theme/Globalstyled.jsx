import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

    * {
        padding:0;
        margin:0;
        box-sizing: border-box;
        outline:none;
        -webkit-text-size-adjust: none;
    }
    html,
    body,
    #__next {
        // height: 100%;
        min-height: 100%;
        display: inherit;
        scroll-behavior: smooth;
    }
    body {
        font-family: ${(props) => props.theme.fonts.family};
        line-height:1.5;
        position: relative;
        color: ${(props) => props.theme.colors.black};
        font-size: ${(props) => props.theme.fontSizes.regular};
        font-weight: 500;
        letter-spacing: 1px;
        /* cursor: none; */
    }
    #cursor-dot,
    #cursor-dot-outline {
    z-index: 999;
    pointer-events: none;
    position: absolute;
    top: 50%;
    left: 50%;
    border-radius: 50%;
    opacity: 0;
    transform: translate(-50%, -50%);
    transition: opacity 0.15s ease-in-out, transform 0.15s ease-in-out;
    }

    #cursor-dot {
    width: 12px;
    height: 12px;
    background-color: rgba(0, 122, 100, 1);
    }

    #cursor-dot-outline {
    width: 15px;
    height: 15px;
    background-color: rgba(0, 122, 100, 0.3);
    }
    a {
        color: ${(props) => props.theme.colors.black};
        font-family: ${(props) => props.theme.fonts.family};
        font-weight: 500;
    }
    a,
    a:hover {
        text-decoration:none;
        cursor: pointer;
    }
    img {
        max-width:100%;
        max-height:100%;
        height:auto;
    }
    ul,
    li, ol {
        list-style:none;
        padding:0;
        margin:0;
        font-family: ${(props) => props.theme.fonts.family};
    }

    input {
        &:-webkit-autofill,
        &:-webkit-autofill:hover,
        &:-webkit-autofill:focus
        &:-internal-autofill-selected {
            transition: background-color 5000s;
            background-color: transparent !important;
            -webkit-box-shadow: 0 0 0 1000px transparent inset;
            appearance: none !important;
            background-image: none !important;
        }
    }

    ${'' /* button,
    select {
        font-family: inherit;
        border: 1px solid ${(props) => props.theme.colors.border};
        padding: 10px;
        -webkit-appearance: none;
        &:disabled {
            background-color: hsl(0, 0%, 95%);
        }
    } */}

    #nprogress {
        .bar {
            height: 5px;
            background: #d5d3d8;
            z-index: 99991;
        }
        .peg {
            box-shadow: none;
        }
    }

    .product-collects .title-grid {
        background: ${(props) => props.theme.colors.white};
    }
    .card-img {
        margin-top: 15px;
        display: block;
        transition: 0.5s ease-in-out;
        width: 230px !important;
        height: 230px !important;
        margin: 0 auto;
        display: block !important;
    }
    .card-img:hover {
        transform: scale(1.2);
    }
    .Toastify__toast {
        width: auto !important;
        height: auto !important;
        border: 0px solid orange !important;
        box-shadow: none !important;
        border-radius: 5px !important;
        position: relative !important;
        padding: 15px 30px 15px 20px;
        border-radius: 100px !important;
        background: #4A4848 !important;
    }
    .Toastify__close-button {
        position: absolute;
        top: 50%;
        right: 10px;
        opacity: 1;
        transform: translateY(-50%);
        color: ${(props) => props.theme.colors.white};
    }
    .Toastify__toast-body {
        padding: 0px;
        font-size: 14px;
        color: ${(props) => props.theme.colors.white};
    }
    .container {
        padding-left: 15px;
        padding-right: 15px;
        margin: 0 auto;
        max-width: 1599px;
        width: auto;
        box-sizing: border-box;
    }
    .close-icon {
        border: 1px solid #F50;
        width: 24px;
        height: 24px;
        right: 20px;
        top: 20px;
        line-height: 24px;
        padding: 3px;
        text-align: center;
        &.share-close {
            border: 0px solid #F50;
            position: absolute;
            top: 15px;
            right: 10px;
            svg {
                color: ${(props) => props.theme.colors.white};
            }
        }
        svg {
            color: #f50
        }
        &:hover {
            /* background: #f50; */
            background: transparent;
            svg {
                color: ${(props) => props.theme.colors.white};
            }
        }
    }

    @media (max-width: 767px) {
        .product_card_style .container {
            padding: 0px;
        }
        .pdp-mobile {
            padding: 0px;
        }
        .card-img {
            width: 160px !important;
            height: 160px !important;
        }
    }
`;
