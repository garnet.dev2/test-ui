import styled from "styled-components";
// import { Max, Min } from "./breakpoints";

export const Space50 = () => `
  margin-bottom: 50px;
`;

export const Spacer30 = styled.div`
  height: 30px;
`;

export const Spacer10 = styled.div`
  height: 10px;
`;

export const Spacer50 = styled.div`
  height: 50px;
`;

export const DisplayCenter = () => `
  justify-content: center;
  display: flex;
  align-items: center;
`;

export const DisplayFlex = () => `
  display: flex;
`;

export const DisplayVertialCenter = () => `
  display: flex;
  align-items: center;
`;

export const DisplayFlexRow = () => `
  display: flex;
  max-width: 100%;
  flex-direction: row;
`;

export const DisplayFlexColumn = () => `
  display: flex;
  flex-direction: column;
`;

export const MobileHide = () => `
  display: none
`;

export const DesktopHide = () => `
  display: none
`;

export const Transform = (transforms) => `
  -moz-transform: ${transforms};
  -o-transform: ${transforms};
  -ms-transform: ${transforms};
  -webkit-transform: ${transforms};
  transform: ${transforms};
`;

export const customStyles = {
  control: (provided, { selectProps: { width } }) => ({
    ...provided,
    border: "0",
    boxShadow: "none",
    borderRadius: 0,
    minHeight: "25px",
    cursor: "pointer",
    fontSize: "12px",
    width,
    "&:hover": {
      border: "0",
      boxShadow: "none",
    },
    "&:focus": {
      border: "0",
      boxShadow: "none",
    },
  }),
  menu: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    border: 0,
    width: state.selectProps.width,
    margin: 0,
    zIndex: 10,
    fontSize: "13px",
    background: "white",
  }),
  option: (provided) => ({
    ...provided,
    color: "black",
    background: "white",
    cursor: "pointer",
  }),
  menuList: (provided) => ({
    ...provided,
    padding: 0,
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    background: `url('./../assets/down-arrow.svg') 0px 0px no-repeat`,
    width: "40px",
    padding: 0,
    display: "none",
  }),
  indicatorSeparator: () => ({
    background: "none",
  }),
  Svg: () => ({
    width: 10,
    display: "none",
  }),
  placeholder: (base) => ({
    ...base,
    fontSize: "13px",
  }),
};

export const productStyles = {
  control: (provided) => ({
    ...provided,
    border: "1px solid #E5E5E5",
    boxShadow: "none",
    borderRadius: "100px",
    minHeight: "auto",
    cursor: "pointer",
    fontSize: "14px",
    fontWeight: "600",
    width: "100%",

    height: "45px",
    "&:hover": {
      border: "1px solid ${(props) => props.theme.colors.orange}",
      boxShadow: "none",
    },
    "&:focus": {
      border: "1px solid ${(props) => props.theme.colors.orange}",
      boxShadow: "none",
    },
  }),
  menu: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    border: "1px solid #E5E5E5",
    background:
      "linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)",
    width: state.selectProps.width,
    margin: 0,
    zIndex: 10,
    fontSize: "13px",
    background: "white",
    width: "100%",
  }),
  option: (provided, state) => ({
    ...provided,
    color: "black",
    // background: state.isSelected
    //   ? "linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)"
    //   : provided.background,
    background: state.isSelected
      ? "#007A64"
      : provided.background,
    // background: 'linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)',
    // backgroundColor: "#2684FF",
    cursor: "pointer",
    fontSize: "13px",
    borderBottom: "1px solid #E5E5E5",
    "&:hover": {
      background:
        "#45B3A0",
      color: "white",
      // background:
      //   "linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)",
      // color: "white",
    },
    // '&:isSelected': {
    //   background: 'linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)',
    //   color: 'white',
    // },
  }),
  menuList: (provided) => ({
    ...provided,
    padding: 0,
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
  }),
  indicatorSeparator: () => ({
    background: "none",
  }),
  dropdownIndicator: () => ({
    color: "#fff",
    background: `url('../down-arrow-black.svg') center no-repeat`,
    width: "40px",
    padding: 0,
  }),
  svg: () => ({
    width: 10,
  }),
  multiValue: () => ({
    borderRadius: "50px",
    background: "rgba(255, 213, 136, 0.25)",
    display: "flex",
    alignItems: "center",
    padding: "3px",
    marginRight: "5px",
    color: "rgba(0, 0, 0, 0.80)",
    fontSize: "10px",
    fontWeight: "700",
  }),
  multiValueRemove: () => ({
    padding: "3px",
    "&:hover": {
      background: "none",
      boxShadow: "none",
    },
  }),
  placeholder: (base) => ({
    ...base,
    color: "black",
    lineHeight: "22px",
    fontSize: "13px",
  }),
};

export const filter = {
  control: (provided) => ({
    ...provided,
    border: "1px solid #C4C4C4",
    boxShadow: "none",
    borderRadius: "5px",
    minHeight: "auto",
    cursor: "pointer",
    fontSize: "12px",
    width: "104px",
    height: "30px",
    "&:hover": {
      border: "1px solid ${(props) => props.theme.colors.orange}",
      boxShadow: "none",
    },
    "&:focus": {
      border: "1px solid ${(props) => props.theme.colors.orange}",
      boxShadow: "none",
    },
  }),
  menu: (provided, state) => ({
    ...provided,
    borderRadius: 0,
    border: "1px solid #C4C4C4",
    width: state.selectProps.width,
    margin: 0,
    zIndex: 10,
    color: "#A09D9D",
    fontSize: "13px",
    background: "white",
    width: "100%",
  }),
  option: (provided) => ({
    ...provided,
    color: "#A09D9D",
    background: "white",
    cursor: "pointer",
    fontSize: "13px",
    borderBottom: "1px solid #C4C4C4",
    "&:hover": {
      background:
        "linear-gradient(180deg, ${(props) => props.theme.colors.orange} 0%, ${(props) => props.theme.colors.secondary} 100%)",
      color: "white",
    },
  }),
  menuList: (provided) => ({
    ...provided,
    padding: 0,
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    width: "20px",
    height: "20px",
    borderRadius: "100px",
    padding: 0,
    position: "absolute",
    right: "5px",
    top: "4px",
    color: "#514D47",
    padding: "0px",
  }),
  indicatorSeparator: () => ({
    background: "none",
  }),
  dropdownIndicator: () => ({
    color: "#514D47",
    padding: "0px",
  }),
  svg: () => ({
    width: 10,
  }),
  multiValue: () => ({
    borderRadius: "50px",
    background: "rgba(255, 213, 136, 0.25)",
    display: "flex",
    alignItems: "center",
    padding: "3px",
    marginRight: "5px",
    color: "rgba(0, 0, 0, 0.80)",
    fontSize: "10px",
    fontWeight: "700",
  }),
  multiValueRemove: () => ({
    padding: "3px",
    "&:hover": {
      background: "none",
      boxShadow: "none",
    },
  }),
  placeholder: (base) => ({
    ...base,
    color: "#A09D9D",
    lineHeight: "22px",
    fontSize: "13px",
  }),
};

export const Slide = styled.div`
  /* background: rgba(255, 248, 233); */
  background: #ffff;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  padding: 0px;
  height: ${(props) => props.theme.width.full};
  z-index: 1001;
  transform: translateX(-100%);
  transition: all 0.3s linear;
  overflow-y: auto;

  &.slide-active {
    transform: translateX(0);
    .navigation-menu {
      display: block;
    }
  }

  .menu-logo {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

export const SlideClose = styled.button`
  background: ${(props) => props.theme.colors.neonblue};
  border: 1px solid ${(props) => props.theme.colors.neonblue};
  width: 25px;
  height: 25px;
  border-radius: 25px;
  position: absolute;
  top: 10px;
  right: 10px;
  padding: 0;
  z-index: 3;
  cursor: pointer;

  &::before,
  &::after {
    position: absolute;
    left: 10px;
    top: 6px;
    content: "";
    height: 11px;
    width: 2px;
    background-color: ${(props) => props.theme.colors.black};
  }

  &::before {
    transform: rotate(45deg);
  }
  &::after {
    transform: rotate(-45deg);
  }
`;

export const Overlay = styled.div`
  background: ${(props) => props.theme.colors.black};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
  display: none;
  animation: 1s ease 0s 1 normal forwards running hAsUlT;

  &.overlay-active {
    display: block;
  }

  @-webkit-keyframes hAsUlT {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 0.8;
    }
  }
  @keyframes hAsUlT {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 0.8;
    }
  }
`;

export const PageTitle = styled.div`
  background: linear-gradient(
    90deg,
    ${(props) => props.theme.colors.orange} 30%,
    ${(props) => props.theme.colors.secondary} 100%
  );
  display: flex;
  align-items: center;
  gap: 10px;
  border-radius: 70px 0px 70px 0px;
  padding: 5px 5px 5px 95px;
  width: 100%;
  max-width: 321px;
  position: relative;
  height: 32px;
  &.after-search-title {
    max-width: 450px;
    margin-top: -20px;
    margin-left: 30px;
  }
  .title {
    font-size: 15px;
    color: ${(props) => props.theme.colors.white};
    font-weight: 700;
    height: 18px;
  }
  .thumb {
    position: absolute;
    left: 40px;
    top: -8px;
    height: 46px;
    width: 46px;
    background: ${(props) => props.theme.colors.white};
    border-radius: 100px;
    border: 1px solid ${(props) => props.theme.colors.orange};
    display: flex;
    align-items: center;
    justify-content: center;
  }

  @media (max-width: 640px) {
    ${
      "" /* background: none;
    border-radius: 0;
    padding: 0;
    width: 100%;
    max-width: 100%; */
    }
    ${"" /* .thumb {display: none;} */}
    .title {
      font-size: 13px;
      ${"" /* color: ${(props) => props.theme.colors.black}; */}
      font-weight: 700;
    }
    &.after-search-title {
      max-width: inherit;
      margin-top: 0;
      margin-left: 0;
      padding: 20px 20px 0px;
    }
  }
`;

export const PageTitleCollection = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  top: -46px;
  &.descover-bottom {
    top: 50px;
    .title-info {
      top: 11px;
    }
    a {
      position: relative;
    }
  }
  h3 {
    font-size: 20px;
    font-weight: 600;
    color: #302f2f;
    position: relative;
    &:after {
      content: "";
      background: ${(props) => props.theme.colors.orange};
      position: absolute;
      left: 50%;
      top: 14px;
      width: 350px;
      height: 2px;
      transform: translateX(-50%);
    }
  }
  .discover-all {
    font-size: 13px;
    color: ${(props) => props.theme.colors.orange};
    letter-spacing: 1.2px;
    padding-right: 15px;
    position: relative;
    display: block;
    width: 130px;
    margin: 0 auto;
    &:after {
      content: "";
      width: 0px;
      height: 0px;
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      border-left: 5px solid ${(props) => props.theme.colors.orange};
      position: absolute;
      right: 0;
      top: 4px;
    }
  }
  .sub-title {
    font-size: 18px;
    color: #a09d9d;
    letter-spacing: 1.2px;
  }
  .title {
    background: ${(props) => props.theme.colors.white};
    z-index: 1;
    position: relative;
    padding: 0px 20px;
  }
  .title-info {
    position: absolute;
    top: 40px;
    left: 50%;
    transform: translateX(-50%);
    text-align: center;
    display: flex;
    flex-flow: column;
    width: 100%;
  }

  @media (max-width: 640px) {
    h3 {
      font-size: 13px;
      &:after {
        width: 220px;
        top: 7px;
      }
    }
    &.descover-bottom {
      top: 30px;
    }
    .discover-all {
      font-size: 11px;
    }
    .sub-title {
      font-size: 10px;
    }
  }
`;

export const ClipmaskTitle = styled.div`
  width: 630px;
  clip-path: polygon(52% 39%, 100% 38%, 88% 100%, 12% 100%, 0% 38%);
  background: ${(props) => props.theme.colors.white};
  height: 120px;
  &.product-information {
    width: 300px;
    background: ${(props) => props.theme.colors.white};
    height: 70px;
    position: absolute;
    top: -31px;
  }

  @media (max-width: 640px) {
    width: 300px;
    height: 85px;
    &.product-information {
      width: 250px;
    }
  }
`;

export const ClipmaskDiscover = styled.div`
  width: 250px;
  clip-path: polygon(20% 0%, 80% 0%, 100% 100%, 0% 100%);
  background: ${(props) => props.theme.colors.white};
  height: 35px;
  &.gallry-mask {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    background: #efefef;
    bottom: -1px;
    &:before {
      content: "";
      width: 250px;
      height: 35px;
      background: ${(props) => props.theme.colors.white};
      display: block;
      position: absolute;
      top: 2px;
      left: 0px;
      clip-path: polygon(20% 0%, 80% 0%, 100% 100%, 0% 100%);
    }
  }
  @media (max-width: 640px) {
    width: 200px;
    height: 30px;
    &.gallry-mask {
      &:before {
        width: 200px;
        height: 30px;
      }
    }
  }
`;

export const ProductStyleBg = styled.div`
  padding: 50px 0px;
  background: linear-gradient(
    180deg,
    rgba(248, 248, 248, 0.59) 0%,
    rgba(248, 248, 248, 0.53) 100%
  );
  margin-bottom: 5px;
  &.best-seller {
    background: linear-gradient(180deg, #FAFAFA 0%, #FAFAFA 81%, #ecfff7a3 100%);
  }
  .product-card-title {
    display: inline-block;
    width: 100%;
    height: 60px;
    margin-bottom: 20px;
    .title {
      background: #fbfbfb;
    }
    &.best-seller-title {
      .title {
        background: rgb(255 252 246);
      }
    }
  }

  @media (max-width: 640px) {
    padding: 30px 0px;
    .product-card-title {
      height: 28px;
      margin-bottom: 10px;
    }
  }
`;

export const BottomSocialStyleNew = styled.div`
  .bottom_social {
    padding-top: 50px;
    padding-bottom: 30px;
    .social-text {
      color: ${(props) => props.theme.colors.orange};
      text-align: center;
      font-size: 15px;
      font-weight: 500;
      line-height: 22px;
      letter-spacing: 1px;
      display: block;
      padding-bottom: 10px;
    }
    .social-info {
      padding: 0;
    }
  }
  @media (max-width: 1200px) {
        .bottom_social {
            padding-top: 30px;
        }
    }
`;
