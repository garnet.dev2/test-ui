/* eslint-disable react/prop-types */
import { ThemeProvider } from "styled-components";
import themeVar from "./Variables";

const Theme = ({ children }) => (
  <ThemeProvider theme={themeVar}>{children}</ThemeProvider>
);

export default Theme;
