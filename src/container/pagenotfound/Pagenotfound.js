/* eslint-disable react-hooks/exhaustive-deps */
import Image from "next/image";
import Link from "next/link";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import { HomeCategory } from "../home/categoryBanner/Styles";
import { AWS_S3_URL } from "@/constants";

const Notfound = styled.div`
  .page-not-found {
    text-align: center;
    padding: 50px 0px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 50px;
    .left {
      width: 60%;
    }
    .right {
      width: 40%;
      .opps {
        text-align: left;
        font-size: 16px;
        font-weight: 600;
      }
    }
  }
  .page-title {
    padding-bottom: 10px;
    font-size: 25px;
    font-weight: 700;
    color: #e03838;
  }
  .opps {
    font-size: 20px;
    display: inline-block;
    width: 100%;
    padding-bottom: 10px;
    .red {
      color: #ff0707;
    }
  }
  .text {
    font-size: 16px;
    display: inline-block;
    width: 100%;
    padding-bottom: 10px;
  }
  .page-thumb {
    padding-bottom: 30px;
    img {
      margin: 0 auto;
    }
  }
  .timer {
    font-size: 18px;
    font-weight: 700;
    display: inline-block;
    width: 100%;
  }

  @media (max-width: 767px) {
    .page-not-found {
      flex-wrap: wrap;
      gap: 30px;
      .left {
        width: 100%;
      }
      .right {
        width: 100%;
      }
    }
    .page-title {
      pafont-size: 18px;
    }
    .text {
      font-size: 14px;
    }
    .timer {
      font-size: 16px;
    }
  }
`;

export default function Pagenotfound(props) {
  const Router = useRouter();
  const [counter, setCounter] = useState(10);
  useEffect(() => {
    if (counter === 0) Router.push("/");
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
  }, [counter]);
  

  return (
    <Notfound>
      <div className="container">
        <div className="page-not-found">
          <div className="left">
            <div className="page-thumb">
              <Image
                src={`/not-found.png`}
                width={378}
                height={194}
                alt="404"
              />
            </div>
            <h1 className="page-title">PAGE NOT FOUND</h1>
            <span className="opps">
              <span className="red">Oops !</span> You ran out of page.
            </span>
            <span className="text">
              The page you’re looking for is now beyond our reach Let’s get you
            </span>
            <div className="timer">Back To Home in 00:00:{counter}</div>
          </div>
          <div className="right">
            <div className="opps">BROWSE CATEGORY</div>
            <HomeCategory className="pagenot-found-cat">
              <div>
                <Link href="/jewellery/rings">
                  <div className="cat_thumb">
                    <img
                      src={`${AWS_S3_URL}garnet//images/Rings.webp`}
                      alt="category"
                    />
                  </div>  
                </Link>
                <div className="cat_name">
                  <span>Rings</span>
                </div>
              </div>
              <div>
                <Link href="/jewellery/earrings">
                  <div className="cat_thumb">
                    <img
                      src={`${AWS_S3_URL}garnet//images/Earrings.webp`}
                      alt="category"
                    />
                  </div>
                </Link>
                <div className="cat_name">
                  <span>Earrings</span>
                </div>
              </div>
              <div>
                <Link href="/jewellery/bracelets">
                  <div className="cat_thumb">
                    <img
                      src={`${AWS_S3_URL}garnet//images/Breclate.webp`}
                      alt="category"
                    />
                  </div>
                </Link>
                <div className="cat_name">
                  <span>Bracelets</span>
                </div>
              </div>
              <div>
                <Link href="/jewellery/bangles">
                  <div className="cat_thumb">
                    <img
                      src={`${AWS_S3_URL}garnet//images/Bangles.webp`}
                      alt="category"
                    />
                  </div>
                </Link>
                <div className="cat_name">
                  <span>Bangles</span>
                </div>
              </div>
            </HomeCategory>
          </div>
        </div>
      </div>
    </Notfound>
  );
}
