import Link from "next/link";
import Image from "next/image";
import {Input} from "@nextui-org/react";
import { BuyGoldStyle } from "./Style";
import DigiMenu from "./digimenu";
import { DiscoverBtn } from "@/styled/button";

export default function BuyGoldPage(props: any) {

    return (
        <>
            <BuyGoldStyle>
                <DigiMenu />
                <div className="container">
                    <span className="title">Buy Gold</span>
                    <Image
                        src={"/bottom-line.svg"}
                        alt="bottom-line"
                        width={51}
                        height={2}
                    />
                    <div className="buy-gold">
                        <form className="buy-gold-form">
                            <div className="field">
                                <Input
                                    type="text"
                                    radius="sm"
                                    size="lg"
                                    variant="bordered"
                                    label="Buy Gold by Amount"
                                    placeholder="Buy Gold by Amount"
                                    labelPlacement="outside"
                                />
                            </div>
                            <div className="field">
                                <Input
                                    type="text"
                                    radius="sm"
                                    size="lg"
                                    variant="bordered"
                                    label="or Buy in Gram"
                                    placeholder="or Buy in Gram"
                                    labelPlacement="outside"
                                />
                            </div>
                            <div className="processto-buy">
                               <DiscoverBtn>Proceed to Buy</DiscoverBtn>
                            </div>
                            <div className="note">Inclusive of 3% GST</div>
                        </form>
                        <div className="buy-rate">
                            <div className="rate-info">
                                <span className="title">Buy Rate</span>
                                <Link href="/" className="refresh">Refresh Gold Rate</Link>
                                <div className="price-avail">
                                    <span className="valid">Price is only valid for 5 mins</span>
                                    <span className="orange">Check Buy History</span>
                                </div>
                            </div>
                        </div>
                        <div className="buy-rate">
                            <div className="gold-info">
                                <span className="title">Gold Balance</span>
                                <div className="price-avail">
                                    <div className="price-list">
                                        <span>0.00 gms Garnet Lanee</span>
                                        <span>0.00 gms Safe Gold</span>
                                        <span>0.00 gms Garnet Lanee</span>
                                    </div>
                                    <span className="orange">Redeem Gold</span>
                                </div>
                                <span className="orange">*SafeGold can’t be sold here</span>
                            </div>
                        </div>
                    </div>
                    <div className="bottom-note">
                        <Image
                            src={"/note.svg"}
                            alt="bottom-line"
                            width={20}
                            height={20}
                        /> The minimum buy amount to purchase Garnet Lanee DigiGold is ₹10
                    </div>
                </div>
            </BuyGoldStyle>
        </>
    );
};