import styled from "styled-components";

export const DigiMenuStyle = styled.div`
    padding: 15px 0px;
    border-bottom: 1px solid #D9D9D9;
    .digi-gold-menu {
        display: flex;
        align-items: center;
        justify-content: space-between;
        a {
            color: #323232;
            font-size: 14px;
            font-weight: 600;
            &:hover {
                color: ${(props) => props.theme.colors.orange};
            }
        }
        .orange {
            color: ${(props) => props.theme.colors.orange};
            font-size: 14px;
        }
        .right-menu {
            display: flex;
            align-items: center;
            gap: 20px;
        }
    }

    @media (max-width: 640px) {
        .digi-gold-menu {
            flex-flow: column;
            gap: 10px;
            .right-menu {
                gap: 15px;
            }
        }   
    }
`;

export const DigiGoldStyle = styled.div`
    .buying-24k {
        background: linear-gradient(90deg, rgba(255,228,209,1) 0%, rgba(255,236,168,1) 42%, rgba(255,255,255,1) 81%);
        display: flex;
        align-items: center;
        justify-content: space-between;
        gap: 30px;
        padding: 60px 40px;
        h1 {
            font-size: 18px;
            color: #302F2F;
            padding-bottom: 10px;
        }
        .sub {
            font-size: 15px;
            color: #323232;
            padding-bottom: 10px;
        }
        .left {
            width: 330px;
        }
        .btn {
            display: flex;
            align-items: center;
            gap: 10px;
            padding-top: 20px;
            a {
                color: #323232;
                font-size: 14px;
                font-weight: 600;
                &:hover {
                    color: ${(props) => props.theme.colors.orange};
                }
            }
        }
    }
    .digi-usp {
        padding: 60px 40px;
        border-bottom: 1px solid #D9D9D9;
        border-top: 1px solid #D9D9D9;
        &.trust {
            display: flex;
            align-items: center;
            justify-content: space-between;
            gap: 10px;
        }
        h1 {
            font-size: 18px;
            color: #302F2F;
            font-weight: 600;
            padding-bottom: 10px;
        }
        .sub {
            font-size: 15px;
            color: #323232;
            padding-bottom: 10px;
        }
        .left {
            width: 60%;
        }
        .right {
            width: 15%;
        }
        ul {
            display: flex;
            align-items: center;
            gap: 25px;
            li {
                display: flex;
                gap: 15px;
                .thumb {
                    width: 70px;
                }
                .info {
                    display: flex;
                    flex-flow: column;
                    gap: 6px;
                    .title {
                        color: #323232;
                        font-size: 18px;
                        font-weight: 600;
                    }
                    .sub-title {
                        color: #323232;
                        font-size: 15px;
                    }
                }
            }
        }
    }

    .digigold-jwelarry {
        .title {
            display: flex;
            flex-flow: column;
            gap: 5px;
            align-items: center;
            padding: 40px;
            font-size: 20px;
            font-weight: 600;
        }
        .digi-gold-process {
            position: relative;
            .process-bg {
                img {
                    width: 100%;
                }
            }
            ul {
                display: flex;
                gap: 15px;
                justify-content: space-between;
                padding: 20px;
                position: absolute;
                left: 20px;
                top: 50%;
                transform: translateY(-50%);
                right: 20px;
                li {
                    display: flex;
                    flex-flow: column;
                    align-items: center;
                    text-align: center;
                    justify-content: center;
                    gap: 10px;
                    width: 14.28%;
                }
                .text {
                    font-size: 15px;
                    font-weight: 500;
                    color: #323232;
                }
            }
            .arrow {
                box-sizing: border-box;
                position: relative;
                display: block;
                transform: scale(var(--ggs,1));
                width: 90px;
                height: 22px;
                &:after,
                &:before {
                    content: "";
                    display: block;
                    box-sizing: border-box;
                    position: absolute;
                    right: 3px
                }
                &:after {
                    width: 15px;
                    height: 15px;
                    border-top: 3px solid ${(props) => props.theme.colors.secondary};
                    border-right: 3px solid ${(props) => props.theme.colors.secondary};
                    transform: rotate(45deg);
                    bottom: 4px;
                }
                &:before {
                    width: 85px;
                    height: 3px;
                    bottom: 10px;
                    background: linear-gradient(89deg, ${(props) => props.theme.colors.orange} -7.64%, ${(props) => props.theme.colors.secondary} 88.49%);
                }
            }
            .orange {
                font-size: 20px;
                font-weight: 600;
                color: ${(props) => props.theme.colors.orange};
            }
            .login-view {
                font-size: 13px;
                font-weight: 600;
                color: ${(props) => props.theme.colors.orange};
            }            
        }
    }
    .que-ans {
        display: flex;
        align-items: center;
        justify-content: space-between;
        gap: 20px;
        padding: 60px 40px;
        .left {
            width: 30%;
            .orange {
                font-size: 15px;
                color: ${(props) => props.theme.colors.orange};
                padding-bottom: 5px;
            }
            .sub {
                font-size: 18px;
                color: #323232;
            }
        }
        .right {
            .service-form {
                max-width: 400px;
                width: 100%;
                display: flex;
                align-items: center;
                justify-content: space-between;
                gap: 20px;
                .text {
                    font-size: 18px;
                    color: #323232;
                    text-align: center;
                    display: inline-block;
                    width: 100%;
                    padding-bottom: 20px;
                }
                .request-call {
                    width: 100%;
                    margin-top: 10px;
                }
            }
        }
    }

    @media (max-width: 640px) {
        .buying-24k {
            gap: 20px;
            padding: 30px;
            flex-flow: column;
            text-align: center;
            .btn {
                justify-content: center;
            }
        }
        .digi-usp {
            padding: 30px;
            ul {
                gap: 15px;
                flex-flow: column;
            }
            .left {
                width: 100%;
                text-align: center;
            }
            .right {
                width: 100%;
                img {
                    margin: 0 auto;
                }
            }
        }
        .digi-usp.trust {
            flex-flow: column;
        }
        .digigold-jwelarry .digi-gold-process {
            position: relative;
            .process-bg {
                display: none;
            }
            ul {
                position: static;
                transform: none;
                flex-flow: column;
                li {
                    width: 100%;
                }
            }
            .arrow {
                transform: rotate(90deg);
                width: 22px;
                height: 90px;
                top: 30px;
                left: 30px;
            }
        }
        .que-ans {
            padding: 30px;
            flex-flow: column;
            .left {
                width: 100%;
                text-align: center;
            }
        }
    }
`;

export const BuyGoldStyle = styled.div`
    .title {
        font-size: 18px;
        padding: 20px 0px 5px;
        display: inline-block;
        width: 100%;
    }
    .buy-gold-form {
        &.exchange {
            display: flex;
            align-items: flex-end;
            gap: 20px;
            .field {
                width: 20%;
            }
        }
    }
    .buy-gold {
        padding: 30px 0px;
        display: flex;
        gap: 40px;
        .buy-gold-form, .buy-rate {
            width: 33.333%;
            border-right: 1px solid #D9D9D9;
            padding-right: 40px;
        }
        .buy-gold-form {
            .field {
                margin-bottom: 45px;
            }
            .processto-buy {
                margin-top: -25px;
                button {width: 100%;}
            }
            .note {
                font-size: 13px;
                color: #323232;
                text-align: right;
                padding-top: 10px;
            }
        }
        .buy-rate {
            &:last-child {
                border: 0px;
            }
            .title {
                padding-top: 0px;
                font-weight: 700;
            }
            .refresh {
                display: inline-block;
                width: 100%;
                text-align: center;
                background: ${(props) => props.theme.colors.white};
                border: 1px solid #A09D9D;
                border-radius: 10px;
                padding: 12px;
                font-size: 15px;
                color: #323232;
                margin: 10px 0px;
            }
            .price-avail {
                display: flex;
                align-items: center;
                justify-content: space-between;
                gap: 10px;
            }
            .valid {
                font-size: 13px;
                color: #323232;
            }
            .price-list {
                display: flex;
                flex-flow: column;
            }
        }
        .rate-info {
            border: 1px solid #E5E5E5;
            background: #F9F8F8;
            padding: 20px;
            border-radius: 10px;
            margin: 40px 0px;
        }
        .gold-info {
            margin: 40px 0px;
        }
    }
    .bottom-note {
        font-size: 15px;
        color: ${(props) => props.theme.colors.black};
        padding: 30px 0px;
        display: flex;
        align-items: center;
        gap: 10px;
    }
    .digigold-jwelarry {
        .digi-gold-process { 
            ul {
                position: static;
                transform: none;
            }
        }
    }
    .orange {
        font-size: 13px;
        color: ${(props) => props.theme.colors.orange};
    }

    @media (max-width: 1024px) {
        .buy-gold {
            &.exchange-gold {
                gap: 20px;
                flex-flow: column;
                .buy-rate {
                    width: 100%;
                    padding: 0;
                }
                .rate-info {
                    margin: 0;
                }
            }
        }
        .buy-gold-form {
            &.exchange {
                flex-flow: column;
                .field {
                    width: 100%;
                }
                .processto-buy {
                    width: 100%;
                    button {
                        width: 100%;
                    }
                }
            }
        }
    }

    @media (max-width: 640px) {
        .buy-gold {
            gap: 20px;
            flex-flow: column;
            .buy-gold-form, .buy-rate {
                width: 100%;
                border-right: 0px;
                padding-right: 0;
                border-bottom: 1px solid #D9D9D9;
                padding-bottom: 20px;
            }
            .rate-info {
                margin: 0;
            }
            .gold-info {
                margin: 0;
            }
        }
    }
`;