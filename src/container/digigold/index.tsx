import Link from "next/link";
import Image from "next/image";
import {Input} from "@nextui-org/react";
import { DigiGoldStyle } from "./Style";
import { DiscoverBtn } from "@/styled/button";
import DigiMenu from "./digimenu";

export default function DigiGoldPage(props: any) {

    return (
        <>
            <DigiGoldStyle>
                <DigiMenu />
                <div className="buying-24k">
                    <div className="left">
                        <h1>Here is an easier way of buying pure 24kt gold</h1>
                        <span className="sub">Buy Garnet Lanee DiGiGold online to save money, grow your wealth, and convert your gold into beautiful jewellery—whenever you want it. Guaranteed, no-hassles buyback.</span>
                        <div className="btn">
                            <DiscoverBtn>Buy Digigold Now</DiscoverBtn>
                            <Link href="/">Learn More</Link>
                        </div>
                    </div>
                    <div className="right">
                        <Image
                            src={"/digigold/24k.png"}
                            alt="24K"
                            width={433}
                            height={287}
                        />
                    </div>
                </div>
                <div className="digi-usp">
                    <ul>
                        <li>
                            <div className="thumb">
                                <Image
                                    src={"/digigold/unparalleled.png"}
                                    alt="Unparalleled convenience"
                                    width={65}
                                    height={65}
                                />
                            </div>
                            <div className="info">
                                <span className="title">Unparalleled convenience</span>
                                <Image
                                    src={"/bottom-line.svg"}
                                    alt="bottom-line"
                                    width={51}
                                    height={2}
                                />
                                <span className="sub-title">Unparalleled convenience Buy online 24x7. Purchase gold online or offline through one of our partner jewellery stores.</span>
                            </div>
                        </li>
                        <li>
                            <div className="thumb">
                                <Image
                                    src={"/digigold/what-you-buy.png"}
                                    alt="What you buy is what you get"
                                    width={65}
                                    height={65}
                                />
                            </div>
                            <div className="info">
                                <span className="title">What you buy is what you get</span>
                                <Image
                                    src={"/bottom-line.svg"}
                                    alt="bottom-line"
                                    width={51}
                                    height={2}
                                />
                                <span className="sub-title">No carrying cost or hidden charges. Every gram of Garnet Lanee DiGiGold you buy online is backed by real gold deposits worth the same.</span>
                            </div>
                        </li>
                        <li>
                            <div className="thumb">
                                <Image
                                    src={"/digigold/guaranteed-buyback.png"}
                                    alt="100% guaranteed buyback"
                                    width={65}
                                    height={65}
                                />
                            </div>
                            <div className="info">
                                <span className="title">100% guaranteed buyback</span>
                                <Image
                                    src={"/bottom-line.svg"}
                                    alt="bottom-line"
                                    width={51}
                                    height={2}
                                />
                                <span className="sub-title">Redeem your Garnet Lanee DiGiGold balance across our 450+ online stores and physical outlets—Garnet Lanee</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="buying-24k">
                    <div className="left">
                        <span className="sub">Invest in a high-payoff digital gold. Buy, sell, or redeem your <strong>Garnet Lanee DiGiGold</strong> in exchange for beautiful jewellery.</span>
                    </div>
                    <div className="right">
                        <div className="btn">
                            <DiscoverBtn>Buy Digigold Now</DiscoverBtn>
                            <Link href="/">Learn More</Link>
                        </div>
                    </div>
                </div>
                <div className="digi-usp trust">
                    <div className="left">
                        <h1>Unwavering Trust</h1>
                        <span className="sub">At Tanishq and CaratLane, quality is our crown jewel. We craft 100% original gold jewellery that go through a rigorous quality check to give them a signature finish. Each gram of Tanishq DiGiGold comes with the Tata Seal of Trust. We have partnered with SafeGold digital platform to offer you a safe, convenient, and secure gold-buying experience</span>
                    </div>
                    <div className="right">
                        <Image
                             src={"/digigold/logo.png"}
                            alt="Logo"
                            width={187}
                            height={118}
                        />
                    </div>
                </div>
                <div className="digigold-jwelarry">
                    <div className="title">
                        DiGiGold to jewellery, in a blink!
                        <Image
                            src={"/bottom-line.svg"}
                            alt="bottom-line"
                            width={51}
                            height={2}
                        />
                    </div>
                    <div className="digi-gold-process">
                        <div className="process-bg">
                            <Image
                                src={"/digigold/bg.png"}
                                alt="Background"
                                width={1445}
                                height={295}
                            />
                        </div>
                        <ul>
                            <li>
                                <div className="thumb">
                                    <Image
                                        src={"/digigold/choose.png"}
                                        alt="Choose your favorite jewellery"
                                        width={60}
                                        height={67}
                                    />
                                </div>
                                <span className="text">Choose your favorite jewellery from Garnet Lanee</span>
                            </li>
                            <li>
                                <span className="arrow"></span>
                            </li>
                            <li>
                                <div className="thumb">
                                    <Image
                                        src={"/digigold/redeem.png"}
                                        alt="Redeem your Garnet Lanee DiGiGold"
                                        width={60}
                                        height={67}
                                    />
                                </div>
                                <span className="text">Redeem your Garnet Lanee DiGiGold at checkout</span>
                            </li>
                            <li>
                                <span className="arrow"></span>
                            </li>
                            <li>
                                <div className="thumb">
                                    <Image
                                        src={"/digigold/shipping.png"}
                                        alt="Redeem your Garnet Lanee DiGiGold"
                                        width={60}
                                        height={67}
                                    />
                                </div>
                                <span className="text">Redeem your Garnet Lanee DiGiGold at checkout</span>
                            </li>
                            <li>
                                <span className="arrow"></span>
                            </li>
                            <li>
                                <span className="orange">Visit Vault</span>
                                <span className="text">Check your gold Balance</span>
                                <span className="login-view">Login to View</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="que-ans">
                    <div className="left">
                        <h3 className="orange">Got questions? We have all the answers!</h3>
                        <span className="sub">If you have any questions regarding Garnet Lanee DiGiGold, give us your phone number and we will call you back to answer your questions.</span>
                    </div>
                    <div className="right">
                        <form className="service-form">
                            <div className="field">
                                <Image
                                    src={"/digigold/service.png"}
                                    alt="service"
                                    width={64}
                                    height={71}
                                />
                            </div>
                            <div className="field contact-field">
                                <span className="text">At Your Service. Always.</span>
                                <Input
                                    type="text"
                                    radius="sm"
                                    size="sm"
                                    variant="bordered"
                                    label="Mobile Number"
                                />
                                <DiscoverBtn className="request-call">Request Call Back</DiscoverBtn>
                            </div>
                        </form>
                    </div>
                </div>
            </DigiGoldStyle>
        </>
    )
}