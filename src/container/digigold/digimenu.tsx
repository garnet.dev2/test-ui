import Link from "next/link";
import { DigiMenuStyle } from "./Style";

export default function DigiMenu(props: any) {

    return (
        <>
            <DigiMenuStyle>
                <div className="container">
                    <div className="digi-gold-menu">
                        <div className="left-menu"><Link href="/digigold/"><span className="orange">DIGITAL</span> GOLD</Link></div>
                        <div className="right-menu">
                            <Link href="/digigold/buygold">Buy Gold</Link>
                            <Link href="/">Sell Gold</Link>
                            <Link href="/digigold/exchange">Exchange / Redeem</Link>
                            <Link href="/">FAQ</Link>
                        </div>
                    </div>
                </div>
            </DigiMenuStyle>
        </>
    );
};