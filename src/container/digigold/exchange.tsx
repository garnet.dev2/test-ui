import Link from "next/link";
import Image from "next/image";
import {Input} from "@nextui-org/react";
import { BuyGoldStyle, DigiGoldStyle } from "./Style";
import DigiMenu from "./digimenu";
import { DiscoverBtn } from "@/styled/button";

export default function ExchangeRedeemPage(props: any) {

    return (
        <>
            <BuyGoldStyle>
                <DigiMenu />
                <div className="container">
                    <span className="title">Exchange / Redeem</span>
                    <Image
                        src={"/bottom-line.svg"}
                        alt="bottom-line"
                        width={51}
                        height={2}
                    />
                    <div className="buy-gold exchange-gold">
                        <DigiGoldStyle>
                            <div className="digigold-jwelarry">
                                <div className="digi-gold-process">
                                    <ul>
                                        <li>
                                            <div className="thumb">
                                                <Image
                                                    src={"/digigold/choose.png"}
                                                    alt="Choose your favorite jewellery"
                                                    width={60}
                                                    height={67}
                                                />
                                            </div>
                                            <span className="text">Add to cart any Gold coin(s) or Jewellery</span>
                                        </li>
                                        <li>
                                            <span className="arrow"></span>
                                        </li>
                                        <li>
                                            <div className="thumb">
                                                <Image
                                                    src={"/digigold/redeem.png"}
                                                    alt="Redeem your Garnet Lanee DiGiGold"
                                                    width={60}
                                                    height={67}
                                                />
                                            </div>
                                            <span className="text">Pay by Digital Gold option at Checkout</span>
                                        </li>
                                        <li>
                                            <span className="arrow"></span>
                                        </li>
                                        <li>
                                            <div className="thumb">
                                                <Image
                                                    src={"/digigold/shipping.png"}
                                                    alt="Redeem your Garnet Lanee DiGiGold"
                                                    width={60}
                                                    height={67}
                                                />
                                            </div>
                                            <span className="text">Get it delivered at your Doorstep, free of cost!</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </DigiGoldStyle>
                        <div className="buy-rate">
                            <div className="rate-info">
                                <span className="title">EXCHANGE Rate</span>
                                <Link href="/" className="refresh">Refresh Gold Rate</Link>
                                <div className="price-avail">
                                    <span className="valid">Price is only valid for 5 mins</span>
                                    <span className="orange">Check Buy History</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form className="buy-gold-form exchange">
                        <div className="field">
                            <Input
                                type="text"
                                radius="sm"
                                size="lg"
                                variant="bordered"
                                label="Buy Gold by Amount"
                                placeholder="Buy Gold by Amount"
                                labelPlacement="outside"
                            />
                        </div>
                        <div className="field">
                            <Input
                                type="text"
                                radius="sm"
                                size="lg"
                                variant="bordered"
                                label="or Buy in Gram"
                                placeholder="or Buy in Gram"
                                labelPlacement="outside"
                            />
                        </div>
                        <div className="processto-buy">
                            <DiscoverBtn>Proceed to Buy</DiscoverBtn>
                        </div>
                    </form>
                    <div className="bottom-note">
                        <Image
                            src={"/note.svg"}
                            alt="bottom-line"
                            width={20}
                            height={20}
                        /> You can also Redeem your Gold Balance online as well as & our Jewellery Partner
                    </div>
                    <Link href="/" className="orange">Check Buy History</Link>
                </div>
            </BuyGoldStyle>
        </>
    );
};