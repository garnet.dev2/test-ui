import React from "react";
import Head from "next/head";

const Meta = (props) => {
  const { meta_link_type } = props;
  let baseUrl = "";
  // console.log(meta_link_type, '----meta link type', props?.title, '---title');
  if (typeof window !== "undefined") {
    baseUrl = window.location.origin;
  }
  return (
    <Head>
      <title>
        {props?.title
          ? `${props?.title}`
          : `Gemlay - Exclusive Diamond Jewellery`}
      </title>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1.0 user-scalable=0"
      />
      <meta property="og:title" content={`${props?.title}`} />
      <meta property="og:url" content={`${meta_link_type}`} />
      <meta property="og:description" content={`${props?.description}`} />
      <meta name="description" content={props?.description} />
      <meta name="keywords" content={`${props?.keywords}`}></meta>
      <meta
        data-react-helmet="true"
        property="og:image"
        content={`${props?.image}`}
      />
      <meta property="og:image:alt" content={`${props?.image}`} />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta property="og:site_name" content="gemlay.com" />
      <meta property="author" content="gemlay.com" />
      <meta property="og:type" content="website" />
      <meta
        name="google-site-verification"
        content="1QqjUOdfsJOlhKn_T3UInrCukbJHJmBlH5KTkCuzSQs"
      />
      <meta name="msvalidate.01" content="D71B2C718248E4F4618FBEDBCB6CA0AD" />
      <meta
        name="facebook-domain-verification"
        content="iqhgxp7sveb0823er7wj0iial3ax5y"
      />
      <meta property="product:brand" content="Gemlay" />
      <meta property="product:availability" content="in stock" />
      <meta property="product:condition" content="new" />
      <meta
        property="product:price:amount"
        content={props?.faceBookPixel?.price ? props?.faceBookPixel?.price : ""}
      />
      <meta property="product:price:currency" content="INR" />
      <meta
        property="product:retailer_item_id"
        content={
          props?.faceBookPixel?.productId ? props?.faceBookPixel?.productId : ""
        }
      />
      <meta
        property="product:item_group_id"
        content={
          props?.faceBookPixel?.productId ? props?.faceBookPixel?.productId : ""
        }
      />
      <link rel="canonical" href={`${meta_link_type}`} />
      {props?.schema_markup && (
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(props?.schema_markup),
          }}
        />
      )}
      {/* //ad/d */}
    </Head>
  );
};

export default Meta;
