import styled from "styled-components";

export const LoaderStyle = styled.div`
  width: 100%;
  height: 60vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoaderOverlay = styled.div`
  /* background: linear-gradient(to right, #EDF1F4, #C3CBDC); */
  background: white;
  opacity: 15;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1000;
  display: none;
  animation: 1s ease 0s 1 normal forwards running hAsUlT;

  &.overlay-active {
    display: block;
  }
`;
