import { Spinner } from "@nextui-org/react";
import { LoaderOverlay, LoaderStyle } from "./Styles";
import styles from "./style.module.scss";
const Loader = () => {
  return (
    <LoaderStyle>
      <Spinner color="success" size="lg" />
      <LoaderOverlay className="overlay-active" />
    </LoaderStyle>
  );
};
export default Loader;
