const initialState = {
  product: {},
};

export default function productDetailsReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case "GET_PRODUCT_DETAILS":
      return {
        ...state,
        product: action.payload,
      };

    default:
      return state;
  }
}

export function finalPriceReducer(state = initialState, action: any) {
  switch (action.type) {
    case "GET_FINAL_PRICE":
      return {
        ...state,
        finalPrice: action.payload,
      };
    default:
      return state;
  }
}

export function customizeSingleProductReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case "CUSTOMIZE_SINGLE_PRODUCT":
      return {
        ...state,
        product: action.payload,
      };
    default:
      return state;
  }
}

export function customizeSingleProductDataReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case "CUSTOMIZE_SINGLE_PRODUCT_DATA":
      return {
        ...state,
        product: action.payload,
      };
    default:
      return state;
  }
}
