// redux/reducers/authReducer.tsx
interface AuthState {
    user: null | any; // Replace 'User' with the actual user data type
  }
  
  interface Action {
    type: string;
    payload?: any;
  }
  
  const initialState: AuthState = {
    user: null,
  };
  
  const authReducer = (state: AuthState = initialState, action: Action): AuthState => {
    switch (action.type) {
      case 'LOGIN':
        return { ...state, user: action.payload };
      case 'LOGOUT':
        return { ...state, user: null };
      default:
        return state;
    }
  };
  
  export default authReducer;
  