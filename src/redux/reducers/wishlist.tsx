const initialState = {
  wishlistData: {},
};

export default function wishistReducer(state = initialState, action: any) {
  switch (action.type) {
    case "GET_WISHLIST":
      return {
        wishlistData: action.payload,
      };

    default:
      return state;
  }
}
