const initialState = {
  cartData: {},
  nliCarts: {},
  nliPayload: [],
};

export default function cartsReducer(state = initialState, action: any) {
  switch (action.type) {
    case "GET_CARTS":
      return {
        ...state,
        cartData: action.payload,
      };
    case "GET_NLI_PAYLOADS":
      return {
        ...state,
        nliPayload: action.payload,
      };
    case "GET_NLI_CARTS":
      return {
        ...state,
        nliCarts: action.payload,
      };

    default:
      return state;
  }
}
