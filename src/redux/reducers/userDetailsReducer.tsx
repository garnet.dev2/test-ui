const initialState = {
  user: {},
  plans: [],
};

export default function loggedInDetailsReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case "GET_USER_DETAILS":
      return {
        ...state,
        user: action.payload,
      };

    case "GET_GSP_DETAILS":
      return {
        ...state,
        plans: action.payload,
      };
    default:
      return state;
  }
}
