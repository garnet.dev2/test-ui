import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import headersData from "../headers";
import { API_BASE_URL } from "@/constants";

export const userReducers = createApi({
  reducerPath: "userReducers",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
    prepareHeaders: (headers) => headersData(headers),
  }),
  tagTypes: ["user"],
  endpoints: (builder) => ({
    getUser: builder.query({
      query: (query) => `/user/${query?.userId}`,
      providesTags: ["user"],
    }),

    getOrders: builder.query({
      query: (query) =>
        `/order-products?pagination=true&pageNumber=${
          query.pageNumber !== 1 ? query.pageNumber : 1
        }&customerId=${query?.id}`,
      providesTags: ["user"],
    }),

    trackOrders: builder.query({
      query: (query) => ({
        url: `/track_order`,
        method: "POST",
        body: query,
      }),
      // invalidatesTags: ["user"]
    }),

    trackOrderHistory: builder.query({
      query: (query) => `/track_order/history?AWBNo=${query}`,
      providesTags: ["user"],
    }),

    // Add a new endpoint for the POST request
    createUser: builder.mutation({
      query: (newUserData) => ({
        url: "/user",
        method: "POST",
        body: newUserData,
      }),
      invalidatesTags: ["user"], // Optional: Invalidates the 'user' cache after the mutation
    }),

    updateUser: builder.mutation({
      query: (payload) => ({
        url: `/user/${payload?.id}`,
        method: "PUT",
        body: payload,
      }),
      invalidatesTags: ["user"], // Optional: Invalidates the 'user' cache after the mutation
    }),

    getPincode: builder.query({
      query: (query) => `shipping_pincode?pincode=${query}`,
      providesTags: ["user"],
    }),

    updateUserAddress: builder.mutation({
      query: (payload) => ({
        url: `/user/address/manage/${payload?.id}`,
        method: "PUT",
        body: payload,
      }),
      invalidatesTags: ["user"], // Optional: Invalidates the 'user' cache after the mutation
    }),

    verifyToken: builder.mutation({
      query: (newUserData) => ({
        url: "/token",
        method: "POST",
        body: newUserData,
      }),
      invalidatesTags: ["user"], // Optional: Invalidates the 'user' cache after the mutation
    }),
  }),
});

export const {
  useGetUserQuery,
  useCreateUserMutation,
  useVerifyTokenMutation,
  // useAddUserAddressMutation,
  useUpdateUserAddressMutation,
  useGetOrdersQuery,
  useTrackOrdersQuery,
  useTrackOrderHistoryQuery,
  useUpdateUserMutation,
  useGetPincodeQuery,
  // useDeleteUserAddressMutation
} = userReducers;
