import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import headersData from "../headers";
import { API_BASE_URL } from "@/constants";

export const walletReducers = createApi({
  reducerPath: "walletReducers",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
    prepareHeaders: (headers) => headersData(headers),
  }),
  tagTypes: ["wallet"],
  endpoints: (builder) => ({
    getWallet: builder.query({
      query: (id) => `/wallet?userId=${id}`,
      providesTags: ["wallet"],
    }),

    getWalletHistory: builder.query({
      query: (query) =>
        `/transactions?admin=true&pagination=true&walletId=${query.id}&historyType=${query.type}`,
      providesTags: ["wallet"],
    }),
  }),
});

export const { useGetWalletQuery, useGetWalletHistoryQuery } = walletReducers;
