import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import headersData from "../headers";
import { API_BASE_URL } from "@/constants";

export const gspReducers = createApi({
  reducerPath: "gspReducers",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
    prepareHeaders: (headers) => headersData(headers),
  }),
  tagTypes: ["gsp"],
  endpoints: (builder) => ({
    getPlan: builder.query({
      query: (id) => `/gold-treasures/plans/${id}`,
      providesTags: ["gsp"],
    }),
    getGsp: builder.query({
      query: (id) => `/gold-treasures/getGspByPopId?popId=${id}`,
      providesTags: ["gsp"],
    }),
    getGspInvoice: builder.query({
      query: (id) => `/gold-treasure-invoice/singleInsDetails?installmentId=${id}`,
      providesTags: ["gsp"],
    }),
  }),
});
// trending-best+seller
export const { useGetPlanQuery, useGetGspQuery, useGetGspInvoiceQuery } =
  gspReducers;
