import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import headersData from '../headers';
import { API_BASE_URL } from '@/constants';


export const homeReducers = createApi({
  reducerPath: 'homeReducers',
  baseQuery: fetchBaseQuery({ baseUrl: API_BASE_URL,
    prepareHeaders: (headers) =>headersData(headers),
  }),
  tagTypes: ['home'],
  endpoints: (builder) => ({
    getFastDelivery: builder.query({
      query: (page = 1) => '/expressDelivery',
      providesTags: ['home'],
    }),
    getBanner: builder.query({
      query: (page = 1) => '/banner',
      providesTags: ['home'],
    }),
    getExclusive: builder.query({
      query: (page = 1) => '/exclusive?type=Default',
      providesTags: ['home'],
    }),
    getSilverExclusive: builder.query({
      query: (page = 1) => '/silver-exclusive?type=Default',
      providesTags: ['home'],
    }),
    getShopByGender: builder.query({
      query: (page = 1) => '/shopByGender',
      providesTags: ['home'],
    }),
    getSilverShopByGender: builder.query({
      query: (page = 1) => '/silver-shopByGender',
      providesTags: ['home'],
    }),
    getMegaMenu: builder.query({ 
      query: (query) => `/megamenu`,
      providesTags: ['home'],
    }),
    getReviews: builder.query({ 
      query: (page=1) => `/customer_review`,
      providesTags: ['home'],
    }),
    getTopHeader: builder.query({
      query: (page=1) => `/topHeader?pagination=true&pageNumber=1`,
      providesTags: ['home'],
    })
  }),
},

);

export const {
  useGetFastDeliveryQuery,
  useGetExclusiveQuery,
  useGetSilverExclusiveQuery,
  useGetSilverShopByGenderQuery,
  useGetShopByGenderQuery,
  useGetMegaMenuQuery,
  useGetBannerQuery,
  useGetReviewsQuery,
  useGetTopHeaderQuery,
} = homeReducers;
