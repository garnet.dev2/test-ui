import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import headersData from "../headers";
import { API_BASE_URL } from "@/constants";
const fetchingfields:any = [
  "bannerImage",
  // "category",
  // "color",
  // "productId",
  // "srno",
  "slug",
  "title",
  // "discount",
  // "glDesignNumber",
  // "grandTotal",
  // "crossPrice",
  // "isTrending",
  // "images",
];
export const productReducers = createApi({
  reducerPath: "productReducers",
  baseQuery: fetchBaseQuery({
    baseUrl: API_BASE_URL,
    prepareHeaders: (headers) => headersData(headers),
  }),
  tagTypes: ["product"],
  endpoints: (builder) => ({
    getProduct: builder.query({
      query: (page) =>
        `/product/all-products?slug=trending-best%2Bseller&pageSize=8`,
      providesTags: ["product"],
    }),
    getBestSellingProduct: builder.query({
      query: (page) =>
        `/product/all-products?slug=best%2Bseller&pageSize=8`,
      providesTags: ["product"],
    }),
    getTrendingProduct: builder.query({
      query: (page) =>
        `/product/all-products?slug=trending&pageSize=8`,
      providesTags: ["product"],
    }),
    getSilverBestSellingProduct: builder.query({
      query: (page) =>
        `/silver-product/all-products?slug=best%2Bseller&pageSize=8`,
      providesTags: ["product"],
    }),
    getSilverTrendingProduct: builder.query({
      query: (page) =>
        `/silver-product/all-products?slug=trending&pageSize=8`, 
      providesTags: ["product"],
    }),

    getPincode: builder.query({
      query: (query) =>
        `/shipping_pincode?pincode=${query?.pincode}&productSlug=${query?.slug}`,
      providesTags: ["product"],
    }),

    getAllProduct: builder.query({
      query: (query) =>
        `/product/all-products?pageNumber=${
          query.pageNumber !== 1 ? query.pageNumber : 1
        }&pageSize=16&pagination=true&slug=${encodeURIComponent(query?.slug)}`,
      providesTags: ["product"],
    }),


    getSearchedProduct: builder.query({
      query: (query) =>
        `/product?pageNumber=${
          query.pageNumber !== 1 ? query.pageNumber : 1
        }&pageSize=10&pagination=true&q=${encodeURIComponent(
          query?.q
        )}&select=${fetchingfields}&admin=true`,
      providesTags: ["product"],
    }),
  }),
});
// trending-best+seller
export const {
  useGetProductQuery,
  useGetTrendingProductQuery,
  useGetBestSellingProductQuery,
  useGetSilverTrendingProductQuery,
  useGetSilverBestSellingProductQuery,
  useGetAllProductQuery,
  useGetSearchedProductQuery,
  useGetPincodeQuery
} = productReducers;

// https://api.testgarnet.in/shipping_pincode?pincode=785001&productSlug=wonderous--diamond-ring
// https://api.testgarnet.in/shipping_pincode?pincode=140301&productSlug=noble-diamond-men-stud
