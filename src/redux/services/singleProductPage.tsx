import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import headersData from '../headers';
import { API_BASE_URL } from '@/constants';


export const singleProductReducers = createApi({
  reducerPath: 'singleProductReducers',
  baseQuery: fetchBaseQuery({ baseUrl: API_BASE_URL,
    prepareHeaders: (headers) =>headersData(headers),
  }),
  tagTypes: ['product'],
  endpoints: (builder) => ({
    getSingleProductByGl: builder.query({
      query: (query) => `/product/singleproductByGl/${query?.glDesignNumber}?type=${"glnumber"}`,
      providesTags: ['product'],
    }),
    getProductPrice: builder.query({
      query: (query) => `/productPrice/order?categoryName=${query?.category}&size=${query?.size}&diamondOrigin=${query?.diamondOrigin}&clarity=${query?.clarity}&goldkarot=${encodeURIComponent(query?.goldkarot)}&productId=${query?.id}`,
      providesTags: ['product'],
    }),
    getProductReview: builder.query({
      query: (query) => `/customer_review?&productId=${query?.id}&pagination=${query?.pagination}&pageNumber=${query?.pageNumber}&pageSize=${query?.pageSize}`,
      providesTags: ['product'],
    }),
    getIsReviewAvail: builder.query({
      query: (query) => `/isReviewAvail?&productId=${query?.pid}&customerId=${query?.cid}`,
      providesTags: ['product'],
    }),
  }),
},
);
export const {
  useGetSingleProductByGlQuery,
  useGetProductPriceQuery,
  useGetProductReviewQuery,
  useGetIsReviewAvailQuery
} = singleProductReducers;
