import { configureStore, Middleware, combineReducers } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { homeReducers } from "./services/home";
import { productReducers } from "./services/products";
import { userReducers } from "./services/users";
import productDetailsReducer, {
  customizeSingleProductReducer,
  customizeSingleProductDataReducer,
  finalPriceReducer,
} from "./reducers/productDetailsReducer";
import loggedInDetailsReducer from "./reducers/userDetailsReducer";
import { singleProductReducers } from "./services/singleProductPage";
import authReducer from "./reducers/authReducer";
import { gspReducers } from "./services/goldSecure";
import { walletReducers } from "./services/wallet";
import cartsReducer from "./reducers/cart";
import wishistReducer from './reducers/wishlist';

// Persist configuration
const persistConfig = {
  key: "root",
  storage,
  whitelist: [
    "loggedInDetailsReducer",
    "authReducer",
    "cartsReducer",
    "prodctDetailsReducer",
    "productReducers",
    "singleProductReducers",
    "wishistReducer"
  ], // Add reducers you want to persist
};

// Combine your reducers into a rootReducer
const rootReducer: any = combineReducers({
  [homeReducers.reducerPath]: homeReducers.reducer,
  [userReducers.reducerPath]: userReducers.reducer,
  loggedInDetailsReducer,
  [productReducers.reducerPath]: productReducers.reducer,
  productDetailsReducer,
  [singleProductReducers.reducerPath]: singleProductReducers.reducer,
  customizeSingleProductReducer,
  customizeSingleProductDataReducer,
  finalPriceReducer,
  cartsReducer,
  wishistReducer,
  [gspReducers.reducerPath]: gspReducers.reducer,
  [walletReducers.reducerPath]: walletReducers.reducer,
});

const middlewares: Middleware[] = [
  homeReducers.middleware,
  productReducers.middleware,
  singleProductReducers.middleware,
  gspReducers.middleware,
  walletReducers.middleware,
  userReducers.middleware,
];

// Persisted reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Configure store with persisted reducer and middleware
const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(middlewares),
});

// Setup persistor
const persistor = persistStore(store);

export { store, persistor };

// Export RootState type based on the combined reducer
export type RootState = ReturnType<typeof store.getState>;

// Setup listeners for the store
setupListeners(store.dispatch);
