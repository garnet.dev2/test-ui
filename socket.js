// src/socket.js
import { API_BASE_URL } from "@/constants";
import { io } from "socket.io-client";

const SOCKET_URL = API_BASE_URL;

const socket = io(SOCKET_URL);

export default socket;
