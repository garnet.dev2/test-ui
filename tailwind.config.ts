import {nextui} from '@nextui-org/theme'

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      fontSize: {
        '9': '9px',
      },
      backgroundImage: {
        'custom-gradient': 'linear-gradient(90deg, #FF6600 0%, #FF9900 100%)',
      },
      colors: {
        custom: '#531CC6',
        customNew: '#413e3e',
        customColor: '#FFDE6A73'
      },
      boxShadow: {
        'customShadow': '0px 2px 2px 0px rgba(0, 0, 0, 0.25)',
      },
    },
  },
  darkMode: "class",
  plugins: [nextui()],
}
